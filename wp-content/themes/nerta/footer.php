</div>

<footer class="footer">
    <div class="footer__container">
        <div class="footer__row">
            <div class="footer__column">
                <div class="footer__contacts">
                    <a class="footer__email" href="mail:info@nerta-sw.ru">info@nerta-sw.ru</a><br>
                    <a class="footer__phone" itemprop="telephone" href="tel:84956777000">8 (495) 6-777-000</a><br>
                    <a class="footer__phone" itemprop="telephone" href="tel:88005552593">8 (800) 555-25-93</a><br>
<!--                    <p>8 (800) 555-25-93</p>-->
<!--                    <p>+7 (495) 6-777-000</p>-->
                </div>
                <script data-b24-form="click/4/zy99qe" data-skip-moving="true"> (function(w,d,u){ var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/180000|0); var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h); })(window,document,'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_4.js'); </script>
                <button class="footer__feedback-button">Перезвоните мне</button>
                <div class="footer__address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    Россия, <span itemprop="addressLocality">Московская <br> область,</span> <span itemprop="streetAddress">Остаповский <br> проезд, 3,стр. 5 </span><br><br>
<!--                    <a href="/kontakty/#map_contacts">на карте</a>-->
                </div>
                <a href="#" class="footer__map__link">НА КАРТЕ</a>
            </div>
            <div class="footer__column" id="map">
                <img src="<?= get_template_directory_uri(); ?>/img/index/map.png"
                     title="Карта до офиса компании производителя моющего оборудования Nerta-SW"
                     alt="карта проезда в офис компанииNerta-SW">
            </div>
            <div class="footer__column">
                <menu>
                    <a href="#">оборудование</a>
                    <a href="#">мойка самообслуживания под ключ</a>
                    <a href="#">услуги</a>
                    <ul class="submenu">
                        <li><a href="#">Автомойка под ключ</a></li>
                        <li><a href="#">Автомойка в лизинг</a></li>
                        <li><a href="#">Франшиза мойки самообслуживания</a></li>
                        <li><a href="#">Продажа автомоек самообслуживания</a></li>
                    </ul>
                    <a href="#">о компании</a>
                    <a href="#">контакты</a>
                </menu>
            </div>
        </div>
        <div class="footer__row__information">
            <p class="">© Nerta Selfwash, 2001–2020</p>
            <div style="cursor: pointer" onclick="return location.href = '<?php home_url("template_url"); ?>/politika-konfidencialnosti/'" class="">Политика конфиденциальности</div>
            <p class="footer__empty__column">%%%%%%%%%%%%%%%%%%%%%%%%</p>
        </div>
    </div>
</footer>
<!--<div class="footer-fixed">-->
<!--    <a href="tel:88005552593" class="btn-footer-phone">8 (800) 5-552-593</a>-->
<!--</div>-->
<!--<div class="shadow" id="js-callback-popup" style="display:none;">-->
<!--    <div class="popup">--><?php //echo do_shortcode( '[contact-form-7 id="12" title="Callback"]' ); ?><!--  </div>-->
<!--</div>-->
<!---->
<!--<div class="shadow_2" id="js-callback-popup_2" style="display:none;">-->
<!--    <div class="popup"> Jnghfdkty </div>-->
<!--</div>-->


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-142223446-2"></script>



<?php
    wp_enqueue_script('jquery');
    wp_enqueue_script('newscript', get_template_directory_uri() . '/js/nerta.js');
    wp_footer(); ?>
<noscript><div><img src="https://mc.yandex.ru/watch/22985389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
</body>
</html>
