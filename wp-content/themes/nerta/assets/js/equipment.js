
let viewportWidth = window.innerWidth;
if (viewportWidth > 1024){
    $('.services__block').mouseover(function (){
        $(this).find('.services__button').show();
        $(this).find('.services__button_options').show();
        $(this).find('.services__information__numbers').css('margin-bottom', '15px');
    });

    $('.services__block').mouseout(function (){
        $(this).find('.services__button').hide();
        $(this).find('.services__button_options').hide();
        $(this).find('.services__information__numbers').css('margin-bottom', '55px');
    })
}

$('.floating_block').hide();

function onScroll(e) {

    let viewportWidth = window.innerWidth;
    if (viewportWidth < 992){
        $('.container_grey').waypoint(function () {
            $('.floating_block').show();
            $('.floating_block').addClass('fixed');
        }, {offset: '90%'});

        $('.calculator__information').waypoint(function () {
            $('.floating_block').removeClass('fixed');
            $('.floating_block').hide();
        }, {offset: '90%'});

        $('.calculator__title').waypoint(function () {
            $('.floating_block').removeClass('fixed');
            $('.floating_block').hide();
        }, {offset: '90%'});
    } else {
        $('.floating_block').hide();
    }
}

document.addEventListener('scroll', onScroll);