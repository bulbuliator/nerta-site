const fuel = {
    'gas-js': 25,
    'diesel-js': 21,
    'pellets-js': 0.055,
    'current': 25
}
const const_item_calculator = {
    'max_col_auto': 100,
    'water_consumption_per_day': 11,
    'electric_power': 120
}

function triplets(str) {
    let parts = (str + '').split('.'),
        main = parts[0],
        len = main.length,
        output = '',
        i = len - 1;

    while(i >= 0) {
        output = main.charAt(i) + output;
        if ((len - i) % 3 === 0 && i > 0) {
            output = ' ' + output;
        }
        --i;
    }

    if (parts.length > 1) {
        output += '.' + parts[1];
    }
    return output;
}

class Calculator {
    // params
    calculator_object_items = {
        'coll_posts': 4,
        'average_check': 150,
        'loading': 50,
        'water_and_sewerage': 60,
        'electricity': 6,
        'detergent_chemistry': 10,
        'duration': 3,
        'fuel': 25,
        'rent': 200000,
        'number_of_operators': 3,
        'operator_salary': 15000,
        'manager_salary': 25000,
        'accountant_salary': 5000,
        'tax': 6,
        'other_costs': 90000
    };
    // params
    calculation_result_items = {
        'revenue_per_day': 0,
        'TOTAL_variable_costs': 0,
        'seasonal_winter_costs': 0,
        'fixed_costs_per_month': 419000,
        'variable_costs_per_month_summer': 172800,
        'variable_costs_per_month_winter': 232800,
        'costs_per_year': 7281600,
        'average_monthly_turnover': 900000,
        'turnover_per_year': 10800000,
        'profit_for_the_year': 3518400,
        'average_monthly_profit': 293200
    };

    // metod :D
    get_range_data() {
        this.calculator_object_items = {
            'coll_posts': Number($('.coll_posts-js')[0].value),
            'average_check': Number($('.average_check-js')[0].value),
            'loading': Number($('.loading-js')[0].value),
            'water_and_sewerage': Number($('.water_and_sewerage-js')[0].value),
            'electricity': Number($('.electricity-js')[0].value),
            'detergent_chemistry': Number($('.detergent_chemistry-js')[0].value),
            'duration': Number($('.duration-js')[0].value),
            'fuel': Number($('.fuel-js')[0].value),
            'rent': Number($('.rent-js')[0].value),
            'number_of_operators': Number($('.number_of_operators-js')[0].value),
            'operator_salary': Number($('.operator_salary-js')[0].value),
            'manager_salary': Number($('.manager_salary-js')[0].value),
            'accountant_salary': Number($('.accountant_salary-js')[0].value),
            'tax': Number($('.tax-js')[0].value),
            'other_costs': Number($('.other_costs-js')[0].value)
        }

        sessionStorage.setItem('calculator-data-item', JSON.stringify(this.calculator_object_items));
    }
    get_result() {

        this.get_range_data();

        sessionStorage.setItem('fuel_value_current', this.calculator_object_items.fuel);
        sessionStorage.setItem(sessionStorage.getItem('active_button'), this.calculator_object_items.fuel);


        this.calculation_result_items.revenue_per_day = this.calculator_object_items.coll_posts
            * const_item_calculator.max_col_auto
            * this.calculator_object_items.loading / 100
            * this.calculator_object_items.average_check;

        this.calculation_result_items.TOTAL_variable_costs = this.calculator_object_items.coll_posts
            * this.calculator_object_items.loading / 100
            * (const_item_calculator.water_consumption_per_day
                * this.calculator_object_items.water_and_sewerage
                + const_item_calculator.electric_power
                * this.calculator_object_items.electricity)
            + this.calculation_result_items.revenue_per_day
            * this.calculator_object_items.detergent_chemistry / 100;

        this.calculation_result_items.seasonal_winter_costs = this.calculator_object_items.coll_posts * fuel.current * this.calculator_object_items.fuel;

        this.calculation_result_items.fixed_costs_per_month = this.calculator_object_items.rent
            + (this.calculator_object_items.number_of_operators
                * this.calculator_object_items.operator_salary)
            + this.calculator_object_items.manager_salary
            + this.calculator_object_items.accountant_salary
            + (this.calculation_result_items.revenue_per_day
                * this.calculator_object_items.tax / 100 * 30)
            + this.calculator_object_items.other_costs;

        this.calculation_result_items.variable_costs_per_month_summer = this.calculation_result_items.TOTAL_variable_costs * 30;

        this.calculation_result_items.variable_costs_per_month_winter = this.calculation_result_items.variable_costs_per_month_summer
            + this.calculation_result_items.seasonal_winter_costs * 30;

        this.calculation_result_items.costs_per_year = this.calculation_result_items.variable_costs_per_month_summer
            * (12 - this.calculator_object_items.duration)
            + this.calculation_result_items.variable_costs_per_month_winter
            * this.calculator_object_items.duration
            + this.calculation_result_items.fixed_costs_per_month * 12;

        this.calculation_result_items.average_monthly_turnover = this.calculation_result_items.revenue_per_day * 30;
        this.calculation_result_items.turnover_per_year = this.calculation_result_items.revenue_per_day * 360;
        this.calculation_result_items.profit_for_the_year = this.calculation_result_items.turnover_per_year - this.calculation_result_items.costs_per_year;
        this.calculation_result_items.average_monthly_profit = this.calculation_result_items.profit_for_the_year / 12;

        for (let key in this.calculation_result_items) {

            $('.' + key + '-js').html(triplets(Math.round(this.calculation_result_items[key])) + ' ₽');

        }
        sessionStorage.setItem('calculator-data-result', JSON.stringify(this.calculation_result_items));
    }

    update_range_block() {
        let calculatorBlock = $('.calculator-block__content');
        calculatorBlock.each(function (index, item) {
            let val = 0;
            let range = $(item).find('.calculator-block__range')[0];
            let output = $(item).find('.calculator-block__text_bold')[0];

            val = (range.value - range.min) / (range.max - range.min);
            output.innerHTML = triplets(range.value);

            range.style = 'background-image:'
                + ' -webkit-gradient(linear, 0% 0%, 100% 0%, '
                + 'color-stop(' + val + ', #1254B5), '
                + 'color-stop(' + val + ', #C5C5C5)'
                + ')';
        });

        this.get_result();
    }

}

//**********************************************
// first actions
//**********************************************
first_boot_calculator();

function first_boot_calculator() {

    let calculator = new Calculator();
    $('.accordion__content').css("display", "none");

// заполним sessionStorage
    if (sessionStorage.length === 0) {

        sessionStorage.setItem('calculator-data-item', JSON.stringify(calculator.calculator_object_items));
        sessionStorage.setItem('calculator-data-result', JSON.stringify(calculator.calculation_result_items));
        sessionStorage.setItem('gas', '20');
        sessionStorage.setItem('diesel', '45');
        sessionStorage.setItem('pellets', '10000');
        sessionStorage.setItem('active_button', 'gas');
        sessionStorage.setItem('fuel_value_current', '20');
    }

// получаем значения и заполняем значения по умолчанию range-slider
    let object_items = JSON.parse(sessionStorage.getItem('calculator-data-item'));

    for (let key in object_items) {
        $('.' + key + '-js')[0].value = Math.round(object_items[key]);
    }

// получаем и заполняем значения по умолчанию таблицы
    let object_results = JSON.parse(sessionStorage.getItem('calculator-data-result'));
    for (let key in object_results) {
        $('.' + key + '-js').html(triplets(Math.round(object_results[key])) + ' ₽');
    }
// нажатая кнопка по умолчанию
    $('.' + sessionStorage.getItem('active_button') + '-js').addClass('active_button');
    $('.fuel-js').val(sessionStorage.getItem('fuel_value_current'));
// стилизация слайдеров
    calculator.update_range_block();
//**********************************************
// functions
//**********************************************

//    событие при изменении типа топлива
    $('.button-js').click(function (e) {

        let name = e.currentTarget.dataset.type;
        $('.fuel-js').val(sessionStorage.getItem(name));
        fuel.current = fuel[name + '-js'];

        sessionStorage.setItem('active_button', name);
        sessionStorage.setItem(name, $('.input').val());
        sessionStorage.setItem('fuel_value_current', $('.input').val());

        if ($('.button-js').hasClass('active_button')) {
            $('.button-js').removeClass('active_button');
        }
        $(this).addClass('active_button');

        if (name == 'gas') {
            $('.fuel__measure').html('₽/куб');
            sessionStorage.setItem('gas-js', $('.input').val());
        } else if (name == 'diesel') {
            $('.fuel__measure').html('₽/литр');
            sessionStorage.setItem('diesel-js', $('.input').val());
        } else {
            $('.fuel__measure').html('₽/куб');
            sessionStorage.setItem('pellets-js', $('.input').val());
        }
        calculator.get_result();
    });
//    событие при изменении значения топлива
    $('.input').change(function () {
        calculator.get_result();
    })

    $('input[type="range"]').on('input', function () {
        calculator.update_range_block();
        calculator.get_result();
    });
// сворачивание таблицы
    $('.accordion__header').click(function () {
        $(this).siblings('.accordion__content').slideToggle("slow");
        if ($('#image').hasClass('active')) {
            $('#image').removeClass('active');
        } else {
            $('#image').addClass('active');
        }
    });
}



