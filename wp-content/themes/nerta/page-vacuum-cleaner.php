<?php
/**
 * Template Name: vacuum cleaner
 */
get_header(); ?>

<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/index.css">
<div id="nerta-main-page">

<section class="main" style="background-image: url('<?= get_template_directory_uri(); ?>/img/vacuum_cleaner/main_cleaner_page.png')">
    <div class="main__container">
        <h1 class="main__title">
            ПЫЛЕСОСЫ <br>САМООБСЛУЖИВАНИЯ <br>NERTA-SW
        </h1>
        <div class="brief">
            <p class="brief__cleaner">
                Увеличивает посещаемость мойки до 20%
            </p>
            </p>
            <p class="brief__cleaner">
                Приносит до 100 000 ₽ в месяц
            </p>
            <p class="brief__cleaner">
                Полностью автономен, управляется удалённо
            </p>
        </div>
    </div>
</section>



<?php get_footer(); ?>
