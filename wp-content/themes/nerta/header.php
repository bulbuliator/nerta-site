<!doctype html>
<html lang="ru">
<head>
    <meta name="robots" content="noindex">
    <meta name="yandex-verification" content="c51c9a50a1709132" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width,maximum-scale=1,initial-scale=1,user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="google-site-verification" content="8bYFBG2ZdPDZwPKtCfBcQy58kH5LhWZLx9UocbkMOV8" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/node_modules/swiper/css/swiper.min.css">
    <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/css/style.css">
<!--    <link rel="stylesheet" href="--><?php //bloginfo('stylesheet_url'); ?><!--">-->

	<title><?php wp_title(' | ', 'echo', 'right'); ?><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>

    <link rel="preload" href="<?php bloginfo("template_url"); ?>/fonts/ubuntubold.woff2" as="font" crossorigin="anonymous" />
    <link rel="preload" href="<?php bloginfo("template_url"); ?>/fonts/ubuntumedium.woff2" as="font" crossorigin="anonymous" />
    <link rel="preload" href="<?php bloginfo("template_url"); ?>/fonts/ubuntulight.woff2" as="font" crossorigin="anonymous" />
    <link rel="preload" href="<?php bloginfo("template_url"); ?>/fonts/opensansbold.woff2" as="font" crossorigin="anonymous" />
    <link rel="preload" href="<?php bloginfo("template_url"); ?>/fonts/Roboto.ttf" as="font" crossorigin="anonymous" />

<!--    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>-->

</head>
<body class="nerta">
<div style="position: absolute; clip: rect(0, 0, 0, 0);" aria-hidden="true">
    <svg xmlns="http://www.w3.org/2000/svg">
        <symbol id="menu-about" viewBox="0 0 52 71">
            <path d="M26 0c-.3 0-.4 0-.6.2C12.4 13.4 0 30.6 0 45c0 14.3 11.7 26 26 26 14.4 0 26-11.7 26-26C52 30.6 39.7 13.4 26.6.2L26 0zm0 2c12.8 13 24.4 29.8 24.4 43 0 13.5-11 24.4-24.4 24.4S1.6 58.4 1.6 45C1.6 31.7 13.2 15 26 2zm14.2 55.4c-.2 0-.4 0-.5.3-3.5 3.6-8.3 5.8-13.7 5.8-5.4 0-10.3-2.2-13.8-5.8-.2-.2-.4-.3-.7-.3-.6 0-.8 1-.4 1.3 4 4 9 6.4 15 6.4s11-2.4 15-6.3c.2-.2.3-.6 0-1 0 0-.4-.3-.8-.3z"/>
        </symbol>
        <symbol id="menu-hardware" viewBox="0 0 56.1 57.1">
            <path d="M11.7 57H2.4C1 57 0 56 0 54.8v-2.3C0 51 1 50 2.4 50h9.3c1.3 0 2.4 1 2.4 2.4v2.3c0 1.3-1 2.4-2.3 2.4zm-9.2-5c-.2 0-.4.3-.4.5v2.3c0 .2.3.4.5.4h9.3c.2 0 .4-.2.4-.4v-2.3c0-.2-.2-.4-.4-.4H2.5zM53.7 7H2.4C1 7 0 6 0 4.8V2.4C0 1 1 0 2.4 0h51.3C55 0 56 1 56 2.4v2.3C56 6 55 7 53.8 7zM2.5 2c-.2 0-.4.3-.4.5v2.3c0 .2.3.4.5.4h51.3c.2 0 .4-.2.4-.4V2.5c0-.2-.2-.4-.4-.4H2.5z"/><path d="M11 52H3V7.8C3 6.3 4.4 5 5.8 5h2.8C10 5 11 6.4 11 7.8V52zm-6-2h4V7.8C9 7.4 9 7 8.6 7H5.7c-.3 0-.6.4-.6.7V50zM53.7 57h-9.3C43 57 42 56 42 54.8v-2.3c0-1.3 1-2.4 2.4-2.4h9.3c1.3 0 2.4 1 2.4 2.4v2.3c0 1.3-1 2.4-2.3 2.4zm-9.2-5c-.2 0-.4.3-.4.5v2.3c0 .2.3.4.5.4h9.3c.2 0 .4-.2.4-.4v-2.3c0-.2-.2-.4-.4-.4h-9.3z"/><path d="M53 52h-8V7.8C45 6.3 46.4 5 47.8 5h2.8C52 5 53 6.4 53 7.8V52zm-6-2h4V7.8c0-.3-.2-.6-.5-.6h-2.8c-.3 0-.6.4-.6.7V50zM21.6 24H9v-7h12.6c1.4 0 2.5 1.2 2.5 2.6v2c0 1.3-1 2.5-2.4 2.5zM11 22h10.6c.3 0 .5 0 .5-.4v-2c0-.2 0-.4-.4-.4H11v3z"/><path d="M47 24H34.7c-1.4 0-2.5-1-2.5-2.4v-2c0-1.3 1.2-2.4 2.6-2.4H47v7zm-12.4-5c-.3 0-.5.3-.5.6v2c0 .2.3.4.6.4H45v-3H34.7z"/>
        </symbol>
        <symbol id="menu-chem" viewBox="0 0 55 87">
            <path d="M54.6 36.8L40.4 18c1-.2 1.6-1 1.6-2V2c0-1-1-2-2-2H14c-1 0-2 1-2 2v14c0 1 1 2 2 2L.4 36.8c-.3.4-.4.8-.4 1.2v34c0 9.4 4.4 15 12 15h31.4C50.7 87 55 81.4 55 72V38c0-.4 0-1-.4-1.2zM38 4v10h-2V4h2zm-6 0h2v10h-2V4zm-4 0h2v10h-2V4zm-4 0h2v10h-2V4zm-4 0h2v10h-2V4zm-4 0h2v10h-2V4zm35 68c0 5-1.3 11-7.6 11H12c-7 0-8-7-8-11V38.6L19 18h16.4L51 38.7V72z"/><path d="M46.3 71c-.6 0-1-.4-1-1V44.3c0-.6.4-1 1-1s1 .4 1 1V70c0 .6-.5 1-1 1z"/><circle cx="46.3" cy="75" r="2.2"/>
        </symbol>
        <symbol id="menu-contacts" viewBox="0 0 50.6 56.5">
            <path d="M42.2 56.5c-10.7 0-22.2-6.2-31-16.5C-1 25.4-.3 11.8.5 6.6 1.2 1.8 6.4 0 9.7 0c1 0 2.3.2 3 1 3.3 4 6.6 12.3 6.8 12.6.8 2.3.3 4.5-1.2 5.8L14 23c-.3 1.7 1 4.8 3.4 8.2 3.7 5.2 8.7 9.3 11.3 9.3L33 37c1.5-1.4 3.8-1.5 5.8-.4.4.2 8 5 11.4 9 1 1 .4 3.6-.5 5.5-1.7 3.6-4.3 5.5-7.5 5.5zm0-2c2.5 0 4-1.6 5-3 1.3-2 1.6-4 1.4-4.5-3.2-4-10.8-8.5-10.8-8.5-1.2-.6-2.6-.6-3.5 0l-4.8 4h-.8c-3.8 0-9.4-5-13-10C14 30 11 25.3 12 22.2l5-4.3c1-1 1-2.2.6-3.6 0 0-3.3-8.3-6.5-12.2 0 0-.4-.2-1.2-.2C7.3 2 3 3.3 2.5 7 1 17 4.8 29 13 38.6c8 10 19 15.8 29.3 15.8"/>
        </symbol>
        <symbol id="menu-service" viewBox="0 0 49.9 49.9">
            <path d="M16 31.3c-.3-.2-.6-.2-.8 0L4 42.6c-.3.2-.3.5 0 .7h.7L16 32v-.7zM18 34L6.7 45.5c-.2.2-.2.5 0 .7 0 .2.2.2.4.2h.5l11.3-11.4c.2-.2.2-.5 0-.7H18z"/><path d="M36 29.4l-6-6L42.6 11h2.6l.4-.2 4.2-4.2v-.4-.4L44 0h-.6l-4.2 4.3v3L26.4 20l-6-6c1.5-3.7.5-8-2.4-11C15.5.5 11.6-.5 8 .5c-.2 0-.3.2-.3.4v.5l4 4-1.2 5-5 1.2-4-4s-.2-.2-.4 0c-.3 0-.4 0-.5.2-1 3.7 0 7.6 2.7 10.3 3 3 7 3.8 11 2.5l5.8 6-2 1.8c-.7-.5-1.6-.7-2.6-.7-1.5 0-3 .6-4 1.6l-9.8 10c-2 2-2 5.5 0 7.7L3 48.2c1 1 2.4 1.6 4 1.6s2.8-.7 3.8-1.7l10-10c1-1 1.5-2.3 1.5-3.8 0-1-.3-2-.7-2.7l2-2 5.8 6c-1.4 3.8-.4 8 2.5 11 2 2 4.6 3 7.3 3 1 0 2 0 2.8-.4.3 0 .4-.2.4-.4v-.5l-4-4 1.2-5 5-1.2 4 4s.2.2.4 0c.2 0 .3 0 .4-.2 1-3.6 0-7.5-2.7-10.2-2.7-3-7-3.8-10.8-2.4zm-21.2-10h-.5c-3.5 1.4-7.5.5-10.2-2-2-2.3-3-5.5-2.5-8.5L5 12.3s.3.2.5 0L11 11c.2 0 .3-.2.4-.4L13 5s0-.3-.2-.4L9.3 1c3-.5 6.2.5 8.4 2.7 2.7 2.7 3.5 6.7 2 10.2 0 .2 0 .2 6.3 6.5l-5 5-6.2-6zM40 8V4.8L44 1l5 5L45 9.7h-2.5-.4L21.3 31l-2-2L40 7.7zM20 37.5l-9.8 10c-1 .8-2 1.2-3.2 1.2s-2.3-.5-3.2-1.3L2.4 46c-1.8-1.7-1.8-4.5 0-6.3l10-10c.8-.8 2-1.2 3-1.2s2.4.5 3.3 1.3l1.4 1.4c1 1 1.4 2 1.4 3.2 0 1.3-.4 2.4-1.3 3.2zM49 41l-3.5-3.6s-.3-.2-.5 0l-5.5 1.4c-.2 0-.3.2-.4.4l-1.6 5.5v.5l3.6 3.5c-3 .6-6.2-.4-8.4-2.6-2.7-2.6-3.5-6.6-2-10 0-.3 0-.5-.2-.6l-6-6 5-5 6 6s.4.2.5 0c3.4-1.3 7.4-.4 10 2.2 2.4 2 3.3 5.3 2.8 8.3z"/>
        </symbol>
        <symbol id="social-fb" viewBox="0 0 49.652 49.652">
            <path d="M31,25.7h-4.039c0,6.453,0,14.396,0,14.396h-5.985c0,0,0-7.866,0-14.396h-2.845v-5.088h2.845v-3.291c0-2.357,1.12-6.04,6.04-6.04l4.435,0.017v4.939c0,0-2.695,0-3.219,0c-0.524,0-1.269,0.262-1.269,1.386v2.99h4.56L31,25.7z"/>
        </symbol>
        <symbol id="arr" viewBox="0 0 6 9">
            <path id="a" d="M5.3 4.5L1.5.7H1L.8 1v.5L4 4.7l-3.2 3v.5l.4.4h.4L5.3 5v-.3-.2z"/>
        </symbol>
        <symbol id="adv-market" viewBox="0 0 85.8 77.9">
            <path class="st0" d="M7 78c-4 0-7-3.2-7-7 0-4 3-7 7-7h1.5l14-26.4c-1.3-1.3-2-3-2-5 0-4 3-7 7-7s7 3 7 7c0 .8-.2 1.6-.5 2.3l15 10.6c1.2-1 2.6-1.4 4.2-1.4 1.4 0 2.7.4 3.8 1l17-33C72.7 10.7 72 9 72 7c0-3.8 3-7 7-7 3.8 0 7 3 7 7 0 3.8-3.2 7-7 7-1 0-2-.2-2.7-.5L59 47.2c.8 1 1.3 2.5 1.3 4 0 4-3 7-7 7-3.8 0-7-3-7-7 0-1.3.4-2.6 1-3.6L32.5 37.2c-1.3 1.5-3.2 2.4-5.3 2.4-1 0-2-.2-2.7-.6L10.8 65c2 1.3 3.2 3.5 3.2 6 0 4-3 7-7 7zm0-11.5c-2.4 0-4.4 2-4.4 4.4 0 2.4 2 4.3 4.4 4.3 2.5 0 4.4-2 4.4-4.4 0-2.5-2-4.5-4.4-4.5zm46.3-19.8c-2.4 0-4.4 2-4.4 4.4s2 4.5 4.3 4.5c2.5 0 4.4-2 4.4-4.4 0-2.3-2-4.3-4.4-4.3zm-26-18.5c-2.5 0-4.4 2-4.4 4.4 0 2.4 2 4.4 4.3 4.4s4.4-2 4.4-4.4c0-2.4-2-4.4-4.4-4.4zM79 2.5c-2.5 0-4.5 2-4.5 4.4 0 2.4 2 4.3 4.4 4.3 2.3 0 4.3-2 4.3-4.4 0-2.5-2-4.5-4.4-4.5z"/>
        </symbol>
        <symbol id="adv-gear" viewBox="0 0 68.9 66.9">
            <path class="st0" d="M45.7 55.8l3-7-7.4-3c.4-2 .5-4 .2-6l7.2-3-2.8-7-7.2 3c-1-1.7-2.6-3.2-4.3-4.5l3-7-7-3-2.8 7c-2-.4-4.2-.4-6.2 0l-2.8-7-7 2.8 3 7.2c-1.8 1.2-3.2 2.6-4.4 4.4l-7-3L0 36.3l7.4 3c-.3 2-.3 4 0 6l-7.4 3 2.8 7 7.6-3c1 1.4 2.4 2.8 4 4L11 63.8l7 2.8 3.3-7.7c2 .4 3.8.5 5.6.2l3 7.7 7-3-3-7.6c1.5-1 3-2.4 4-4l7.7 3.4zm-25-4.5c-5-2-7.4-8-5.3-13 2-5 8-7.3 13-5.2 5 2.2 7.3 8 5.2 13-2.2 5-8 7.4-13 5.3zM69 15.5v-4h-4.4c-.2-1.2-.6-2.2-1.2-3l3-3-3-3-2.8 3c-1-.7-2-1.2-3-1.4V0h-4.2v4c-1 .3-2.2.8-3 1.4l-3-3-3 3 3 3c-.6.8-1 2-1.2 3H42v4h4.3c.3 1.2.7 2 1.3 3l-3 3 2.8 3 3.2-3.2c1 .5 1.8 1 3 1V27h4v-4.5c1-.2 2-.6 2.8-1.2l3.2 3.3 3-3-3.3-3c.6-1 1-2 1.2-3H69v.2zm-13.6 3.2c-3 0-5.4-2.4-5.4-5.4S52.4 8 55.4 8s5.3 2.3 5.3 5.3-2.4 5.4-5.3 5.4z"/>
        </symbol>
        <symbol id="adv-letter" viewBox="0 0 73 86">
            <path class="st0" d="M6 86h44c-1-.6-2-1.2-3-2H6c-2.2 0-4-1.8-4-4V6c0-2.2 1.8-4 4-4h31c1 0 2 .4 2.8 1.2C40.5 4 41 5 41 6v10c0 3.3 2.7 6 6 6h12c1 0 1.8.3 2.5 1l.3.2C62.5 24 63 25 63 26v28.8l2 .8V26c0-1.6-.6-3-1.8-4.2l-.4-.4L41.2 1.7C40.2.7 38.6 0 37 0H6C2.7 0 0 2.7 0 6v74c0 3.3 2.7 6 6 6zM43 6l15.3 14H47c-2.2 0-4-1.8-4-4V6z"/><path class="st0" d="M12.5 21h15c.6 0 1-.4 1-1s-.4-1-1-1h-15c-.6 0-1 .4-1 1s.4 1 1 1zM12.5 33h28c.6 0 1-.4 1-1s-.4-1-1-1h-28c-.6 0-1 .4-1 1s.4 1 1 1zM51.5 37c0-.6-.4-1-1-1h-38c-.6 0-1 .4-1 1s.4 1 1 1h38c.6 0 1-.4 1-1zM48.5 42c0-.6-.4-1-1-1h-35c-.6 0-1 .4-1 1s.4 1 1 1h35c.6 0 1-.4 1-1zM12.5 48h12c.6 0 1-.4 1-1s-.4-1-1-1h-12c-.6 0-1 .4-1 1s.4 1 1 1zM12.5 66h8c.6 0 1-.4 1-1s-.4-1-1-1h-8c-.6 0-1 .4-1 1s.4 1 1 1zM12.5 71h15c.6 0 1-.4 1-1s-.4-1-1-1h-15c-.6 0-1 .4-1 1s.4 1 1 1zM58 56c-8.3 0-15 6.7-15 15s6.7 15 15 15 15-6.7 15-15-6.7-15-15-15zm9 16l-2.8.7c0 .5-.4 1-.6 1.5l1.5 2.4-1.4 1.5-2.4-1.4-1.5.6L59 80h-2l-.6-2.8c-.5 0-1-.4-1.5-.6L52.4 78 51 76.7l1.5-2.4-.6-1.5-3-.6v-2l2.8-.6c0-.5.4-1 .6-1.5L51 65.4l1.4-1.5 2.4 1.5 1.5-.6.6-3h2l.6 3c.5 0 1 .3 1.5.5l2.5-1.5 1.5 1.5-1.5 2.4c.3.4.5 1 .6 1.4l3 .6v2z"/><circle class="st0" cx="58" cy="71" r="4"/>
        </symbol>
        <symbol id="adv-study" viewBox="0 0 53.9 62">
            <path class="st0" d="M45.4 39.2L36 35.8c-1.3-.5-2.7-1.7-3-2.6 4.4-4 7-11.5 7-16.2 0-2.3-.6-4.5-1.8-6.5L43 9v14h-2v2h6v-2h-2V8.4L49 7 27 0 4.5 7l11.2 3.5c-1 2-1.8 4.2-1.8 6.5 0 4.7 2.7 12.2 7 16.2-.3 1-1.7 2-3 2.6l-9.4 3.4C3.6 41.2 0 46 0 51.5V62h54V51.5c0-5.5-3.5-10.4-8.6-12.3zM27 2.2L39.5 6H27v2h12.5L27 12 11.3 7 27 2zm0 12l9.3-3c.6 1 1 2 1.4 3.2L27 18l-10.6-3.6c.3-1.2.8-2.2 1.4-3.3l9.2 3zm-11 2L26.8 20l11-3.7C38 23.5 33 34 27 34S15.6 22.5 16 16.2zm6.6 18.4c1.4 1 2.8 1.4 4.4 1.4s3-.5 4.4-1.4c.6 1.2 2 2.2 3 3l-7.5 9-7.7-9c1.4-1 2.7-2 3.3-3zM52 60h-8v-7h-2v7H12v-7h-2v7H2v-8.5c0-4.6 3-8.8 7.3-10.3l8-3L27 49.8l9.5-11.5 8 3C49 42.6 52 46.7 52 51.3V60z"/>
        </symbol>
    </svg>
</div>
<div class="wrapper">

<header class="header">
    <div class="header__container">
        <a href="/" class="header__logo">
            <img src="<?php bloginfo("template_url"); ?>/img/logo.png"  alt="logo" title="логотип">
        </a>
        <script data-b24-form="click/4/zy99qe" data-skip-moving="true"> (function(w,d,u){ var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/180000|0); var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h); })(window,document,'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_4.js'); </script>
        <span class="header__callback">Обратный звонок</span>
        <a href="mailto:info@nerta-sw.ru" class="header__email">info@nerta-sw.ru</a>
        <a href="tel:84956777000" class="header__phone">8 (495) 6-777-000</a>
        <a href="tel:88005552593" class="header__phone">8 (800) 555-25-93</a>
        <button id="header-menu" class="header__menu">
            <img src="<?php bloginfo("template_url"); ?>/img/menu.svg">
            <img src="<?php bloginfo("template_url"); ?>/img/close.svg">
        </button>
    </div>
</header>

<nav class="nav">
    <div class="nav__container">
        <?php if (function_exists(show_custom_menu(0))) show_custom_menu(0); ?>
    </div>
</nav>

<section class="menu" id="menu">
    <div class="menu__container">
        <div class="menu__list">
            <?php if (function_exists(show_custom_menu(0))) show_custom_menu(0); ?>
        </div>
        <a href="mailto:info@nerta-sw.ru" class="menu__email">info@nerta-sw.ru</a>
        <span class="menu__callback js-callback-popup-show">Обратный звонок</span>
        <a href="tel:84956777000" class="menu__phone">8 (495) 6-777-000</a>
        <a href="tel:88005552593" class="menu__phone">8 (800) 555-25-93</a>
        <a href="/" class="menu__logo">
            <img src="<?php bloginfo("template_url"); ?>/img/logo.png">
        </a>
    </div>
</section>
