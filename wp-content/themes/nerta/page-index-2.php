<?php
/**
 * Template Name: New index
 */
get_header(); ?>
<div id="nerta-main-page">
<div class="main-top main-top-new">
    <div class="container">
 <h1><span id="h-inner-1"> Оборудование для моек самообслуживания</span>
    <span id="h-inner-2" >Nerta SW</span></h1>
    <ul id="main-list">

        <li><span>Мойки самообслуживания под ключ</span></li>
        <li><span>Мойки самообслуживания в лизинг</span></li>
        <li><span>Франшиза мойки самообслуживания</span></li>
        <li><span>Купить мойку самообслуживания, готовый бизнес</span></li>
        <li><span>Собственное производство оборудования</span></li>
    </ul>



        <div id="main-img"></div>
                <div id="main-theme"></div>
    </div>
    <span id="working_from">Работаем с 2000<br>
Работаем по РФ и СНГ</span>
</div>
<div class="cost">
    <div class="container">
        <div class="title">
          <h2>Цены на оборудование для моек самообслуживания</h2>
        </div>

        <div class="pagination">
            <div class="pagination-title">Количество постов</div>
            <ul>
                <li class="js-post-count" data-count="1">
                    1
                </li>
                <li class="js-post-count" data-count="2">
                    2
                </li>
                <li class="js-post-count" data-count="3">
                    3
                </li>
                <li class="js-post-count" data-count="4">
                    4
                </li>
                <li  class="js-post-count" data-count="5">
                    5
                </li>
                <li  class="js-post-count" data-count="6">
                    6
                </li>
                <li class="js-post-count" data-count="7">
                    7
                </li>
                <li class="js-post-count" data-count="8">
                    8
                </li>
            </ul>
        </div>

        <div class="costtable">
            <div class="costtable-dop">
                <div class="costtable-dop-title">Дополнительно</div>
                <label class="pagination-checkbox checkbox">
                    <input type="checkbox" name="montag" class="js-post-montag"> <span>Монтаж&nbsp;—&nbsp;<span class="strong"><span class="js-post-montag-cost"></span>&nbsp;&euro;</span></span>
                </label>
                <label class="pagination-checkbox checkbox">
                    <input type="checkbox" name="dop" class="js-post-dop"> <span>Система водоподготовки и&nbsp;обратного осмоса, включая монтаж&nbsp;—&nbsp;<span class="strong"><span class="js-post-dop-cost"></span>&nbsp;&euro;</span></span>
                </label>
            </div>
            <ul>
                <li class="start">
                    <div class="costtable-item js-costtable-btn" data-type="start">
                        <div class="costtable-text">
                            Start
                            <span><span class="js-post-cost" data-type="start"></span>&nbsp;&euro;</span>
                        </div>
                    </div>
                    <div class="costtable-compare js-costtable-compare" data-type="start">
                        Сравнить<svg class="costtable-arr"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arr"></use></svg>
                    </div>
                </li>
                <li class="base">
                    <div class="costtable-item js-costtable-btn" data-type="base">
                        <div class="costtable-text">
                            Base
                            <span><span class="js-post-cost" data-type="base"></span>&nbsp;&euro;</span>
                        </div>
                    </div>
                    <div class="costtable-compare js-costtable-compare" data-type="base">
                        Сравнить<svg class="costtable-arr"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arr"></use></svg>
                    </div>
                </li>
                <li class="light">
                    <div class="costtable-item">
                        <div class="costtable-text js-costtable-btn" data-type="light">
                            Light
                            <span><span class="js-post-cost" data-type="light"></span>&nbsp;&euro;</span>
                        </div>
                    </div>
                    <div class="costtable-compare js-costtable-compare" data-type="light">
                        Сравнить<svg class="costtable-arr"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arr"></use></svg>
                    </div>
                </li>
                <li class="optima">
                    <div class="costtable-item js-costtable-btn" data-type="optima">
                        <div class="costtable-text">
                            Optima
                            <span><span class="js-post-cost" data-type="optima"></span>&nbsp;&euro;</span>
                        </div>
                    </div>
                    <div class="costtable-compare js-costtable-compare" data-type="optima">
                        Сравнить<svg class="costtable-arr"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arr"></use></svg>
                    </div>
                </li>
                <li class="maxi">
                    <div class="costtable-item js-costtable-btn" data-type="maxi">
                        <div class="costtable-text">
                            Maxi
                            <span><span class="js-post-cost" data-type="maxi"></span>&nbsp;&euro;</span>
                        </div>
                    </div>
                    <div class="costtable-compare js-costtable-compare" data-type="maxi">
                        Сравнить<svg class="costtable-arr"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arr"></use></svg>
                    </div>
                </li>
            </ul>
        </div>

        <div class="dop">
                <ul class="dop-list">
                    <li class="dop-item">
                        <div class="dop-img">
                            <img src="<?php bloginfo("template_url"); ?>/img/options/44.jpg" alt="Система
водоподготовки">
                        </div>
                        <div class="oh-2">
                            <div class="dop-title"><h3>Система водоподготовки автомойки</h3></div>
                            <p>2&nbsp;фильтра умягчения
                            непрерывного действия</p>
                            <p>Контейнер для таблетированной соли</p>
                            <p>Установка фильтрации
                            непрерывного действия</p>
                            <p>Ёмкость с&nbsp;системой автоматики</p>
                            <p class="dop-cost">Цена: <span class="js-post-dop-price" data-type="preparation"></span> &euro;</p>
                            <ul style="padding-left: 18px;"><li>Полностью очищает воду от солей и хлора.</li>
                                    <li>Вода при высыхании не оставляет разводов.</li>
                                    <li>После мойки протирки кузова не требуется.</li>
                                    <li>Финишная мойка, ополоснул и поехал.</li>
                                </ul>
                            

<br><br>
                        </div>

                    </li>
                    <li class="dop-item">
                        <div class="dop-img">
                            <img style='min-height:300px;' src="<?php bloginfo("template_url"); ?>/img/options/55.jpg" alt="Система
обратного осмоса">
                        </div>
                        <div class="oh">
                            <div class="dop-title"><h3>Система обратного осмоса мойки</h3></div>

                            <p>Рама из&nbsp;нержавеющей стали</p>
                            <p>Система обратного осмоса
                            производительностью 750 литров в&nbsp;час</p>
                            <p>Электронная панель управления
                            с&nbsp;системой контроля качества воды</p>
                            <p>Автоматическая система отключения
                            осмоса при низком уровне воды
                            с&nbsp;информированием на&nbsp;табло</p>
                            <p class="dop-cost">Цена: <span class="js-post-dop-price" data-type="osmosis"></span> &euro;</p>
                            <ul style="display: block;padding-left: 18px;">
                                                
                                    <li>Очищает воду от механических загрязнений (песок; ржавчина) — защищает моечные аппараты от повреждений.</li>
                                    <li>Нормализует химический состав воды — снижает расход моечной химии.</li>
                                </ul>
                        </div>


                    </li>
                </ul>
        </div>

        <div class="costcompare">

        </div>
    </div>
</div>
<!--  -->

<div class="container" style="margin-bottom: 69px;clear:both;">
    <div class="options-wrapper">
        

<div class="title"><h2>Дополнительное оборудование для автомоек</h2></div>
        <div id="img-6" style="max-width: 598px;">
           <img style="width:100%;" src='<?php bloginfo("template_url"); ?>/img/options/ZEE00013.jpg'>
        </div>

        <div class="options-text left sans" id="text-1" style="">
<div style="margin-left: 25px;">
                <div class="left-child">
                <h3 class="b">Пылесос самообслуживания EWA на 1 пост</h3>
                <span>Мощность:<span class="grey"> 2,5 кВт</span></span><br>
Поток воздуха пылесоса: <span class="grey"> 180 м3/час</span><br>
Вид питания: <span class="grey"> 3×400 ВV 50hz</span><br>

<span class=" value">Цена:<span class="grey value"> 2500 €</span></span>
            </div>
            <hr>
            <div class="left-child">
                <h3 class="b">Пылесос самообслуживания EWA на 2 поста</h3>
                <span>Мощность: <span class="grey"> 2 x 2,5 кВт</span><br>
Поток воздуха пылесоса:<span class="grey"> 2 x 180 м3/час</span><br>
Вид питания:<span class="grey"> 3×400 ВV 50hz</span><br>

<span class=" value">Цена:<span class="grey value"> 5000 €</span><br></span>
</span>
            </div>

</div>

</div>



        <br>
        <br>

        <div class="options-text" style="clear: both;">
            <p></p>
        </div>
   </div>

</div>

<!--  -->

<!--  -->
<div class="feedback" style="">
   <div class="container">
       <div class="feedback-content">
           <?php echo do_shortcode( '[contact-form-7 id="12" title="Callback"]' ); ?>
       </div>
   </div>
</div>

<div class="options" style="padding:0px;">
<div class="container">
    <div class="options-wrapper">
    <div class="title" style="padding-top: 50px;"><h2>Рама с силовым оборудованием автомойки</h2></div>

        <img  src="<?php bloginfo("template_url"); ?>/img/options/rama.jpg" alt="Рама с&nbsp;силовым оборудованием">
        <div class="options-text" style="padding: 65px 0px;max-width:100%;">
            <p>Изготовлена из нержавеющей стали марки Aisi 304, что делает ее надежной и долговечной. Конструкция рамы разработана с доступом к каждому элементу.  Сокращает время на техническое обслуживание и ремонтные работы оборудования. Независимые друг от друга посты с индивидуальным управлением. Возможна сборка комплектации по техническому заданию. Для тех, кто знает, что выбрать.</p>
        </div>
<div class="title"><h2>Панель управления мойки самообслуживания</h2></div>

        <div class="options-text left" id="text-1" >
<div>
            <p>Комплектуется платежными терминалами, работающими по 54-ФЗ.
Клиенту не нужно никуда ходить, выбрал программу, оплатил и<br> приступил к мойке автомобиля. Позволяет увеличить пропускную способность автомойки.
</p><br>
<p>
Дизайн панели может быть выполнен в фирменном стиле заказчика.
</p><br>
<ul>
    <li>Выполнена из нержавеющей стали высшего класса толщиной 1,5мм.</li>
    <li>Комплектуется платежными терминалами, работающими по 54-ФЗ.</li>
    <li>Без контактная оплата : Appel Pay; Samsung Pay; Google Pay</li>
    <li>Дизайн панели может быть выполнен в фирменном стиле заказчика.</li>
</ul>

<p>
Совмещая панель управления и платёжный терминал, вы увеличиваете пропускную способность мойки.  Клиенту не нужно никуда ходить, </br>выбрал программу, оплатил и приступил к мойке автомобиля.
</p>
</div>

</div>
        <div id="img-5" style="max-width:580px;">
         <img src='<?php bloginfo("template_url"); ?>/img/options/ZEE09993.jpg'>
        </div>

        <br>
        <br>

        <div class="options-text" style="clear: both;">
            <p></p>
        </div>
        <div class="panel">
            <div class="panel-item">
                <img src="<?php bloginfo("template_url"); ?>/img/options/2233.png" alt="Комплектация Maxi">
                <div class="panel-item-title">Комплектация Maxi</div>
            </div>
            <div class="panel-item">
                <img src="<?php bloginfo("template_url"); ?>/img/options/22333.png" alt="Light / Optima">
                <div class="panel-item-title">Light / Optima</div>
            </div>
            <div class="panel-item last-child">
                <img src="<?php bloginfo("template_url"); ?>/img/options/panel3.jpg" alt="Basic / Start">
                <div class="panel-item-title">Basic / Start</div>
            </div>
            <div class="panel-payment">
                <div class="panel-payment-title">Формы оплаты</div>
                <ul>
                    <li>&mdash;&nbsp;Мобильные телефоны (Apple/Samsung/Google pay)</li>
                    <li>&mdash;&nbsp;Купюры</li>
                    <li>&mdash;&nbsp;Монеты</li>
                    <li>&mdash;&nbsp;Банковские карты</li>

                    <li>&mdash;&nbsp;Карты лояльности</li>
                    <li>&mdash;&nbsp;Чип-ключи</li>
                </ul>
            </div>
        </div>
        <div class="clearfix">
            <img style="padding-right:130px;"  src="<?php bloginfo("template_url"); ?>/img/options/22.png" alt="Программы панели управления" class="options-text-panel">
            
            <div class="options-text subtitle"><h2>Основные программы автомойки самообслуживания</h2></div>
            <div class="options-text">
                <p>
                    <span class="b">&laquo;Пена&raquo;</span><br>
                    работает с&nbsp;холодной водой, при пониженном давлении в&nbsp;магистрали, смесь воды, вспененного шампуня и&nbsp;воздуха поступает к&nbsp;пистолету, предназначенному для нанесения пены.
                </p>
                <br>
                <p>
                    <span class="b">&laquo;Вода+Пена&raquo;</span><br>
                    Для полной мойки автомобиля с&nbsp;использованием только одного инструмента. Смесь воды и&nbsp;шампуня поступает по&nbsp;магистрали к&nbsp;пистолету, предназначенному для высокого давления.
                </p>
                <br>
                <p>
                    <span class="b">&laquo;Холодная вода&raquo;, &laquo;Горячая вода&raquo;</span><br>
                    Холодная или горячая вода под высоким давлением, поступает по&nbsp;магистрали
                    к&nbsp;пистолету, предназначенному для высокого давления. Предназначена
                    для ополаскивания автомобиля после мойки с&nbsp;шампунем или после нанесения пены.
                </p>
                <br>
                <p>
                    <span class="b">&laquo;Воск&raquo;</span><br>
                    Работает с&nbsp;холодной водой, под высоким давлением в&nbsp;магистрали смесь воды
                    и&nbsp;воска поступает к&nbsp;пистолету, предназначенному для высокого давления. Предназначена для нанесения защитного слоя полимерного воска.
                </p>
                <br>
                <p>
                    <span class="b">&laquo;Пауза&nbsp;/ Стоп&raquo;</span><br>
                    Остановка любой из&nbsp;выбранных программ, например, для смены пистолета
                    или других действий.
                </p>
            </div>
            <div style="float:right; width: 324px; margin-right: 40px; clear:both;">

            <img style="padding-right:30px; padding-top:120px;"  src="<?php bloginfo("template_url"); ?>/img/options/2q.jpg" alt="" class="options-text-panel">
            <img style="padding-right:30px; padding-top:60px;"  src="<?php bloginfo("template_url"); ?>/img/options/11q1.jpg" alt="" class="options-text-panel">
            </div>

            <div class="options-text subtitle"><h3>Дополнительные программы мойки</h3></div>
            <div class="options-text">
                <p>
                    <span class="b">&laquo;Осмос&raquo;</span><br>
                    Работает под высоким давлением холодной водой, подготовленной установкой обратного осмоса. Деминерализованная вода поступает к&nbsp;пистолету, предназначенному для высокого давления. Предназначена для удаления
                    с&nbsp;лакокрасочного покрытия автомобиля остаточных солей жесткости. Полностью исключает появление разводов на&nbsp;кузове автомобиля, после применения данной функции машину можно не&nbsp;протирать.
                </p>
                <br>
                <p>
                    <span class="b">&laquo;Воздух&raquo;</span><br>
                    Обдув мощным потоком воздуха труднодоступные места автомобиля.
                </p>
                <br>
                <p>
                    <span class="b">&laquo;Мойка дисков&raquo;</span><br>
                    Нанесение на&nbsp;автомобильные диски специального моющего средства, которое помогает избавиться от&nbsp;сильного загрязнения и&nbsp;тормозной пыли.
                </p>
                <br>
                <p>
                    <span class="b">&laquo;Средство от насекомых&raquo;</span><br>
                    Нанесение специального химического состава против следов от&nbsp;мошки, мух и&nbsp;других насекомых на&nbsp;поверхности транспортного средства.</p>
                <br>
                <p>
                    <span class="b">&laquo;Щетка&raquo;</span><br>
                    Альтернативное нанесение пены с&nbsp;помощью специальной щетки на&nbsp;транспортное средство, для удаления более глубоких загрязнений.
                </p>
            </div>
        </div>
    </div>
</div>
<div class="application">
    <div class="container">
         <img src="<?php bloginfo("template_url"); ?>/img/options/application.jpg" alt="Мониторинг">
    </div>
</div>




<div class="container">
    <div class="options-wrapper">
        
</br>
<div class="title" style="margin:0px !important;"><h2>Мониторинг аппаратов самообслуживания</h2></div>
</br></br>
        <div class="options-text left" id="text-1" style="">
<div>
            <p>Нами разработана система удаленного контроля и управления комплексом самообслуживания. Каждый пост оборудования снабжен GSM-модулем, что позволяет контролировать работу всего комплекса из любой точки мира, где есть доступ к интернету. Вы всегда будете в курсе технического и финансового состояния вашей автомойки.
</p></br>
<p>
Круглосуточный доступ к информации:
</p></br>
<ul>
    <li>Сумма выручки с каждого поста или количеству проданных жетонов;</li>
    <li>Время работы каждой программы;</li>
    <li>Расход и остаток используемых химических препаратов;</li>
    <li>Текущие затраты на электроэнергию, воду, химию и т.д.;</li>
    <li>Управление стоимостью минут каждой программы;</li>
    <li>Детализация любой информации за необходимый период времени по почте или смс.</li>

</ul>

<p>
Доступ к личному кабинету с любого устройства подключённому к интернету. Используется web–интерфейс, не нужно устанавливать никаких программ.
</p>
</br>
<p>
Управляйте бизнесом из любой точки земного шара.
</p>


</div>

</div>

        <div id="img-6" class="ex-img" style="margin-left: 40px; ">
<!--             <img src='<?php bloginfo("template_url"); ?>/img/options/result.jpg'> -->
<img class="ex-img-in"  src="<?php bloginfo("template_url"); ?>/img/options/result.jpg" alt="Финансовый отчет">

<!-- The Modal -->

        </div>
        <div class="options-text" style="clear: both;">
            <p></p>
        </div>
   </div>

</div>

<div class="container">
    <div class="options-wrapper">
        

<div class="title" style="padding-top: 40px;"><h2>Карты лояльности автомойки самообслуживания</h2></div>

        <div class="options-text left" id="text-1" style="max-width: 830px; padding-top: 30px;">
<div>
            <p>Оплата бесконтактными картами лояльности. Привязывает клиентов к конкретным мойкам. По желанию карта используется для начисления баллов в зависимости от суммы разового пополнения. Пополнять карту можно на мойке и через интернет. Сервер статистики помогает отслеживать клиентам баланс, частоту пополнения, посещения моек, выбора программ. Дизайн карты может быть выполнен в  фирменном стиле автомойки.
</p>



</div>

</div>

        <div id="img-6" class="discc" style="width:190px;
     float: right;padding-right: 45px;">
            <img  style="" src='<?php bloginfo("template_url"); ?>/img/options/discount.jpg'>
        </div>


        <div class="options-text" style="clear: both;">
            <p></p>
        </div>
   </div>

</div>

<div class="container">
    <div class="options-wrapper">
        

<div class="title"><h2>Ваши преимущества при работе с компанией Нерта:</h2></div>

        <div class="options-text left" id="text-1" style="">
<div style="padding-right:25px; margin-bottom:15px;">
            <p>
                <h3 class="b" style="font-size: 18px;">Надёжное и качественное оборудование</h3></br>

Не используем дешёвые, низкокачественные комплектующие при производстве постов, аппаратов и строительстве автомоек. Только проверенные комплектующие, который проходят 100% входной контроль! Заработайте на отсутствии простоев мойки!
 </br></br>
<h3 class="b" style="font-size: 18px;">Продвинутая программа мониторинга и управления автомойкой</h3></br>

Управляйте бизнесом из любой точки мира с помощью мобильного телефона/ноутбука. Фирменная программа мониторинга и управлением автомойкой, написана под наше оборудование. Программное обеспечение постоянно обновляется и дорабатывается. Контроль и оперативное управление автомойкой, минимизирует издержки!
</br></br>
<h3 class="b" style="font-size: 18px;">Оригинальная программа лояльности клиентов автомойки самообслуживания</h3></br>

Благодаря оригинальной программе лояльности, у вас будит вся необходимую информацию о клиентах, для проведения маркетинговых акций направленных на привлечения клиентов. Помогает в низкий сезон. Увеличьте трафик автомойки, увеличится прибыль!

</p>



</div>

</div>

        <div id="img-6" style="max-width: 598px;">
            <img style="max-width: 584px;float:left; width: 100%;" src="<?php bloginfo("template_url"); ?>/img/options/ZEE00698.jpg">
            <img style="display:inline-block; max-width:48.9%; float:left; padding-top: 12px;" src="<?php bloginfo("template_url"); ?>/img/options/11.jpg">
            <img style="display:inline-block; max-width:48.9%; float:left; padding-top: 12px; padding-left: 1.91%;" src="<?php bloginfo("template_url"); ?>/img/options/111.jpg">

        </div>

        <br>
        <br>

        <div class="options-text" style="clear: both;">
            <p></p>
        </div>
   </div>

</div>

<div class="container">
    <div class="options-wrapper">
        

<div class="title"><h2>Автомойка самообслуживания - выгодный бизнес</h2> </div>
        <div id="img-6" style="max-width: 598px; padding-top:10px;">
            <img style="width:100%;" src="<?php bloginfo("template_url"); ?>/img/options/ZEE00243.jpg">
        </div>

        <div class="options-text left" id="text-1" style="">
<div style="margin-left: 25px;">
            <p>Это высокорентабельный бизнес, с высокой степенью автоматизации. Рынок России переживает бум строительства автомоек самообслуживания. Ежегодно вводится в эксплуатацию более 1000 постов. С каждым годом удобство и надёжность моечного оборудования выходит на первое место. </p>
    </br>        <p>Средний срок окупаемости автомойки, нашего производства, составляет 18 месяца. Средний срок строительства 4 постовой автомойки 2-3 месяца.
</p></br><p>
На Российском рынке представлено много компаний, занимающихся продажей и производством автомоек. К сожалению, только в нескольких компаний в РФ имеются компетенции для производства и строительства рентабельных автомоек. 
Выбирайте в подрядчики только надёжные и проверенные временем компании. Фирмы с большим количеством выполненных проектов.
</p>




</div>

</div>



        <br>
        <br>

        <div class="options-text" style="clear: both;">
    
        </div>

   </div>

</div>
<div style="background-color: #f3f3f3;width:100%; margin-top: 60px;">
    <div class="container" style="padding:1px;">
<div class="title">Рентабельность мойки самообслуживания зависит от:</div>
<div class="icon-block">
<div class="icon ic-1"> <img style="" src="<?php bloginfo("template_url"); ?>/img/options/icon1.png"></div>
<div class="icon ic-1 ic-2"> <img style="" src="<?php bloginfo("template_url"); ?>/img/options/icon2.png"></div>
<div class="icon ic-1 ic-3"> <img style="" src="<?php bloginfo("template_url"); ?>/img/options/icon3.png"></div>
<div class="icon ic-1 ic-4"> <img style="" src="<?php bloginfo("template_url"); ?>/img/options/icon4.png"></div>

<ul id="ul-ic">
    <li>Место расположение</li>
    <li>Качество оборудования</li>
    <li>Грамотное</br> проектирование</li>
    <li>Строительство</br> моечного комплекса</li>
<div style="clear:both;"></div>
</ul>
</div>
<div class="sec-icon-block">
<div class="icon ic-1"> <img style="" src="<?php bloginfo("template_url"); ?>/img/options/icon1.png">
<span>Место расположение</span></div>

<div class="icon ic-1 ic-2"> <img style="" src="<?php bloginfo("template_url"); ?>/img/options/icon2.png">
<span>Качество оборудования</span></div>

<div class="icon ic-1 ic-3"> <img style="" src="<?php bloginfo("template_url"); ?>/img/options/icon3.png">
<span>Грамотное</br> проектирование</span></div>

<div class="icon ic-1 ic-4"> <img style="" src="<?php bloginfo("template_url"); ?>/img/options/icon4.png">
<span>Строительство</br> моечного комплекса</span></div>


<div style="clear:both;"></div>

</div>
    </div>
</div>

<div class="container">
    <div class="options-wrapper">
        

<div class="title" style="line-height:44pt;"><h2>Как правильно выбрать подрядчика при заказе оборудования и строительстве автомойки?</h2></div>
        <div class="options-text left" id="text-1" style="">
<div style="padding-right:  60px;margin-bottom:80px;">

<ul>
    <li>Изучите портфолио, выполненных компанией-подрядчиком. Посетите пару, тройку объектов, посмотрите, как работают автомойки, по общайтесь с администрацией автомойки</li>
    <li>Обратите внимание на оборудование постов автомойки. Запросите спецификацию на комплектующие. Комплектующие должны быть от известных производителей. Не рекомендуем использовать комплектующие, собранное на компонентах от неизвестных производителей.</li>
    <li>Рекомендуем посетить производство, где собирается аппараты для автомойки. Оцените культуру и загруженность производства. Только при наличии культуры производства получаются качественные аппараты для мойки самообслуживания.</li>
    <li>Обратите внимание на цены постов и аппаратов автомоек. Остерегайтесь низких цен! Бесплатный сыр только в мышеловке! При покупке дешевого, низкокачественного оборудования вы получите проблемы при эксплуатации мойки, а как следствие простои постов, потеря прибыли.</li>
</ul>


</div>

</div>

                <div id="img-6" style="max-width: 598px;">
<img src="<?php bloginfo("template_url"); ?>/img/options/ZEE01010.jpg">
        </div>


        <div class="options-text" style="clear: both;">
            <p></p>
        </div>
   </div>

</div>

<div class="container">
    <div class="options-wrapper">
        


        <div id="img-6" style="max-width: 598px;">
<img src='<?php bloginfo("template_url"); ?>/img/options/ZEE01023.jpg'>
        </div>

        <div class="options-text left" id="text-1" style="">
<div style="margin-left: 31px;">
    <ul>
        <li>Изучите систему дистанционного мониторинга и управления автомойкой. Без функциональной системы мониторинга и управления мойкой самообслуживания, вы не сможете контролировать и управлять процессами, что приведёт к увеличению затрат, простою оборудованию, а возможно, и воровству персонала.</li>
        <li>Обратите внимание на разработанную систему лояльности клиентов автомойки самообслуживания. Вы должны владеть полной информацией о вашем клиенте (ИФО; тел;email). Данная информация необходима для проведения маркетинговых кампаний, для привлечения клиентов. Помогает в низкий сезон.</li>
    </ul>
<p>Остались вопросы, звоните или оставляйте заявку на сайте. Мы ответим на все интересующие вас вопросы! </p>

</div>

</div>


        <br>
        <br>

        <div class="options-text" style="clear: both;">
            <p></p>
        </div>
   </div>

</div>



</div>

<div style="background-color: #f3f3f3;width:100%; margin-top: 60px;">
    <div class="container">
        <div class="title" style="padding-top:69px;padding-bottom:57px; text-align:center; margin:0px !important;font-size: 32px;"><h2>Купите мойку самообслуживания по правильной цене в компании Nerta-SW!</h2></div>
    </div>
</div>
<div>

    <script>
//--------------------------------
if(window.innerWidth<850)
{
  document.querySelector('.cost').appendChild(document.querySelector('.dop'));
}
////--------------------------------
</script>
<?php get_footer(); ?>