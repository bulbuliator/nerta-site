<?php
/**
 * Template Name: equipment basic
 */
get_header(); ?>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/basic.css">

    <section class="chief"
             style="background-image: url('<?= get_template_directory_uri(); ?>/img/equipment_basic/main.jpg')">
        <div class="chief__container">
            <div class="breadcrumbs">
                <?php the_breadcrumb() ?>
            </div>
            <h1 class="chief__title">
                Комплектация <br>
                BASIC
            </h1>
            <div class="chief__description">
                <p class="chief__description__information">
                    Все необходимое для запуска мойки.
                    Основное отличие от более дорогих комплектаций –
                    система нанесения пены. Пена подаётся под высоким давлением.
                </p>
                <script data-b24-form="click/4/zy99qe" data-skip-moving="true"> (function (w, d, u) {
                        var s = d.createElement('script');
                        s.async = true;
                        s.src = u + '?' + (Date.now() / 180000 | 0);
                        var h = d.getElementsByTagName('script')[0];
                        h.parentNode.insertBefore(s, h);
                    })(window, document, 'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_4.js'); </script>
                <button class="chief__description__button">Оформить заявку</button>
            </div>
        </div>
    </section>

    <section class="small__advantages">
        <div class="small__advantages__container">
            <div class="small__advantages__block">
                <img class="small__advantages__image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_basic/icon1.png" alt="">
                <p class="small__advantages__block__bold_text">
                    Экономьте на стоимости моющего оборудования
                </p>
                <p class="small__advantages__block__normal_text">
                    Технолоия подачи пены под высоким давлением увеличивает расход моющей химии, но сокращает время нанесения до
                    40 секунд и позволяет сэкономить на  моющем оборудовании
                </p>
            </div>
            <div class="small__advantages__block">
                <img class="small__advantages__image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_basic/icon2.png" alt="">
                <p class="small__advantages__block__bold_text">
                    Запустите мойку целиком на нашем оборудовании
                </p>
                <p class="small__advantages__block__normal_text">
                    Комплектация содержит все необходимое для запуска МСО по самой низкой стоимости из представленых
                </p>
            </div>
            <div class="small__advantages__block">
                <img class="small__advantages__image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_basic/icon3.png" alt="">
                <p class="small__advantages__block__bold_text">
                    BASIC – самое выгодное предложение
                </p>
                <p class="small__advantages__block__normal_text">
                    Гарантия на оборудование – 1 год
                </p>
            </div>
        </div>
    </section>

    <section class="equipment__description">
        <div class="equipment__description__container">
            <div class="equipment__description__title">
                <h2>
                    Панель управления программами
                </h2>
            </div>

            <div class="equipment__description__blocks">
                <div class="equipment__description__block1">
                    <img class="block__image_1" src="<?= get_template_directory_uri(); ?>/img/equipment_basic/7.jpg"
                         title="Панель управления basic от производителя оборудования для моек самообслуживания  NERTA-SW"
                         alt="Панель управления basic оборудование  мойки самообслуживания">

                    <div class="equipment__description__block1__information">
                        <div class="equipment__description__block1__column">
                            <p class="equipment__description__subtitle">Моющие программы:</p>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_basic/icon4.png" alt="">
                                <p>пена</p>
                            </div>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_basic/icon5.png" alt="">
                                <p>вода+пена (шампунь)</p>
                            </div>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_basic/icon6.png" alt="">
                                <p>вода под давлением</p>
                            </div>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_basic/icon7.png" alt="">
                                <p>воск</p>
                            </div>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_basic/icon8.png" alt="">
                                <p>стоп/пауза</p>
                            </div>
                        </div>
                        <button class="equipment__description__button">Рассчитать стоимость</button>
                    </div>
                </div>
                <div class="equipment__description__block2">
                    <p class="equipment__description__block2__subtitle">
                        Внешняя панель управления мойкой из нержавеющей стали Aisi 304 с сенсорными клавишами с
                        подсветкой
                    </p>
                    <ul>
                        <li>Моющие программы: пена, Вода+Пена (шампунь), вода под давлением, воск, стоп/пауза</li>
                        <li>Возможно подключить дополнительные программы:
                            воздух, осмос, мойка дисков, средство от насекомых, щетка
                        </li>
                        <li>Купюроприемник ICT (Тайвань)
                        </li>
                        <li>Монетоприемник EU-2 (Китай)
                        </li>
                        <li>Пеноизоляция</li>
                        <li>Теплоизоляция</li>
                        <li>Климат-контроль температуры внутри панели</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="power__equipment">
        <div class="power__equipment__container">
            <div class="power__equipment__title">
                <h2>
                    Силовое оборудование
                </h2>
            </div>
            <div class="power__equipment__block__image">
                <img src="<?= get_template_directory_uri(); ?>/img/equipment_basic/1.jpg"
                     title="Рама с силовым оборудованием  от компании Nerta-SW"
                     alt="Рама с силовым оборудованием для моек самообслуживания|Nerta-SW">
                <div class="power__equipment__block__image__title">
                    <p>Рама из нержавеющей стали Aisi 304</p>
                </div>
            </div>
            <div class="power__equipment__block__list">
                <div class="power__equipment__block__1">
                    <ul>
                        <li>
                            Рама из нержавеющей стали Aisi 204
                        </li>
                        <li>
                            Промышленный насос высокого давления Annovi Reverberi с муфтой, рабочее давление от 15 до
                            200 бар (Италия)
                        </li>
                        <li>
                            Электродвигатель Ravel мощностью 5,5 кВт (Италия)
                        </li>
                        <li>
                            Система дозировки жидких химических реагентов Etatron (Италия)
                        </li>
                    </ul>
                </div>
                <div class="power__equipment__block__1">
                    <ul>
                        <li>
                            Электромагнитный клапан Dunfoss (Дания)
                        </li>
                        <li>
                            Электрический шкаф Schneider Electric (Франция) - 1 на весь комплект оборудования
                        </li>
                        <li>
                            Комплект рукавов высокого давления (Тайвань)
                        </li>
                        <li>
                            Пистолет высокого давления для нанесения пены с насадкой ST75 R+M (Германия)
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </section>

    <section class="dop__description dop__description__blue">
        <div class="dop__description__container dop__description__container__padding ">
            <div class="dop__description__block__img ">
                <img class="dop__description__image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_basic/2.jpg"
                     title="Склад компании Nerta-SW"
                     alt="Склад производителя оборудования для моек самообслуживания|Nerta-SW">
            </div>
            <div class="dop__description__block ">
                <ul>
                    <li>
                        Магистраль подачи воды с шаровым кранами
                    </li>
                    <li>
                        Пистолет высокого давления ST2600 R+M (Германия)
                    </li>
                    <li>
                        Поворотная консоль 1800 мм. из нержавеющей стали
                    </li>
                    <li>
                        Система против замерзания ANTI FROST BASE
                        (датчик температуры, который включает пролив
                        всех магистралей высокого давления, при понижении
                        температуры ниже установленного минимума,
                        например +2 градусов, что позволяет работать
                        при минусовых температурах
                    </li>
                </ul>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Цена
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                5 000 €
                            </div>
                            <div class="block__cost__number3">
                                500 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Монтаж, в независимости от количества постов
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                2 000 €
                            </div>
                            <div class="block__cost__number3">
                                200 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Slider main container -->
    <section class="swiper__content swiper__content__grey">
        <div class="swiper">
            <div class="swiper-button-container">
                <div class="swiper-button-prev swiper-button-prev1"></div>
                <div class="swiper-button-next swiper-button-next1"></div>
            </div>
            <div class="swiper-container swiper-1">
                <div class="swiper-wrapper">
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_basic/s1.png"
                             title="Насос высокого давления (Baisc) для моек самообслуживания|Nerta-SW"
                             alt="Насос высокого давления Annovi Reverberi для моек самообслуживания|Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Насос Annovi Reverberi
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_basic/s2.png"
                             title="Система дозировки для моек самообслуживания |Nerta-SW"
                             alt="Система дозировки Etatron для мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Система дозировки Etatron
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_basic/s3.png"
                             title="Рукав высокого давления для моек самообслуживания |Nerta-SW"
                             alt="Рукав высокого давления для мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Рукав высокого давления
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_basic/s4.png"
                             title="Электродвигатель для моек самообслуживания |Nerta-SW"
                             alt="Электродвигатель Ravel для мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Электродвигатель Ravel
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_basic/s5.png"
                             title="Монетоприёмник для моек самообслуживания |Nerta-SW"
                             alt="Монетоприёмник EU-2 для мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Монетоприёмник EU-2
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_basic/s6.png"
                             title="Электромагнитный клапан для моек самообслуживания |Nerta-SW"
                             alt="Электромагнитный клапан Danfoss  для мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Электромагнитный клапан Danfoss
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_basic/s7.png"
                             title="Купюроприёмник для моек самообслуживания |Nerta-SW"
                             alt="Купюроприёмник ICTдля мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Купюроприемник ICT
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_basic/s8.png"
                             title="Воздушный компрессор для моек самообслуживания |Nerta-SW"
                             alt="Воздушный компрессор Remez для мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Воздушный компрессор Remeza (Италия - Белоруссия)
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_basic/s9.png"
                             title="Система дозировки для моек самообслуживания |Nerta-SW"
                             alt="Дозатор моющего средства SECO для мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Дозатор моющего средства SECO
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dop__description">
        <div class="dop__description__container dop__description__container__margin">
            <div class="dop__description__block__img ">
                <img class="dop__description__image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_basic/3.jpg"
                     title="Силовое оборудование от компании Nerta-SW"
                     alt="Силовое оборудование для моек самообслуживания|Nerta-SW">
            </div>
            <div class="dop__description__block ">
                <div class="dop__description__block__title">
                    <p>Дополнительные опции</p>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Емкость для воды на 1000 литров
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                200 €
                            </div>
                            <div class="block__cost__number3">
                                20 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Бесконтактная оплата с помощью пластиковых карт лояльности
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                200 €
                            </div>
                            <div class="block__cost__number3">
                                20 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Устройство плавного пуска двигателя Schneider Electric
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                200 €
                            </div>
                            <div class="block__cost__number3">
                                20 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Система удаленного контроля и мониторинга
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                300 €
                            </div>
                            <div class="block__cost__number3">
                                30 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Насос подачи воды Wilo
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                300 €
                            </div>
                            <div class="block__cost__number3">
                                30 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Бесконтактная оплата с помощью банковских карт и смартфонов (Apple/Samsung/ GooglePay)
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                400 €
                            </div>
                            <div class="block__cost__number3">
                                40 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Частотный преобразователь Schneider Electric
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                400 €
                            </div>
                            <div class="block__cost__number3">
                                40 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Бойлер косвенного нагрева на 300 литров
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                500 €
                            </div>
                            <div class="block__cost__number3">
                                50 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Slider main container -->
    <section class="swiper__content swiper__content__display">
        <div class="swiper">
            <div class="swiper__content__title">
                <h2>
                    Пример цветового оформления панелей
                </h2>
            </div>
            <div class="swiper-button-container">
                <div class="swiper-button-prev swiper-button-prev2"></div>
                <div class="swiper-button-next swiper-button-next2"></div>
            </div>
            <div class="swiper-container swiper-2">
                <div class="swiper-wrapper">
                    <div class="swiper-slide swiper-slide__margin_bottom swiper-slide_center">
                        <img class="swiper-slide__color_image"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_basic/4.jpg"
                             title="Панель управления start от производителя оборудования для моек самообслуживания  NERTA-SW"
                             alt="Панель управления start для оборудования  мойки самообслуживания –цвет красный">
                    </div>
                    <div class="swiper-slide swiper-slide__margin_bottom swiper-slide_center">
                        <img class="swiper-slide__color_image"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_basic/5.jpg"
                             title="Панель управления start от производителя оборудования для моек самообслуживания  NERTA-SW"
                             alt="Панель управления start для оборудования  мойки самообслуживания –цвет зеленый">
                    </div>
                    <div class="swiper-slide swiper-slide__margin_bottom swiper-slide_center">
                        <img class="swiper-slide__color_image"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_basic/6.jpg"
                             title="Панель управления start от производителя оборудования для моек самообслуживания  NERTA-SW"
                             alt="Панель управления start для оборудования  мойки самообслуживания –цвет синий">
                    </div>
                    <div class="swiper-slide swiper-slide__margin_bottom swiper-slide_center">
                        <img class="swiper-slide__color_image"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_basic/7.jpg"
                             title="Панель управления start от производителя оборудования для моек самообслуживания  NERTA-SW"
                             alt="Панель управления start для оборудования  мойки самообслуживания –цвет пурпурный">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="color__variations">
        <div class="color__variations__container">
            <div class="color__variations__title">
                <h2>
                    Пример цветового оформления панелей
                </h2>
            </div>
            <div class="color__variations__content">
                <img class="swiper-slide__color_image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_basic/4.jpg"
                     title="Панель управления start от производителя оборудования для моек самообслуживания  NERTA-SW"
                     alt="Панель управления start для оборудования  мойки самообслуживания –цвет красный">
                <img class="swiper-slide__color_image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_basic/5.jpg"
                     title="Панель управления start от производителя оборудования для моек самообслуживания  NERTA-SW"
                     alt="Панель управления start для оборудования  мойки самообслуживания –цвет зеленый">
                <img class="swiper-slide__color_image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_basic/6.jpg"
                     title="Панель управления start от производителя оборудования для моек самообслуживания  NERTA-SW"
                     alt="Панель управления start для оборудования  мойки самообслуживания –цвет синий">
                <img class="swiper-slide__color_image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_basic/7.jpg"
                     title="Панель управления start от производителя оборудования для моек самообслуживания  NERTA-SW"
                     alt="Панель управления start для оборудования  мойки самообслуживания –цвет пурпурный">
            </div>
        </div>
    </section>

    <section class="feedback"
             style="background-image: url('<?= get_template_directory_uri(); ?>/img/equipment_basic/feed.jpg')">
        <div class="feedback__container">
            <div class="feedback__title">
                Звоните, инженеры компании помогут с выбором оборудования
            </div>
            <p>
                Сравните характеристики комплектаций, рассчитайте стоимость оборудования
            </p>
            <div class="feedback__phone_number">
                8 (800) 555-25-93
            </div>
            <div class="feedback__button">
                <script data-b24-form="click/4/zy99qe" data-skip-moving="true"> (function (w, d, u) {
                        var s = d.createElement('script');
                        s.async = true;
                        s.src = u + '?' + (Date.now() / 180000 | 0);
                        var h = d.getElementsByTagName('script')[0];
                        h.parentNode.insertBefore(s, h);
                    })(window, document, 'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_4.js'); </script>
                <button class="feedback__button__2">Калькулятор</button>
                <button class="feedback__button__1">Оформить заявку</button>
            </div>
        </div>
    </section>
    <!-- Slider main container -->
    <section class="options__mini">
        <div class="options__mini__container">
            <div class="options__mini__subtitle">
                <h2>
                    Посмотреть другие комплектации
                </h2>
            </div>
            <div class="options__mini__row">

                <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_start/'"
                     class="options__mini__block color_1 shadow_box">
                    <div class="options__mini__block__content">
                        <p class="options__mini__title__big">
                            START
                        </p>
                        <p class="options__mini__title">
                            Для тех кто собирает мойку самостоятельно, на своих компонентах.
                        </p>
                        <ul class="options__mini__list">
                            <li>Панель управления;</li>
                            <li>Электротехнический шкаф;</li>
                            <li>Дозатор моющей химии.</li>
                        </ul>
                    </div>
                    <div class="options__mini__block__cost">
                        <div class="block__cost">
                            <div class="block__cost__container">
                                <div class="block__cost__text">
                                    Цена за 1 пост
                                </div>
                                <div class="block__cost__number">
                                    <div class="block__cost__number1">
                                        2 400 €
                                    </div>
                                    <div class="block__cost__number2">
                                        240 000 ₽
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_light/'"
                     class="options__mini__block color_3">
                    <div class="options__mini__block__content">
                        <p class="options__mini__title__big">
                            LIGHT
                        </p>
                        <p class="options__mini__title">
                            Начальный уровень.
                            Для тех кто экономит с умом.
                        </p>
                        <ul class="options__mini__list">
                            <li>Полный комплект для организации МСО;</li>
                            <li>Пена наносится под низким давлением;</li>
                            <li>Комплектующие начального уровня (Евросоюз, Китай, Россия);</li>
                            <li>Гарантия 1 год.</li>
                        </ul>
                    </div>
                    <div class="options__mini__block__cost">
                        <div class="block__cost">
                            <div class="block__cost__container">
                                <div class="block__cost__text">
                                    Цена за 1 пост
                                </div>
                                <div class="block__cost__number">
                                    <div class="block__cost__number1">
                                        8 000 €
                                    </div>
                                    <div class="block__cost__number2">
                                        800 000 ₽
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_optima/'"
                     class="options__mini__block color_4">
                    <div class="options__mini__block__content">
                        <p class="options__mini__title__big">
                            OPTIMA
                        </p>
                        <p class="options__mini__title">
                            Оптимальное соотношение цена/качества.
                            Для тех кто умеет считать.
                        </p>
                        <ul class="options__mini__list">
                            <li>Полный комплект автомоек самообслуживания;</li>
                            <li>Пена наносится поднизким давлением;</li>
                            <li>Комплектующие среднего уровня (Евросоюз).</li>
                        </ul>
                    </div>
                    <div class="options__mini__block__cost">
                        <div class="block__cost">
                            <div class="block__cost__container">
                                <div class="block__cost__text">
                                    Цена за 1 пост
                                </div>
                                <div class="block__cost__number">
                                    <div class="block__cost__number1">
                                        10 500 €
                                    </div>
                                    <div class="block__cost__number2">
                                        1 050 000 ₽
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_maxi/'"
                     class="options__mini__block color_5">
                    <div class="options__mini__block__content">
                        <p class="options__mini__title__big">
                            MAXI
                        </p>
                        <p class="options__mini__title">
                            Самоё надежное и неприхотливое.
                            Для тех кому лучше идеально, чем дёшево.
                        </p>
                        <ul class="options__mini__list">
                            <li>Полный комплект для мойки самообслуживания;</li>
                            <li>Пена наносится поднизким давлением;</li>
                            <li>Типовые комплектующие (Япония, Канада, Евросоюз);</li>
                            <li>Монтаж;</li>
                            <li>Гарантия 2 года.</li>
                        </ul>
                    </div>
                    <div class="options__mini__block__cost">
                        <div class="block__cost">
                            <div class="block__cost__container">
                                <div class="block__cost__text">
                                    Цена за 1 пост
                                </div>
                                <div class="block__cost__number">
                                    <div class="block__cost__number1">
                                        13 000 €
                                    </div>
                                    <div class="block__cost__number2">
                                        1 300 000 ₽
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="<?php bloginfo("template_url"); ?>/js/gallery.js"></script>


<?php get_footer(); ?>