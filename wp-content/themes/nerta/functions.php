<?php

// @Dinar 24.08.20 add styles and scripts for new pages
// wp_enqueue_style( 'page_1', get_template_directory_uri() . '/page_1.css', false, '1.1', 'all');



if (function_exists('add_theme_support')) {
    add_theme_support('menus');
};

function show_custom_menu() {
    $menu_name = 'main';
    $menu = wp_get_nav_menu_object($menu_name);
    $menu_items = wp_get_nav_menu_items($menu->term_id);
    // print_r($menu_items);
    foreach ((array) $menu_items as $key => $menu_item) {
            $title  = $menu_item->title;
            $url    = $menu_item->url;

            if ($menu_item->menu_item_parent == '0') { 
                $menu_list .='<div class="nav__block">';

                $menu_list .='<a href="'. $url .'">'. $title .'</a>';

                $menu_list .='<div class="nav__block__pod">';
                foreach ((array) $menu_items as $key => $menu_item_2) {

                    if ($menu_item_2->menu_item_parent == $menu_item->db_id) { 
                        $title  = $menu_item_2->title;
                        $url    = $menu_item_2->url;
                        $menu_list .='<a href="'. $url .'">'. $title .'</a>';
                    }
                }
                $menu_list .='</div>';
                $menu_list .='</div>';
            } 
    }

    echo $menu_list;
}

add_action( 'wp_footer', 'mycustom_wp_footer' );
 
function mycustom_wp_footer() {
?>
<script type="text/javascript">
document.addEventListener( 'wpcf7mailsent', function( event ) {
    if ( '12' == event.detail.contactFormId ) {
        yaCounter22985389.reachGoal('callback');
    }
if ( '62' == event.detail.contactFormId ) {
        yaCounter22985389.reachGoal('Feedback');
    }
}, false );

</script>
<?php
}

function the_breadcrumb(){
 
    // получаем номер текущей страницы
    $pageNum = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
 
    $separator = ' '; //  »
 
    // если главная страница сайта
    if( is_front_page() ){
 
        if( $pageNum > 1 ) {
            echo '<a href="' . site_url() . '">Главная</a>' . $separator . $pageNum . '-я страница';
        } else {
            echo 'Вы находитесь на главной странице';
        }
 
    } else { // не главная
 
        echo '<a href="' . site_url() . '">Главная</a>' . $separator;
 
 
        if( is_single() ){ // записи
 
            the_category(', '); echo $separator; the_title();
 
        } elseif ( is_page() ){ // страницы WordPress
            echo '<span>';
                the_title();
            echo '</span>';
        } elseif ( is_category() ) {
 
            single_cat_title();
 
        } elseif( is_tag() ) {
 
            single_tag_title();
 
        } elseif ( is_day() ) { // архивы (по дням)
 
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a>' . $separator;
            echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a>' . $separator;
            echo get_the_time('d');
 
        } elseif ( is_month() ) { // архивы (по месяцам)
 
            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a>' . $separator;
            echo get_the_time('F');
 
        } elseif ( is_year() ) { // архивы (по годам)
 
            echo get_the_time('Y');
 
        } elseif ( is_author() ) { // архивы по авторам
 
            global $author;
            $userdata = get_userdata($author);
            echo 'Опубликовал(а) ' . $userdata->display_name;
 
        } elseif ( is_404() ) { // если страницы не существует
 
            echo 'Ошибка 404';
 
        }
 
        if ( $pageNum > 1 ) { // номер текущей страницы
            echo ' (' . $pageNum . '-я страница)';
        }
 
    }
 
}


add_filter('xmlrpc_enabled', '__return_false');
remove_action('wp_head', 'rsd_link');
add_filter('big_image_size_threshold','_return_false');
add_filter( 'jpeg_quality', create_function( '', 'return 100;' ) );