<?php
/**
 * Template Name: equipment
 */
get_header(); ?>

<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/equipments.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/start.css">

<section class="chief" style="background-image: url('<?= get_template_directory_uri(); ?>/img/equipment_500/main.jpg')">
    <div class="chief__container">
        <div class="breadcrumbs">
            <?php the_breadcrumb() ?>
        </div>
        <h1 class="chief__title">
            Оборудование <br>
            для мойки <br>
            самообслуживания
        </h1>
        <div class="chief__description">
            <p class="chief__description__information">
                Собственное производство. Производим моечное оборудование с 2006 года
            </p>
        </div>
    </div>
</section>
<section class="options">
    <div class="options__container">
        <div class="options__subtitle">
            <h2>
                Оборудование для мойки самообслуживания
                цены
            </h2>
        </div>
        <div class="options__row">

            <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_start/'"
                 class="options__block color_1 shadow_box">
                <h2 class="options__title__big">
                    START
                </h2>
                <h2 class="options__title">
                    Для тех кто собирает мойку самостоятельно, на своих компонентах.
                </h2>
                <ul class="options__list">
                    <li>Панель управления;</li>
                    <li>Электротехнический шкаф;</li>
                    <li>Дозатор моющей химии.</li>
                </ul>

                <div class="options__information">
                    <div class="block__cost">
                        <div class="block__cost__container">
                            <div class="block__cost__text">
                                Цена за 1 пост
                            </div>
                            <div class="block__cost__number">
                                <div class="block__cost__number1">
                                    2 400 €
                                </div>
                                <div class="block__cost__number3">
                                    240 000 ₽
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_basic/'"
                 class="options__block color_2">
                <h2 class="options__title__big">
                    BASE
                </h2>
                <h2 class="options__title">
                    Для тех кто любит дёшево.
                </h2>
                <ul class="options__list">
                    <li>Полный комплект для организации мойки;</li>
                    <li>Пена наносится под высоким давлением;</li>
                    <li>Комплектующие начального уровня (Евросоюз, Китай);</li>
                    <li>Гарантия 1 год.</li>
                </ul>
                <div class="options__information">
                    <div class="block__cost">
                        <div class="block__cost__container">
                            <div class="block__cost__text">
                                Цена за 1 пост
                            </div>
                            <div class="block__cost__number">
                                <div class="block__cost__number1">
                                    5 000 €
                                </div>
                                <div class="block__cost__number3">
                                    500 000 ₽
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_light/'"
                 class="options__block color_3">
                <h2 class="options__title__big">
                    LIGHT
                </h2>
                <h2 class="options__title">
                    Начальный уровень.
                    Для тех кто экономит с умом.
                </h2>
                <ul class="options__list">
                    <li>Полный комплект для организации МСО;</li>
                    <li>Пена наносится под низким давлением;</li>
                    <li>Комплектующие начального уровня (Евросоюз, Китай, Россия);</li>
                    <li>Гарантия 1 год.</li>
                </ul>
                <div class="options__information">
                    <div class="block__cost">
                        <div class="block__cost__container">
                            <div class="block__cost__text">
                                Цена за 1 пост
                            </div>
                            <div class="block__cost__number">
                                <div class="block__cost__number1">
                                    8 000 €
                                </div>
                                <div class="block__cost__number3">
                                    800 000 ₽
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="options__block__image"
                 style="background-image: url('<?= get_template_directory_uri(); ?>/img/equipment_500/1.jpg')">
            </div>
            <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_optima/'"
                 class="options__block color_4">
                <h2 class="options__title__big">
                    OPTIMA
                </h2>
                <h2 class="options__title">
                    Оптимальное соотношение цена/качества.
                    Для тех кто умеет считать.
                </h2>
                <ul class="options__list">
                    <li>Полный комплект автомоек самообслуживания;</li>
                    <li>Пена наносится поднизким давлением;</li>
                    <li>Комплектующие среднего уровня (Евросоюз).</li>
                </ul>
                <div class="options__information">
                    <div class="block__cost">
                        <div class="block__cost__container">
                            <div class="block__cost__text">
                                Цена за 1 пост
                            </div>
                            <div class="block__cost__number">
                                <div class="block__cost__number1">
                                    10 500 €
                                </div>
                                <div class="block__cost__number3">
                                    1 050 000 ₽
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_maxi/'"
                 class="options__block color_5">
                <h2 class="options__title__big">
                    MAXI
                </h2>
                <h2 class="options__title">
                    Самоё надежное и неприхотливое.
                    Для тех кому лучше идеально, чем дёшево.
                </h2>
                <ul class="options__list">
                    <li>Полный комплект для мойки самообслуживания;</li>
                    <li>Пена наносится поднизким давлением;</li>
                    <li>Типовые комплектующие (Япония, Канада, Евросоюз);</li>
                    <li>Монтаж;</li>
                    <li>Гарантия 2 года.</li>
                </ul>
                <div class="options__information">
                    <div class="block__cost">
                        <div class="block__cost__container">
                            <div class="block__cost__text">
                                Цена за 1 пост
                            </div>
                            <div class="block__cost__number">
                                <div class="block__cost__number1">
                                    13 000 €
                                </div>
                                <div class="block__cost__number3">
                                    130 000 ₽
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="preview">
    <div class="preview__container">
        <div class="preview__title">
            <h2>
                Система водоподготовки автомойки
            </h2>
        </div>
        <div class="preview__row">
            <div class="preview__column">
                <ul>
                    <li>2 фильтра умягчения непрерывного действия</li>
                    <li>Контейнер для таблетированной соли</li>
                    <li>Установка фильтрации непрерывного действия</li>
                    <li>Ёмкость с системой автоматики</li>
                </ul>
                <p class="preview__p">
                    Очищает воду от механических загрязнений (песок; ржавчина).
                    Это защищает моечные аппараты от повреждений. Нормализует
                    химический состав воды. Это снижает расход моечной химии.
                </p>
                <button class="preview__button">Подробнее</button>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Система водоподготовки автомойки
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                5 500 €
                            </div>
                            <div class="block__cost__number3">
                                550 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="preview__column__image">
                <img class="preview__image" src="<?= get_template_directory_uri(); ?>/img/equipment_500/2.jpg"
                     alt="система водоподготовки для автомойки"
                     title="Водоподготовка для мойки самообслуживания Nerta-SW">
            </div>
        </div>
    </div>
</section>
<section class="preview">
    <div class="preview__container">
        <div class="preview__title">
            <h2>
                Система обратного осмоса мойки
            </h2>
        </div>
        <div class="preview__row preview__row__reverse">
            <div class="preview__column">
                <ul>
                    <li>Рама из нержавеющей стали</li>
                    <li>Система обратного осмоса производительностью 250-750 литров в час</li>
                    <li>Электронная панель управления с системой контроля качества воды</li>
                    <li>Автоматическая система отключения осмоса при низком уровне воды с информированием на табло</li>
                </ul>
                <p class="preview__p">
                    Полностью очищает воду от солей и хлора. Вода при высыхании не
                    оставляет разводов. После мойки протирки кузова не требуется.
                    Финишная мойка, ополоснул и поехал.
                </p>
                <button class="preview__button">Подробнее</button>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Система обратного осмоса мойки
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                2 500 €
                            </div>
                            <div class="block__cost__number3">
                                250 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="preview__column__image">
                <img class="preview__image" src="<?= get_template_directory_uri(); ?>/img/equipment_500/3.jpg"
                     alt="осмос на автомойке"
                     title="Осмос для мойки самообслуживания Nerta-SW">
            </div>
        </div>
    </div>
</section>
<section class="dop__equipment dop__equipment__display">
    <div class="dop__equipment__container">
        <div class="dop__equipment__title">
            <h2>
                Дополнительное оборудованиe
            </h2>
        </div>
        <div class="dop__equipment__row">
            <div class="dop__equipment__column">
                <div class="">
                    <img src="<?= get_template_directory_uri(); ?>/img/equipment_500/4.jpg"
                         alt="Пылесос самообслуживания для автомойки"
                         title="Пылесос для мойки самообслуживания от производителя Nerta-SW">
                    <div class="dop__equipment__column__title">
                        <p>Чистка салона автомобиля</p>
                    </div>
                    <div class="dop__equipment__column__list">
                        <ul>
                            <li>Пылесосы</li>
                            <li>Химчистка</li>
                            <li>Полироль</li>
                            <li>Чернитель</li>
                            <li>Ковромойка</li>
                        </ul>
                    </div>
                </div>
                <button>Подробнее</button>
            </div>
            <div class="dop__equipment__column">
                <img src="<?= get_template_directory_uri(); ?>/img/equipment_500/5.jpg"
                     alt="Вспомогательное оборудование для мойки самообслуживания"
                     title="Вспомогательное оборудование для моек самообслуживания от производителя Nerta-SW">
                <div class="dop__equipment__column__title">
                    <p>Вспомогательное оборудование</p>
                </div>
                <div class="dop__equipment__column__list">
                    <ul>
                        <li>Приёмник купюр</li>
                        <li>Выдача жетонов</li>
                        <li>Продажа карт лояльности</li>
                        <li>Раковина для мытья рук и тряпок</li>
                    </ul>
                </div>
                <button>Подробнее</button>
            </div>
            <div class="dop__equipment__column">
                <img src="<?= get_template_directory_uri(); ?>/img/equipment_500/6.jpg"
                     alt="Очистные сооружения для автомойки"
                     title="Очистные сооружения для автомойки самообслуживания Nerta-SW">
                <div class="dop__equipment__column__title">
                    <p>Очистные сооружения</p>
                </div>
                <div class="dop__equipment__column__list">
                    <ul>
                        <li>Система рециркуляции «АРОС»</li>
                        <li>Флотационная очистка отработанной воды</li>
                        <li>Очистные установки «Экомойка»</li>
                    </ul>
                </div>
                <button>Подробнее</button>
            </div>
        </div>
    </div>
</section>
<section class="benefits">
    <div class="benefits__container">
        <div class="benefits__title">
            <h2>Преимущества оборудования <span>NERTA-SW </span></h2>
        </div>
        <div class="benefits__content">
            <div class="benefits__content__column_1">
                <div class="benefits__content__block benefits__content__block__column">
                    <div class="benefits__content__block__information">
                        <div class="benefits__subtitle">
                            Надёжные комплектующие
                        </div>
                        <div class="benefits__content__block__text">
                            Для производства используем комплектующие именитых производителей.
                        </div>
                        <div class="benefits__content__block__text">
                            <span class="block_1__bold">Насосы:</span> Wilo; Grundfos
                        </div>
                        <div class="benefits__content__block__text">
                            <span class="block_1__bold">Электродвигатели:</span> Ravel; EME; Mazzoni
                        </div>
                        <div class="benefits__content__block__text">
                            <span class="block_1__bold">Электромагнитные клапана:</span>
                        </div>
                        <div class="benefits__content__block__text">
                            Низкого давления: Danfoss
                        </div>
                        <div class="benefits__content__block__text">
                            Высокого давления: RAPA; BÜRKET; NRT
                        </div>
                        <div class="benefits__content__block__text">
                            <span class="block_1__bold">Дозировка моющей химии:</span> Seko; Etatron
                        </div>
                        <div class="benefits__content__block__text">
                            <span class="block_1__bold">Рукава высокого давления:</span> Semperjet
                        </div>
                        <div class="benefits__content__block__text">
                            <span class="block_1__bold">Купюроприемники:</span> CashCode; Comestero
                        </div>
                        <div class="benefits__content__block__text">
                            <span class="block_1__bold">Монетоприемники:</span> Alberici; Comestero
                        </div>
                    </div>
                </div>
            </div>
            <div class="benefits__content__column_2">

                <div class="benefits__content__block benefits__content__row__column">
                    <div class="benefits__content__block__information">
                        <div class="benefits__subtitle">
                            Корпуса и силовая рама из нержавейки
                        </div>
                        <ul>
                            <li>Нержавеющую сталь – Aisi 304 (Чехия)</li>
                            <li>Толщина листа – 1,5 мм</li>
                            <li>Толщина рамы – 2 мм</li>
                            <li>Гарантия от коррозии – 10 лет</li>
                        </ul>
                    </div>
                </div>
                <div class="benefits__content__block benefits__content__row__column">
                    <div class="benefits__content__block__information">
                        <div class="benefits__subtitle">
                            Индивидуальный подход
                        </div>
                        <div class="benefits__content__block__text">
                            Благодаря собственному производству моечного оборудованияможем собрать
                            комплектацию
                            по
                            техническому заданиюили под бюджет заказчика.
                        </div>
                    </div>
                </div>
                <div class="benefits__content__block benefits__content__row__column">
                    <div class="benefits__content__block__information">
                        <div class="benefits__subtitle">
                            Гарантия
                        </div>
                        <div class="benefits__content__block__text">
                            В зависимости от комплектации от 1 до 2 лет
                        </div>
                    </div>
                </div>
                <div class="benefits__content__block benefits__content__row__column">
                    <div class="benefits__content__block__information">
                        <div class="benefits__subtitle">
                            Оборудование в лизинг
                        </div>
                        <div class="benefits__content__block__text">
                            Все установки можно оформить в лизинг. Работайте на лучшем техоборудовании.
                        </div>
                        <button class="benefits__button" onclick="return location.href = '<?php home_url("template_url"); ?>avtomoika-v-lizing-pod-kluch/'"
                                class="">Подробнее
                        </button>
                    </div>

                </div>
                <div class="benefits__content__block">
                    <div class="benefits__content__block__information">
                        <div class="benefits__subtitle">
                            Эргономичное оборудование
                        </div>
                        <div class="benefits__content__block__text">
                            Производим техоборудование для МСО с 2006 года. Наши моечные комплексы доведены до
                            баланса
                            между
                            надёжностью, экономичностью и удобству эксплуатации. Благодаря собственной сети автомоек
                            самообслуживания (более 15 комплексов), досконально понимаем все процессы и потребности
                            клиентов.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="peculiarities">
    <div class="peculiarities__container">
        <div class="peculiarities__title">
            <h2>Особенности Моечного оборудования NERTA-SW</h2>
        </div>
        <div class="peculiarities__blocks peculiarities__blocks__margin">
            <div class="peculiarities__row">
                <div class="peculiarities__block">
                    <h3 class="peculiarities__block__subtitle">
                        Силовая рама
                    </h3>
                    <p>
                        Производится из нержавеющей стали марки Aisi 304, что делает ее
                        долговечной. Конструкция рамы разработана с доступом к каждому
                        элементу. Сокращает время на техническое обслуживание и
                        ремонтные работы техоборудования. Независимые друг от друга
                        посты с индивидуальным управлением.
                    </p>
                    <p>
                        На раму устанавливаются:
                    </p>
                    <ul>
                        <li>Платы управления</li>
                        <li>Насосы высокого и низкого давления</li>
                        <li>Электромагнитные клапаны</li>
                        <li>Дозаторы моющей химии</li>
                        <li>Электродвигатели</li>
                    </ul>
                    <p>
                        Возможна сборка комплектации по техническому заданию. Для тех, кто знает, что выбрать.
                    </p>
                </div>
                <div class="peculiarities__block__image">
                    <img src="<?= get_template_directory_uri(); ?>/img/equipment_500/7.jpg"
                         alt="Силовая рама для автомойки самообслуживания"
                         title="Силовая рама для  моек самообслуживания от производителя Nerta-SW">
                </div>
            </div>
        </div>
        <div class="peculiarities__blocks">
            <div class="peculiarities__row">
                <div class="peculiarities__block">
                    <h3 class="peculiarities__block__subtitle">
                        Панель управления
                    </h3>
                    <p>
                        Платежный терминал и панель управления 2 в 1. Клиенту не нужно
                        никуда ходить, выбрал программу, оплатил и приступил к мойке
                        автомобиля. Позволяет увеличить пропускную способность
                        автомойки. Увеличивает количество использования дополнительных
                        программ.
                    </p>
                    <ul>
                        <li>Выполнена из нержавеющей стали высшего класса толщиной 1,5 мм</li>
                        <li>Комплектуется платежными терминалами, работающими по 54-ФЗ</li>
                        <li>Любые формы оплаты: наличные; PayPass, Apple Pay, Samsung Pay, Google Pay, карты
                            лояльности
                        </li>
                        <li>Дизайн панели может быть выполнен в фирменном стиле заказчика</li>
                    </ul>
                    <p>
                        Совмещая панель управления и платёжный терминал, вы увеличиваете пропускную способность мойки.
                    </p>
                </div>
                <div class="peculiarities__block__image">
                    <img src="<?= get_template_directory_uri(); ?>/img/equipment_500/8.jpg"
                         alt="Панель управления мойки самообслуживания"
                         title="Панель управления для  автомоек самообслуживания от производителя Nerta-SW">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="remote__control">
    <div class="remote__control__container">
        <div class="remote__control__title">
            <h2>
                Удалённое управление
            </h2>
        </div>
        <div class="remote__control__blocks">
            <div class="remote__control__block">
                <p>
                    Разработана система удаленного контроля и управления комплексом
                    самообслуживания. Каждый пост снабжен GSM-модулем, что позволяет
                    контролировать работу всего комплекса из любой точки мира, где
                    есть доступ к интернету. Вы всегда будете в курсе технического
                    и финансового состояния вашей автомойки.
                </p>
                <p class="remote__control__block__bold">
                    Круглосуточный доступ к информации:
                </p>
                <ul>
                    <li>
                        Сумма выручки с каждого поста или количеству проданных жетонов
                    </li>
                    <li>
                        Время работы каждой программы
                    </li>
                    <li>
                        Расход и остаток используемых химических препаратов
                    </li>
                    <li>
                        Текущие затраты на электроэнергию, воду, химию и т.д.
                    </li>
                    <li>
                        Управление стоимостью минут каждой программы
                    </li>
                    <li>
                        Детализация любой информации за необходимый период времени по почте или смс
                    </li>
                </ul>
                <p>
                    Доступ к личному кабинету с любого устройства подключённого к интернету. Используется web–интерфейс,
                    не нужно устанавливать никаких программ
                </p>
                <p class="remote__control__block__bold">Управляйте бизнесом из любой точки земного шара.</p>
            </div>
            <div class="remote__control__block__image">
                <img src="<?= get_template_directory_uri(); ?>/img/equipment_500/12.jpg"
                     alt="Рабочий стол удалённого управления оборудованием автомойки"
                     title="Рабочий стол удалённого управления оборудованием моек самообслуживания от производителя Nerta-SW">
            </div>
        </div>
    </div>
</section>
<section class="loyalty__card">
    <div class="loyalty__card__container">
        <div class="loyalty__card__block">
            <h2 class="loyalty__card__title">
                Карты лояльности
            </h2>
            <p>
                Оплата бесконтактными картами лояльности. Привязывает клиентов к
                конкретным мойкам. По желанию карта используется для начисления
                баллов в зависимости от суммы разового пополнения. Пополнять
                карту можно на мойке и через интернет. Сервер статистики помогает
                отслеживать клиентам баланс, частоту пополнения, посещения моек,
                выбора программ. Дизайн карты может быть выполнен в фирменном
                стиле автомойки.
            </p>
        </div>
        <div class="loyalty__card__block">
            <img src="<?= get_template_directory_uri(); ?>/img/equipment_500/9.png"
                 alt="Карты лояльности автомойки самообслуживания"
                 title="Карты лояльности для  автомоек самообслуживания от производителя Nerta-SW">
        </div>
    </div>
</section>
<section class="feedbacks">
    <div class="feedbacks__container">
        <div class="feedbacks__block">
            <div class="feedbacks__title">
                <h2>Покупайте техоборудование от производителя</h2>
            </div>
            <p class="feedbacks__text">
                Звоните <span class="feedbacks__text_big">8 (800) 555-25-93</span> или отправляйте запрос на почту <span
                        class="feedbacks__text_big">info@nerta-sw.ru</span>.
            </p>
            <p class="feedbacks__text">
                Специалисты компании ответят на все ваши вопросы. Подберут установки под ваш бюджет.
            </p>
            <script data-b24-form="click/4/zy99qe" data-skip-moving="true"> (function (w, d, u) {
                    var s = d.createElement('script');
                    s.async = true;
                    s.src = u + '?' + (Date.now() / 180000 | 0);
                    var h = d.getElementsByTagName('script')[0];
                    h.parentNode.insertBefore(s, h);
                })(window, document, 'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_4.js'); </script>
            <button class="feedbacks__btn">
                Заказать обратный звонок
            </button>
        </div>
        <div class="feedbacks__block__image">
            <img class="feedbacks__image" src="<?= get_template_directory_uri(); ?>/img/equipment_500/10.jpg"
                 alt="Пылесос самообслуживания для мойки самообслуживания"
                 title="Пылесос самообслуживания для  автомоек самообслуживания от производителя Nerta-SW">
        </div>
    </div>
</section>
<section class="additional__benefits">
    <div class="additional__benefits__container">
        <div class="additional__benefits__title">
            <h2>
                Нет желания вникать в техооборудование и строительство моечного комплекса?
            </h2>
        </div>
        <div class="additional__benefits__subtitle">
            Воспользуйтесь услугой <span
                    onclick="return location.href = '<?php home_url("template_url"); ?>/mojka-samoobsluzhivaniya-pod-klyuch/'"
                    class="font__blue">автомойка под ключ.</span> Сделаем всё за вас.
        </div>
        <div class="additional__benefits__blocks">
            <div class="additional__benefits__row">
                <div class="additional__benefits__block block__blue">
                    <img src="<?= get_template_directory_uri(); ?>/img/equipment_500/Vector.svg" alt="">
                    <p>Проконсультируем по участкам для МСО</p>
                </div>
                <div class="additional__benefits__block">
                    <img src="<?= get_template_directory_uri(); ?>/img/equipment_500/Vector-1.svg" alt="">
                    <p>Подготовим проектную документацию</p>
                </div>
                <div class="additional__benefits__block">
                    <img src="<?= get_template_directory_uri(); ?>/img/equipment_500/Vector-2.svg" alt="">
                    <p>Построим моечный комплекс</p>
                </div>
                <div class="additional__benefits__block additional__benefits__block__big">
                    <img src="<?= get_template_directory_uri(); ?>/img/equipment_500/Vector-3.svg" alt="">
                    <p>Установим и запустим оборудование мойки самообслуживания</p>
                </div>
                <div class="additional__benefits__block additional__benefits__block__big">
                    <img src="<?= get_template_directory_uri(); ?>/img/equipment_500/Vector-4.svg" alt="">
                    <p>Проведём обучение персонала</p>
                </div>
                <div class="additional__benefits__block additional__benefits__block__big">
                    <img src="<?= get_template_directory_uri(); ?>/img/equipment_500/Vector-5.svg" alt="">
                    <p>Настроим рекламную кампанию автомойки</p>
                </div>
            </div>
        </div>
        <div class="additional__benefits__subtitle">
            <span class="font__blue">Ориентировочный срок реализации проекта – 6 месяцев</span>
        </div>
    </div>
</section>
<section class="ready__business">
    <div class="ready__business__container">
        <h2 class="ready__business__title">
            хотите купить готовый бизнес?
        </h2>
        <div class="ready__business__content">
            <div class="ready__business__block">
                <div class="ready__business__subtitle">
                    Купите <span
                            onclick="return location.href = '<?php home_url("template_url"); ?>/mojka-samoobsluzhivaniya-pod-klyuch/'"
                            class="text_blue">готовую автомойку самообслуживания.</span>
                </div>
                <div class="ready__business__block__row">
                    <div class="ready__business__block__number">1</div>
                    <div class="ready__business__block__li">
                        Продаём мойки самообслуживания только собственного производства
                    </div>
                </div>
                <div class="ready__business__block__row">
                    <div class="ready__business__block__number">2</div>
                    <div class="ready__business__block__li">
                        Гарантия на моечное оборудование
                    </div>
                </div>
                <div class="ready__business__block__row">
                    <div class="ready__business__block__number">3</div>
                    <div class="ready__business__block__li">
                        Сервисное обслуживание и ремонт
                    </div>
                </div>
                <div class="ready__business__block__row">
                    <div class="ready__business__block__number">4</div>
                    <div class="ready__business__block__li">
                        Возможно долевое участие
                    </div>
                </div>
            </div>
            <div class="ready__business__block__image">
                <img class="ready__business__image" src="<?= get_template_directory_uri(); ?>/img/equipment_500/11.jpg"
                     alt="">
            </div>
        </div>

    </div>
</section>
<script src="<?php bloginfo("template_url"); ?>/js/gallery.js"></script>

<?php get_footer(); ?>
