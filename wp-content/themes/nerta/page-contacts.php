<?php
/**
 * Template Name: Contacts
 */

get_header(); ?>
    <div class="inner-header">
        <div class="container">
            <div class="inner-header-company">Nerta Selfwash</div>
            <?php the_title( '<h1 class="h1">', '</h1>' );?>
        </div>
    </div>
    <script data-b24-form="inline/6/gpq4a2" data-skip-moving="true"> (function(w,d,u){ var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/180000|0); var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h); })(window,document,'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_6.js'); </script>

<?php
//the_post();
//the_content();
//?>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript" defer></script>
<script src="<?php echo get_template_directory_uri()?>/js/map.js" defer></script>
<?php get_footer(); ?>