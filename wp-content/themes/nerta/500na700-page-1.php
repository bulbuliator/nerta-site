<?php
/**
 * Template Name: 500na700 page -1
 МОЙКИ САМООБСЛУЖИВАНИЯ ПОД КЛЮЧ
 */
get_header();
?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/page_1.css">
<section class="main" style="background: url(<?php bloginfo("template_url"); ?>/img/page_1/main-bg-min.jpg) no-repeat center center / cover">
	<div class="main__container">

		<div class="breadcrumbs">
			<?php the_breadcrumb() ?>
		</div>

		<h1>мойки самообслуживания под ключ nerta sw</h1>
		<div class="main__text">
			<span>За время работы мы <br> реализовали более <br> 100 проектов, которые <br> стабильно приносят доход <br> своим собственникам.</span>
			<span>Берем на себя полный <br> комплекс работ от помощи <br> в подборе участка земли <br> до обслуживания мойки <br> самообслуживания.</span>
		</div>
		<div class="main__info">
			Работаем с 2000 года <br> по РФ и СНГ
		</div>
	</div>
</section>

<section class="prices start">
	<div class="prices__container">
		<h2>Цены на мойки  самообслуживания под ключ</h2>

		<div class="prices__blocks">

			<div class="prices__block">
				<img class="prices__block__image" alt="автомойка под ключ на 2 поста цена" title="автомойка под ключ на 2 поста цена от производителя Nerta-SW" data-src="<?php bloginfo("template_url"); ?>/img/page_1/prices_1-min.png">
				<div class="prices__block__name">2 поста</div>
				<div class="prices__block__info">
					<div>
						<span>Cтроительство</span>
						<span>от 2 000 000 <b itemprop="priceCurrency" content="RUB" class="rub">₽</b></span>
					</div>
					<div>
						<span>Оборудование</span>
						<span>от 10 000 <b itemprop="priceCurrency" content="EUR" class="rub">€</b></span>
					</div>
				</div>
			</div>

			<div class="prices__block">
				<img class="prices__block__image" alt="автомойка под ключ на 3 поста цена" title="автомойка под ключ на 3 поста цена от производителя Nerta-SW" data-src="<?php bloginfo("template_url"); ?>/img/page_1/prices_2-min.png">
				<div class="prices__block__name">3 поста</div>
				<div class="prices__block__info">
					<div>
						<span>Cтроительство</span>
						<span>от 2 600 000 <b itemprop="priceCurrency" content="RUB" class="rub">₽</b></span>
					</div>
					<div>
						<span>Оборудование</span>
						<span>от 12 700 <b itemprop="priceCurrency" content="EUR" class="rub">€</b></span>
					</div>
				</div>
			</div>

			<div class="prices__block">
				<img class="prices__block__image" alt="автомойка под ключ на 4 поста цена" title="автомойка под ключ на 4 поста цена от производителя Nerta-SW" data-src="<?php bloginfo("template_url"); ?>/img/page_1/prices_3-min.png">
				<div class="prices__block__name">4 поста</div>
				<div class="prices__block__info">
					<div>
						<span>Cтроительство</span>
						<span>от 3 400 000 <b itemprop="priceCurrency" content="RUB" class="rub">₽</b></span>
					</div>
					<div>
						<span>Оборудование</span>
						<span>от 14 600 <b itemprop="priceCurrency" content="EUR" class="rub">€</b></span>
					</div>
				</div>
			</div>

			<div class="prices__block">
				<img class="prices__block__image" alt="автомойка под ключ на 5 поста цена" title="автомойка под ключ на 5 поста цена от производителя Nerta-SW" data-src="<?php bloginfo("template_url"); ?>/img/page_1/prices_1-min.png">
				<div class="prices__block__name">5 постов</div>
				<div class="prices__block__info">
					<div>
						<span>Cтроительство</span>
						<span>от 4 000 000 <b itemprop="priceCurrency" content="RUB" class="rub">₽</b></span>
					</div>
					<div>
						<span>Оборудование</span>
						<span>от 16 900 <b itemprop="priceCurrency" content="EUR" class="rub">€</b></span>
					</div>
				</div>
			</div>

			<div class="prices__block">
				<img class="prices__block__image" alt="стоимость мойки самообслуживания на 6 постов" title="стоимость мойки самообслуживания на 6 постов от производителя Nerta-SW" data-src="<?php bloginfo("template_url"); ?>/img/page_1/prices_2-min.png">
				<div class="prices__block__name">6 постов</div>
				<div class="prices__block__info">
					<div>
						<span>Cтроительство</span>
						<span>от 4 600 000 <b itemprop="priceCurrency" content="RUB" class="rub">₽</b></span>
					</div>
					<div>
						<span>Оборудование</span>
						<span>от 19 000 <b itemprop="priceCurrency" content="EUR" class="rub">€</b></span>
					</div>
				</div>
			</div>

			<div class="prices__block">
				<img class="prices__block__image" alt="стоимость мойки самообслуживания на 8 постов" title="стоимость мойки самообслуживания на 8 постов от производителя Nerta-SW" data-src="<?php bloginfo("template_url"); ?>/img/page_1/prices_3-min.png">
				<div class="prices__block__name">8 постов</div>
				<div class="prices__block__info">
					<div>
						<span>Cтроительство</span>
						<span>от 6 000 000 <b itemprop="priceCurrency" content="RUB" class="rub">₽</b></span>
					</div>
					<div>
						<span>Оборудование</span>
						<span>от 23 200 <b itemprop="priceCurrency" content="EUR" class="rub">€</b></span>
					</div>
				</div>
			</div>

		</div>

		<p class="prices__text">
			Автомойки самообслуживания под ключ - готовые решения. Выбирайте исходя из своего бюджета и характеристик земельного участка. При необходимости разрабатываем проекты с учетом индивидуальных требований.
		</p>
	</div>
</section>

<div>
	<div class="mobile-tab id_mobile-tab" data-id="steps">этапы реализации мойки самообслуживания</div>
	<section class="steps" id="steps">
		<div class="steps__container">
			<h2>Этапы реализации мойки самообслуживания</h2>

			<div class="steps__first">
				<div class="steps__first__info">
					<h3>Поиск участка для автомойки самообслуживания</h3>
					<div>Внимательно оцените перспективность участка перед покупкой или арендой. Требования к участку:</div>
					<span>Вид разрешенного использования: размещение объектов дорожного сервиса, код 4.9.1., с 2019 года - размещение автомобильных моек (4.9.1.3).</span>
					<span>Расположение участка рядом с оживленными  магистралями, офисными и торговыми центрами, в спальных районах и т.д.</span>
					<span>Близость точек подключения к водопроводу, канализации, электроэнергии.</span>
					<p>Не стоит торопиться и делать выбор в пользу первого попавшегося участка даже с очень привлекательной ценой. Помните, что просто перенести мойку на другой объект не получится!</p>
				</div>
				<div class="steps__first__image">
					<img data-src="<?php bloginfo("template_url"); ?>/img/page_1/steps_1-min.png" alt="самомойка под ключ цена" title="самомойка под ключ цена от 8 000 000 руб. Nerta-SW">
					<span>На парковке гипермаркета «ЛЕНТА»</span>
				</div>
			</div>

			<div class="steps__second">
				<h3>Автомойка под ключ – подготовка проекта</h3>
				<p class="steps__second__text">Стоимость и сроки проектирования зависят от того, как планируется оформлять построенный комплекс – как капитальное или как временное сооружение. В первом случае сроки и бюджет значительно выше. Это обусловлено необходимостью разработки большего количества документов и получения разрешений в государственных учреждениях.</p>
				<div class="steps__second__blocks">
					<div class="steps__second__block">
						<img data-src="<?php bloginfo("template_url"); ?>/img/page_1/steps_2_1-min.png" alt="проект мойки самообслуживания" title="проект мойки самообслуживания от компании Nerta-SW">
						<span class="steps__second__block__signature">Проектная документация</span>
						<h4>Строительство капитального сооружения</h4>
						<span class="steps__second__block__list">Разработка обоснования предельного отклонения от допустимых параметров (при необходимости)</span>
						<span class="steps__second__block__list">Разработка архитектурно-градостроительного облика (АГО)</span>
						<span class="steps__second__block__list">Разработка Проектной документации автомойки самообслуживания. Согласно Постановлению РФ №87 от 16.02.2008г.</span>
						<p>Реализация капитального сооружения возможна только при нахождении земельного участка в собственности или в муниципальной аренде.</p>
						<div class="steps__second__block__info">
							<div>
								<span>Цена</span>
								<span>от 500 000 <b itemprop="priceCurrency" content="RUB" class="rub">₽</b></span>
							</div>
							<div>
								<span>Срок</span>
								<span>от 2 месяцев</span>
							</div>
						</div>
					</div>
					<div class="steps__second__block">
						<img data-src="<?php bloginfo("template_url"); ?>/img/page_1/steps_2_2-min.png" alt="проект автомойки самообслуживания" title="проект автомойки самообслуживания от производителя Nerta-SW">
						<span class="steps__second__block__signature">Проектное решение</span>
						<h4>Строительство временного сооружения</h4>
						<p>Как правило, возводят на арендованных площадях. При аренде земельного участка рекомендуем оформлять договор долгосрочной аренды. Это обеспечит стабильность ваших взаимоотношений с собственником. <br><br>Для возведения временного сооружения необходимы следующие документы:</p>
						<span class="steps__second__block__list">Проектное решение временного сооружения АГР (Архитектурное-градостроительное решения)</span>
						<span class="steps__second__block__list">Согласование АГР в администрации населенного пункта;</span>
						<span class="steps__second__block__list">Заключение кадастрового инженера о некапитальности объекта.</span>
						<div class="steps__second__block__info">
							<div>
								<span>Цена</span>
								<span>от 500 000 <b itemprop="priceCurrency" content="RUB" class="rub">₽</b></span>
							</div>
							<div>
								<span>Срок</span>
								<span>от 2 месяцев</span>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="steps__third">
				<h3>Подготовка земельного участка</h3>

				<div class="steps__third__blocks">

					<div class="steps__third__block">
						<div class="steps__third__block__subtitle">3.1</div>
						<div class="steps__third__block__image">
							<img data-src="<?php bloginfo("template_url"); ?>/img/page_1/steps_3_1-min.png" alt="строительство автомоек самообслуживания" title="строительство автомоек самообслуживания под ключ Nerta-SW">
							<span>Монтаж железобетонных колец</span>
						</div>
						<div class="steps__third__block__name">Подготовка земельного участка</div>
						<span class="steps__third__block__list">Выравнивание участка;</span>
						<span class="steps__third__block__list">Подвод и прокладка необходимых инженерных коммуникаций;</span>
						<span class="steps__third__block__list">Установка ЖБИ колец.</span>
						<div class="steps__third__block__info">
							<div>
								<span>Цена</span>
								<span>от 100 000 <b itemprop="priceCurrency" content="RUB" class="rub">₽</b></span>
							</div>
							<div>
								<span>Срок</span>
								<span>от 2 месяцев</span>
							</div>
						</div>
					</div>

					<div class="steps__third__block">
						<div class="steps__third__block__subtitle">3.2</div>
						<div class="steps__third__block__image">
							<img data-src="<?php bloginfo("template_url"); ?>/img/page_1/steps_3_2-min.png" alt="мойки самообслуживания строительство" title="мойки самообслуживания строительствопод ключ Nerta-SW">
							<span>Пользуемся бетоном марки не ниже М350</span>
						</div>
						<div class="steps__third__block__name">Заливка монолитной бетонной плиты или монтаж сборно-разборного фундамента</div>
						<span class="steps__third__block__list">Заливка подбетонки;</span>
						<span class="steps__third__block__list">Укладка теплоизоляции, теплого пола, арматуры;</span>
						<span class="steps__third__block__list">Заливка основного бетонного фундамента;</span>
						<span class="steps__third__block__list">Шлифовка готового бетона.</span>
						<div class="steps__third__block__info">
							<div>
								<span>Цена</span>
								<span>от 250 000 <b itemprop="priceCurrency" content="RUB" class="rub">₽</b></span>
							</div>
							<div>
								<span>Срок</span>
								<span>от 3 недель</span>
							</div>
						</div>
					</div>

					<div class="steps__third__block">
						<div class="steps__third__block__subtitle">3.2</div>
						<div class="steps__third__block__image">
							<img data-src="<?php bloginfo("template_url"); ?>/img/page_1/steps_3_3-min.png" alt="мойки самообслуживания установка" title="мойки самообслуживания установка Nerta-SW">
							<span>Монтаж каркаса</span>
						</div>
						<div class="steps__third__block__name">Установка навеса, технического помещения мойки самообслуживания.</div>
						<span class="steps__third__block__list">Установка каркаса автомойки и технического помещения;</span>
						<span class="steps__third__block__list">Прокладка кабелей освещения;</span>
						<span class="steps__third__block__list">Монтаж ламп, сборка электрических щитов;</span>
						<span class="steps__third__block__list">Крепление рекламного фриза.</span>
						<div class="steps__third__block__info">
							<div>
								<span>Цена</span>
								<span>от 250 000 <b itemprop="priceCurrency" content="RUB" class="rub">₽</b></span>
							</div>
							<div>
								<span>Срок</span>
								<span>от 2 месяцев</span>
							</div>
						</div>
					</div>

				</div>
			</div>

			<div class="steps__wrapper">
				<div class="steps__four">
					<h3>Монтаж моечного <br>оборудования</h3>
					<img class="steps__four__image" data-src="<?php bloginfo("template_url"); ?>/img/page_1/steps_4_1-min.png" alt="стоимость установки оборудования мойка самообслуживания" title="стоимость установки оборудования для мойки самообслуживания Nerta-SW">
					<div class="steps__four__subtitle">Монтаж оборудования</div>
					<span class="steps__four__list">Установка оборудования;</span>
					<span class="steps__four__list">Тестирование и наладка;</span>
					<span class="steps__four__list">Ввод в эксплуатацию автомойки самообслуживания.</span>
					<div class="steps__third__block__info">
						<div>
							<span>Цена</span>
							<span>от 170 000 <b itemprop="priceCurrency" content="RUB" class="rub">₽</b></span>
						</div>
						<div>
							<span>Срок</span>
							<span>от 3 дней</span>
						</div>
					</div>
				</div>
				<div class="steps__four five">
					<h3>Благоустройство прилегающей территории</h3>
					<img class="steps__four__image" data-src="<?php bloginfo("template_url"); ?>/img/page_1/steps_5_1-min.png" alt="купить автомойку самообслуживания под ключ цена" title="купить автомойку самообслуживания под ключ от производителя Nerta-SW">
					<div class="steps__four__subtitle">МСО с благоустроенной территорией</div>
					<span class="steps__four__list">Выполняется асфальтирование площадки;</span>
					<span class="steps__four__list">Нанесение разметки и укладка газона;</span>
					<span class="steps__four__list">Производятся другие работы, направленные на придание мойке привлекательности</span>
					<div class="steps__third__block__info">
						<div>
							<span>Цена</span>
							<span>от 1 200 <b itemprop="priceCurrency" content="RUB" class="rub">₽</b>/м2</span>
						</div>
						<div>
							<span>Срок</span>
							<span>от 3 дней</span>
						</div>
					</div>
				</div>
			</div>

			<div class="steps__six">

				<div class="steps__six__image">
					<img data-src="<?php bloginfo("template_url"); ?>/img/page_1/steps_6-min.png" alt="автомойка пена от производителя Nerta-SW" title="Собственное производство моющей химии NERTA-SW">
					<span>Моющая химия собственного производства</span>
				</div>
				<div class="steps__six__content">
					<div class="steps__six__content__bg"></div>
					<h3>Обучение и помощь</h3>
					<p class="steps__six__content__text">Обеспечиваем комплексную поддержку клиентов, осуществляя:</p>
					<span class="steps__six__content__list">Обучение персонала правилам работы,<br> обслуживания, поддержания стабильного <br> функционирования мойки;</span>
					<span class="steps__six__content__list">Поставка моющей химии собственного <br> производства;</span>
					<span class="steps__six__content__list">Помощь в раскрутке, организация <br> рекламных кампаний.</span>
				</div>
			</div>
		</div>
	</section>
</div>

<div>
	<div class="mobile-tab id_mobile-tab" data-id="costs">сколько стоит мойка самообслужвания под ключ?</div>
	<section class="costs" id="costs">
		<div class="costs__container">

			<div class="costs__block">
				<h2>Сколько стоит мойка самообслуживания под ключ?</h2>
				<div class="costs__block__wrapper">
					<div class="costs__block__content">
						<p class="costs__block__text">На итоговую стоимость влияет комплекс факторов, часть из которых уже была рассмотрена выше. Но всегда есть выбор - экономить на каждой статье расходов, покупать все самое дорогое или выбирать сбалансированный вариант. NERTA-SW – это, как раз, баланс между ценой, надёжностью и удобством эксплуатации. <br><br>NERTA-SW – разрабатываем проекты, сочетающие:</p>
						<span class="costs__block__list">адекватную стоимость</span>
						<span class="costs__block__list">расширенную функциональность</span>
						<span class="costs__block__list">достойное качество</span>
						<span class="costs__block__list">эффективность работы</span>
						<p class="costs__block__text">В среднем строительство МСО от разработки проекта до запуска мойки занимает в районе 3-4 месяцев. Затянуть проект может согласование разрешительной документации, необходимость серьезной подготовки участка, масштабное благоустройство и т. д. <br><br>В среднем цена строительства бюджетной мойки самообслуживания на 4 поста составляет около 8 миллионов рублей. Эта цифра ориентировочная, но близкая к реальности. Кроме уже рассмотренных факторов бюджет в рублях может меняться под воздействием колебаний курса евро и доллара. Услуги поставщиков, стоимость оборудования чаще всего определяется в иностранной валюте.</p>
					</div>
					<div class="costs__block__image">
						<img data-src="<?php bloginfo("template_url"); ?>/img/page_1/cost_1-min.png" alt="бесконтактные автомойки" title="бесконтактные автомойки от производителя Nerta-SW">
						<span class="costs__block__image__subtitle">Четырёх постовая МСО</span>
						<div class="costs__block__image__info">
							<h3>Автомойка самообслуживания - цена под ключ (4 поста)</h3>
							<div>
								<span>Цена</span>
								<span>от 4 500 000 <b itemprop="priceCurrency" content="RUB" class="rub">₽</b></span>
							</div>
							<div>
								<span>Срок</span>
								<span>от 3 месяцев</span>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="costs__block">
				<h2>Мойка самообслуживания под ключ - цена в Москве</h2>
				<div class="costs__block__wrapper">
					<div class="costs__block__content">
						<p class="costs__block__text">Стоимость строительства МСО в Москве выше по сравнению с регионами. Это обусловлено рядом причин:</p>
						<span class="costs__block__list"><p>Цена участка.</p> Земля в Москве дорогая. Нужно закладывать в бюджет более высокие расходы</span>
						<span class="costs__block__list"><p>Стоимость услуг строителей и материалов.</p> В Москве по сравнению с регионами завышенные цены на работу мастеров и поставки стройматериалов.</span>
						<span class="costs__block__list"><p>Требовательность клиентов.</p> Столичный автолюбитель привык к сервису. Поэтому придется вложить средства в благоустройство территории.</span>
						<div class="costs__block__content__info">
							<h3>Автомойка самообслуживания - цена под ключ (4 поста)</h3>
							<div>
								<span>Цена</span>
								<span>~ 12 000 000 <b itemprop="priceCurrency" content="RUB" class="rub">₽</b></span>
							</div>
							<div>
								<span>Срок</span>
								<span>3-4 месяца</span>
							</div>
						</div>
					</div>
					<div class="costs__block__image">
						<img data-src="<?php bloginfo("template_url"); ?>/img/page_1/cost_2-min.png" alt="мойка самообслуживания под ключ цена москва" title="мойка самообслуживания под ключ цена москва от производителя Nerta-SW">
						<span class="costs__block__image__subtitle">МСО закрытого типа</span>
					</div>
				</div>
			</div>

		</div>
	</section>
</div>

<section class="info">
	<div class="info__container">
		<div class="info__text">
			Чтобы уточнить сколько будет стоить проект, задать вопросы, получить консультации, свяжитесь с менеджерами компании «NERTA-SW». Специалисты соберут данные, проведут расчеты, подготовят коммерческое предложение.
		</div>
		<div class="info__content">
			<div>
				<span>Почта</span>
				<a href="mailto:info@nerta-sw.ru">info@nerta-sw.ru</a>
			</div>
			<div>
				<span>Телефон</span>
				<a href="tel:+74956777000">+7 (495) 6-777-000</a>
			</div>
			<button class="js-callback-popup-show">Заказать обратный звонок</button>
		</div>
	</div>
</section>
<div>
	<div class="mobile-tab id_mobile-tab" data-id="why">почему выгоднее купить мойки самообслуживания под ключ, чем строить самостоятельно?</div>
	<section class="why" id="why">
		<div class="why__container	">
			<div class="why__info">
				<h2>Почему выгоднее купить мойки  самообслуживания под ключ, чем строить  самостоятельно?</h2>
				<p class="why__text">Поиск земельного участка и подрядчиков для выполнения строительных работ, разработка и согласование проектной документации. С этими трудностями сталкиваются предприниматели, желающие самостоятельно открыть мойку самообслуживания. Отсутствие необходимого опыта, достаточных знаний в рассматриваемой сфере часто приводит к нерациональному использованию имеющихся средств.</p>
			</div>
			<div class="why__blocks">
				<div class="why__block">
					<h3>Экономия времени и средств</h3>
					<span class="why__block__text">Клиенту не придется заниматься общением <br> с подрядчиками, выбором оборудования. Сотрудники компании NERTA-SW помогают оценить перспективность участка. Это поможет избежать неоправданных расходов.</span>
				</div>
				<div class="why__block">
					<h3>Комфорт и уверенность</h3>
					<span class="why__block__text">Компания NERTA-SW ввела в эксплуатацию более 100 моек, работает с 2000 года. Мы гарантированно построим автомойку, которая будет востребована у клиентов.</span>
				</div>
				<div class="why__block">
					<h3>Помощь  и поддержка</h3>
					<span class="why__block__text">Консультируем по продвижению бизнеса. Помогаем организовать эффективную рекламную кампанию, которая обеспечит стабильный поток клиентов на МСО.</span>
				</div>
				<div class="why__block">
					<h3>Гарантия и ответственность</h3>
					<span class="why__block__text">Доверять выполнение всего комплекса работ одной компании удобно и выгодно. NERTA-SW несёт полную ответственность за выполненные работы. Вам не нужно разбираться кто из подрядчиков виноват в проблеме в случае её возникновения. На выполненные работы, а также установленное оборудование дается гарантия 1 год.</span>
				</div>
				<div class="why__block">
					<h3>Эксплуатационное обслуживание</h3>
					<span class="why__block__text">Благодаря системе удалённого мониторинга оборудования, вы всегда в курсе, когда нужно произвести регламентные работы.</span>
				</div>
			</div>
		</div>
	</section>
</div>
<div>
	<div class="mobile-tab id_mobile-tab" data-id="recommendation">рекомендации, которые помогут сделать будущий бизнес еще более успешным!</div>
	<section class="recommendation" id="recommendation">
		<div class="recommendation__container">
			<h2>Рекомендации, которые помогут сделать будущий бизнес еще более успешным!</h2>
			<div class="recommendation__wrapper">
				<div class="recommendation__block">
					<div class="recommendation__block__content">
						<p class="recommendation__block__text">В регионах, где на протяжении трёх и более месяцев стабильно отрицательная температура (от -10°С и ниже), стоит сразу строить закрытую автомойку самообслуживания. <br><br>Вы получаете неоспоримое преимущество перед конкурентами с открытыми мойками самообслуживания. Достоинства закрытых МСО:</p>
						<span class="recommendation__block__list">Значительно увеличивает поток клиентовпри отрицательной температуре;</span>
						<span class="recommendation__block__list">Защищает клиентов от ветра и косого дождя;</span>
						<span class="recommendation__block__list">Может обустраиваться в уже возведенном здании.</span>
						<p class="recommendation__block__text">Но стоимость строительства закрытых моек самообслуживания выше. Поэтому наиболее рентабельным использование такого решения становится в северных регионах страны.</p>
					</div>
					<div class="recommendation__block__image">
						<img data-src="<?php bloginfo("template_url"); ?>/img/page_1/recom_2-min.png" alt="бесконтактная мойка сколько стоит" title="бесконтактная мойка сколько стоит от производителя Nerta-SW ">
						<span>Мойка закрытого типа</span>
					</div>
				</div>
				<div class="recommendation__block">
					<div class="recommendation__block__image">
						<img data-src="<?php bloginfo("template_url"); ?>/img/page_1/recom_1-min.png" alt="москва строительство автомойки под ключ" title="москва строительство автомойки под ключ от производителя Nerta-SW">
						<span>Используем нержавеющую сталь Aisi 304</span>
					</div>
					<div class="recommendation__block__content">
						<p class="recommendation__block__text">Не экономьте на оборудовании. От качества, надежности, удобства работы технического оснащения зависит впечатления клиентов МСО. <br><br>Дешевое оборудование часто выходит из строя, что приводит к простоям, непредвиденным затратам, клиенты автомойки самообслуживания оставляют отрицательные отзывы. Все эти факторы значительно уменьшают посещаемость МСО.<br><br>Не пренебрегайте внешним видом. Важно использовать долговечные стройматериалы при строительстве и отделке мойки. Такая МСО будет сохранять привлекательность на протяжении 5 и более лет, обеспечивая уверенность в качестве, надежности бизнеса.<br><br>В случае ограниченности бюджета можно арендовать старую автомойку либо площадку на паркинге. Плюсы такого решения: </p>
						<span class="recommendation__block__list">Наличие подведенных инженерных коммуникаций;</span>
						<span class="recommendation__block__list">Готовые транспортные узлы;</span>
						<span class="recommendation__block__list">Наличие «разогретой клиентской базы, которая уже готова пользоваться услугами вашей МСО;</span>
						<span class="recommendation__block__list">Уменьшение затрат на покупку участка.</span>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<section class="reasons" id="reasons">
	<div class="reasons__container">
		<div class="reasons__info">
			<h2>5 причин доверить строительство мойки самообслуживания под ключ компании <span>nerta-sw</span></h2>
			<p class="reasons__text">Осуществляем деятельность на территории РФ с 2000 года. За это время было успешно реализовано более 100 проектов. Установлено более 500 постов по всей территории Российской Федерации. Накопили опыт, который позволяет оптимизировать реализацию каждого проекта. </p>
		</div>
		<div class="reasons__blocks">
			<div class="reasons__block">
				<img data-src="<?php bloginfo("template_url"); ?>/img/page_1/reasons_1-min.png" alt="строительство автомойки самообслуживания" title="строительство автомойки самообслуживания от производителя Nerta-SW">
				<span class="reasons__block__subtitle">Отработанные процессы строительства</span>
			</div>
			<div class="reasons__block">
				<span class="reasons__block__number">01</span>
				<h3>Собственное производство моечного оборудования</h3>
				<p class="reasons__block__text">Высокотехнологичные заводские линии – залог изготовления моечного оборудования в оговоренные сроки.</p>
			</div>
			<div class="reasons__block">
				<span class="reasons__block__number">02</span>
				<h3>Комплексный подход и сервисное обслуживание</h3>
				<p class="reasons__block__text">У компаний имеется система дистанционного мониторинга и управления оборудованием МСО для контроля стабильности работы МСО, минимизирования простоев, вызванных техническими неисправностями.</p>
			</div>
			<div class="reasons__block">
				<span class="reasons__block__number">03</span>
				<h3>Высокая скорость строительства</h3>
				<p class="reasons__block__text">Строим быстро даже масштабные объекты «с нуля». Оптимизируем транспортные издержи, используем современные технологии.</p>
			</div>
		</div>
		<div class="reasons__blocks">
			<div class="reasons__block">
				<span class="reasons__block__number">04</span>
				<h3>Сбалансированные цены</h3>
				<p class="reasons__block__text">Рационально подходим к использованию предоставленного бюджета. Экономим там, где можно, инвестируем туда, куда нужно. Благодаря налаженным партнерским отношениям с подрядчиками гарантируем доступные цены на проектировочные, строительные, монтажные работы.</p>
			</div>
			<div class="reasons__block">
				<span class="reasons__block__number">05</span>
				<h3>Безупречная репутация</h3>
				<p class="reasons__block__text">Располагаем собственной сетью моек самообслуживания, состоящей из 18 автомоек по всей России. Благодаря этому досконально знаем все «подводные камни», нюансы бизнеса. Делимся опытом с партнерами, помогаем развить бизнес, привлечь клиентуру, решать возникающие задачи.</p>
			</div>
			<div class="reasons__block">
				<img data-src="<?php bloginfo("template_url"); ?>/img/page_1/reasons_2-min.png" alt="сотрудники производства моек самообслуживания" title="сотрудники производства моек самообслуживания от Nerta-SW">
				<span class="reasons__block__subtitle">Производство NERTA-SW</span>
			</div>
		</div>
	</div>
</section>

<div>
	<div class="mobile-tab id_mobile-tab" data-id="how">Как купить мойки самообслуживания  <br> под ключ?</div>
	<section class="how" id="how">
		<div class="how__container">
			<h2>Как купить мойки самообслуживания под ключ?</h2>
			<div class="how__content">
				<p class="how__content__text">Первый шаг к созданию успешного и прибыльного бизнеса - заполните форму заявки на сайте либо созвонитесь с менеджерами компании по номеру <a href="tel:+84956777000">8 (495) 6-777-000.</a> <br><br>Консультанты компании свяжутся с вами. Подготовим детализированный бизнес-план, поможем спрогнозировать прибыль, рассчитать затраты. Обеспечиваем комфортные условия сотрудничества, отсутствие «сюрпризов» (увеличение цены, затягивание сроков, т.д.) при реализации проекта.<br><br>Открыты к диалогу и выполнению масштабных задач. Работаем открыто и прозрачно. Обращаем внимание, что все расчеты, консультации предоставляются бесплатно. Поэтому оставьте свои контактные данные. Мы же сделаем всё возможное, чтобы подготовить для вас успешный бизнес с серьезными перспективами, гарантированной прибылью.</p>
				<div class="how__content__image">
					<img data-src="<?php bloginfo("template_url"); ?>/img/page_1/how-min.png" alt="сотрудник компании автомоек самообслуживания" title="сотрудник компании автомоек самообслуживания от производителя Nerta-SW">
					<span>NERTA-SW – работаем с 2000 года</span>
				</div>
			</div>
			<h2>
				автомобильные мойки самообслуживания  —  <span>максимальный доход с минимальными вложениями!</span>
			</h2>
		</div>
	</section>
</div>

<div class="progress-bar" id="progress-bar"></div>

<script src="https://cdn.jsdelivr.net/npm/lazyload@2.0.0-rc.2/lazyload.js"></script>
<script src="<?php bloginfo("template_url"); ?>/js/page_1.js"></script>

<?php get_footer(); ?>
