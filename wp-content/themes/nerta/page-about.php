<?php
/**
 * Template Name: About
 */

get_header(); ?>
<div class="inner-header">
    <div class="container">
        <div class="inner-header-company">Nerta Selfwash</div>
        <?php the_title( '<h1 class="h1">', '</h1>' );?>
    </div>
</div>

<div class="about">
    <div class="container">
        <div class="about-text">
            Компания Nerta создана в&nbsp;2001&nbsp;году.<br>
            Изготавливаем, продаём оборудование и&nbsp;химию для промышленного и&nbsp;автомоечного сектора. Четырнадцатилетний опыт позволяет успешно запускать и&nbsp;поддерживать бизнес наших клиентов, включая гарантийное и&nbsp;постгарантийное обслуживание.
            <br>
            <br>
            В&nbsp;2006 году мы&nbsp;построили первую автомойку самообслуживания на&nbsp;территории&nbsp;РФ и&nbsp;предложили рынку один из&nbsp;самых инновационных продуктов&nbsp;&mdash; активную пену Carnet Jumbo.
        </div>
    </div>
</div>
<div class="advantages">
    <div class="container">
        <div class="advantages-title">Преимущества работы с&nbsp;нами</div>
    </div>
    <div class="advantages-list">
        <div class="advantages-item">
            <div class="advantages-content">
                <svg class="advantages-img"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#adv-market"></use></svg>
                <div class="title">Маркетинг и&nbsp;реклама</div>
                <p>обходимую информацию для организации рекламной компании и&nbsp;продвижения услуг на&nbsp;рынке.</p>
            </div>
        </div>
        <div class="advantages-item">
            <div class="advantages-content">
                <svg class="advantages-img"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#adv-gear"></use></svg>
                <div class="title">Комплектующие</div>
                <p>Мы&nbsp;храним комплектующие
                    на&nbsp;складе, что позволяет быстро поставлять клиентам необходимое,
                    без ожидания поставщиков.</p>
            </div>
        </div>
        <div class="advantages-item">
            <div class="advantages-content">
                <svg class="advantages-img"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#adv-letter"></use></svg>
                <div class="title">Гарантия и&nbsp;постгарантия</div>
                <p>В&nbsp;случае необходимости
                    или нестандартной ситуации
                    оперативно отправляем специалиста.</p>
            </div>
        </div>
        <div class="advantages-item">
            <div class="advantages-content">
                <svg class="advantages-img"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#adv-study"></use></svg>
                <div class="title">Обучаем персонал</div>
                <p>Сотрудники смогут быстро приступить к&nbsp;работе, обладая необходимыми навыками.</p>
            </div>
        </div>
    </div>
</div>

<div class="feedback">
    <div class="container">
        <div class="feedback-content">
            <?php echo do_shortcode( '[contact-form-7 id="12" title="Callback"]' ); ?>
        </div>
    </div>
</div>
<div class="projects">
    <div class="container">
        <div class="title">Реализованные проекты и благодарности</div>
    </div>
    <div class="container">
        <div class="projects-map" id="map"></div>
    </div>
    <div class="container">
        <div class="countries">
            <ul class="countries-list">
                <li><strong>Россия</strong></li>
                <li>Березовский — 2 объекта</li>
                <li>Владивосток</li>
                <li>Евпатория</li>
                <li>Екатеринбург — 3 объекта</li>
                <li>Каменск-Уральский</li>
                <li>Казань — 5 объекта</li>
                <li>Калуга — 5 объектов</li>
                <li>Козельск — 2 объекта</li>
                <li>Котовск</li>
                <li>Кирсанов</li>
                <li>Кстово</li>
                <li>Липецк</li>
                <li>Набережные Челны — 3 объекта</li>
                <li>Нальчик</li>
                <li>Нижний Новгород — 2 объекта</li>
                <li>Нижняя Тура</li>
                <li>Новошахтинск</li>
                <li>Первоуральск</li>
                <li>Печоры</li>
                <li>Пермь</li>
                <li>Псков — 7 объекта</li>
                <li>Рассказово — 2 объекта</li>
                <li>Рязань — 2 объекта</li>
                <li>Саров — 2 объекта</li>
                <li>Сосенский</li>
                <li>Тверь</li>
                <li>Тутаев</li>
                <li>Ульяновск — 3 объекта</li>
                <li>Уфа</li>
                <li>Учалы</li>
                <li>Челябинск</li>
                <li>Хабаровск — 3 объекта</li>
                <li>Шлиссельбург</li>
                <li>Ясногорск</li>
            </ul>
            <ul class="countries-list">
                <li><strong>Москва и Московская область</strong></li>
                <li>Белая Дача</li>
                <li>Балашиха — 2 объекта</li>
                <li>Волоколамск — 2 объекта</li>
                <li>Дмитров</li>
				<li>Домодедовская</li>
				<li>Дзержинский</li>
                <li>Елино</li>
                <li>Еганово</li>
                <li>Ермолино</li>
                <li>Звенигород</li>
                <li>Истра</li>
                <li>Калининец</li>
				<li>Кашира</li>				
				<li>Коломна</li>				
				<li>Красная</li>
				<li>Кудиново</li>				
				<li>Пахра</li>
                <li>Лобня</li>
                <li>Лосино-Петровский</li>
                <li>Люберцы — 2 объекта</li>
                <li>Мамыри</li>
                <li>Молжаниновский р-н</li>
                <li>Молоково</li>
                <li>Московский</li>
                <li>Нахабино — 2 объекта</li>
                <li>Новые дома</li>
                <li>Новоселки</li>
				<li>Ногинск</li>
                <li>Подольск</li>
				<li>Подрезково</li>
				<li>Реутов</li>
                <li>Строгино — 2 объекта</li>
				<li>Серебряные пруды</li>
				<li>Ташкентская улица</li>
				<li>Тушино</li>
                <li>Химки —  2 объекта</li>
                <li>Чехов —  2 объекта</li>
                <li>Шолохово</li>
                <li>Щелково</li>
                <li>Электросталь — 3 объекта</li>
                <li>Электроугли</li>
            </ul>
            <ul class="countries-list">
                <li><strong>Казахстан</strong></li>
                <li>Актау</li>
            </ul>
        </div>
    </div>
    <?php
    the_post();
    the_content();
    ?>
</div>
<script src="<?php bloginfo("template_url"); ?>/js/lib/d3/d3.min.js"></script>
<script src="<?php bloginfo("template_url"); ?>/js/lib/topojson/topojson.min.js"></script>
<script src="<?php bloginfo("template_url"); ?>/js/tooltip.js"></script>
<script src="<?php bloginfo("template_url"); ?>/js/ipmap.js"></script>
<script>
    (function (){
        var map = new IPMap({
            selector: 'map',
            topojsonPath: '<?php bloginfo("template_url"); ?>/data/russia.js',
        });

        map.drawMap();
    })();
</script>
<?php get_footer(); ?>
