<?php
/**
 * Template Name: Text
 */

get_header(); ?>
    <div class="inner-header">
        <div class="container">
            <div class="inner-header-company">Nerta Selfwash</div>
            <?php the_title( '<h1 class="h1">', '</h1>' );?>
        </div>
    </div>
    <div class="services">
        <div class="container">
            <div class="services-feedback"><div class="feedback-content"> <?php echo do_shortcode( '[contact-form-7 id="12" title="Callback"]' ); ?></div></div>
            <div class="services-text">
                <?php
                the_post();
                the_content();
                ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>