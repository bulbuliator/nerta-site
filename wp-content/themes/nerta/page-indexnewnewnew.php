<?php
/**
 * Template Name: Main Page
 */
get_header(); ?>

<div id="nerta-main-page" class="nerta-second">
<div class="main-top main-top-new">
    <div class="container">
 <h1><span id="h-inner-1">Мойки самообслуживания <br>под ключ</span>
    <span id="h-inner-2" >Nerta SW</span></h1>
    <ul id="main-list">

        <li><span>За время работы мы реализовали более 100 проектов, которые стабильно приносят доход своим собственникам. </span></li>
        <li><span>Берем на себя полный комплекс работ от помощи в подборе участка земли до обслуживания мойки самообслуживания.</span></li>
        <br>

    </ul>




        <div id="main-img"></div>
                <div id="main-theme"></div>
    </div>
    <span id="working_from">Работаем с 2000<br>
Работаем по РФ и СНГ</span>
</div>
<div class="cost gray">
    <div class="container" style="padding-bottom: 50px;">
        <div class="title">
          <h2>Цены на мойки самообслуживания под ключ</h2>
        </div>

<div class="panels-3" style="margin-bottom: 69px;clear:both;">
    <div class="first-column">
 <img style='' alt="автомойка под ключ на 2 поста цена" title="автомойка под ключ на 2 поста цена от производителя Nerta-SW" src="<?php bloginfo("template_url"); ?>/img/options/2.png" alt="Система
обратного осмоса">
        <div class="column-inner">
            <table style="width:100%" class="second-page-table white-table">
              <tr >

                <th colspan="2"><div class="table-panel"></div><span class="in-table-th">2 поста</span></th>
              </tr>
              <tr>
                <td>Строительство</td>
                <td>от 2 000 000 ₽</td>
              </tr>
              <tr>
                <td>Оборудование</td>
                <td>от 10 000 €</td>
              </tr>
            </table>
        </div>
    </div>
    <div class="second-column">
 <img style='' alt="автомойка под ключ на 3 поста цена" title="автомойка под ключ на 3 поста цена от производителя Nerta-SW" src="<?php bloginfo("template_url"); ?>/img/options/3.png" alt="Система
обратного осмоса">
        <div class="column-inner">
                        <table style="width:100%" class="second-page-table white-table">
              <tr >
                <th colspan="2"><div class="table-panel"></div><span class="in-table-th">3 поста</span></th>
              </tr>
              <tr>
                <td>Строительство</td>
                <td>от 2 600 000 ₽</td>
              </tr>
              <tr>
                <td>Оборудование</td>
                <td>от 12 700 €</td>
              </tr>
            </table>
        </div>
    </div>
    <div class="third-column">
 <img style='' alt="автомойка под ключ на 4 поста цена" title="автомойка под ключ на 4 поста цена от производителя Nerta-SW" src="<?php bloginfo("template_url"); ?>/img/options/444.png" alt="Система
обратного осмоса">
        <div class="column-inner">
            <table style="width:100%" class="second-page-table white-table">
              <tr >
                <th colspan="2"><div class="table-panel"></div><span class="in-table-th">4 постов</span></th>
              </tr>
              <tr>
                <td>Строительство</td>
                <td>от 3 400 000 ₽</td>
              </tr>
              <tr>
                <td>Оборудование</td>
                <td>от 14 600 €</td>
              </tr>
            </table>
        </div>
    </div>

<div style="clear:both;"></div>
</div>


    </div>
</div>

<div class="cost white">
    <div class="container" style="padding-bottom: 50px;">


<div class="panels-3" style="margin-bottom: 69px;clear:both; padding-top:50px;">
    <div class="first-column">
 <img style='' alt="автомойка под ключ на 5 поста цена" title="автомойка под ключ на 5 поста цена от производителя Nerta-SW" src="<?php bloginfo("template_url"); ?>/img/options/2.png" alt="Система
обратного осмоса">
        <div class="column-inner">
            <table style="width:100%" class="second-page-table gray-table">
              <tr >
                <th colspan="2"><div class="table-panel"></div><span class="in-table-th">5 постов</span></th>
              </tr>
              <tr>
                <td>Строительство</td>
                <td>от 4 000 000 ₽</td>
              </tr>
              <tr>
                <td>Оборудование</td>
                <td>от 16 900 €</td>
              </tr>
            </table>
        </div>
    </div>
    <div class="second-column">
 <img style='' alt="стоимость мойки самообслуживания на 6 постов " title="стоимость мойки самообслуживания на 6 постов от производителя Nerta-SW" src="<?php bloginfo("template_url"); ?>/img/options/3.png" alt="Система
обратного осмоса">
        <div class="column-inner">
                        <table style="width:100%" class="second-page-table gray-table">
              <tr >
                <th colspan="2"><div class="table-panel"></div><span class="in-table-th">6 постов</span></th>
              </tr>
              <tr>
                <td>Строительство</td>
                <td>от 4 600 000 ₽</td>
              </tr>
              <tr>
                <td>Оборудование</td>
                <td>от 19 000 €</td>
              </tr>
            </table>
        </div>
    </div>
    <div class="third-column">
 <img style='' alt="стоимость мойки самообслуживания на 8 постов " title="стоимость мойки самообслуживания на 8 постов от производителя Nerta-SW" src="<?php bloginfo("template_url"); ?>/img/options/444.png">
        <div class="column-inner">
            <table style="width:100%" class="second-page-table gray-table">
              <tr >
                <th colspan="2"><div class="table-panel"></div><span class="in-table-th">8 поста</span></th>
              </tr>
              <tr>
                <td>Строительство</td>
                <td>от 6 000 000 ₽</td>
              </tr>
              <tr>
                <td>Оборудование</td>
                <td>от 23 200 €</td>
              </tr>
            </table>
        </div>
    </div>

<div style="clear:both;"></div>
</div>


    </div>
</div>
<div class="container">
    <div  class="main-attention-cont">Автомойки самообслуживания под ключ - готовые решения. Выбирайте исходя из своего бюджета и характеристик земельного участка. При необходимости разрабатываем проекты с учетом индивидуальных требований.</div>
</div>
    <div class="title" style="padding-top: 50px;"><h2>Этапы реализации мойки самообслуживания под ключ</h2></div>
<div class="sub-header-line-left"></div>
<div class="container">

    <div class="column-sub-header">1. Поиск участка для автомойки самообслуживания</div>
        <div class="options-text left" id="text-1" >

            <div>
                <p style="padding:15px 0px; padding-top:27px;">Внимательно оцените перспективность участка перед покупкой или арендой. Требования к участку:</p>


                <ul class="list">
                    <li>Вид разрешенного использования: размещение объектов дорожного сервиса, код 4.9.1., с 2019 года - размещение автомобильных моек (4.9.1.3).</li>
                    <li>Расположение участка рядом с оживленными магистралями, офисными и торговыми центрами, в спальных районах, т.д.</li>
                    <li>Близость точек подключения к водопроводу, канализации, электроэнергии.</li>

                </ul>

                <p>
                </p>
            </div>
<div class="column-container">
    <div class="column-row">
        <img class="column-icon" src="https://nerta-sw.ru/wp-content/uploads/2020/06/007-map-1.png">
        <span class="column-text">
            Назначение земли
        </span>
    </div>
    <div class="column-row">
        <img class="column-icon" src="https://nerta-sw.ru/wp-content/uploads/2020/06/008-recycle.png">
        <span class="column-text">
            Электричество
        </span>
    </div>
        <div class="column-row">
        <img class="column-icon" src="https://nerta-sw.ru/wp-content/uploads/2020/06/2-layers-2.png">
        <span class="column-text">
            Вода
        </span>
    </div>
</div>
        </div>
        <div id="img-5"  class="right-pic" style="margin-top:15px;">
            <img alt="самомойка под ключ цена" title="самомойка под ключ цена от 8 000 000 руб. Nerta-SW" src='https://nerta-sw.ru/wp-content/uploads/2020/06/untitled-2.jpg'>
            <i class="under-image-i under-image-i-right" > на парковке гипермаркета «ЛЕНТА»</i>
        </div>

        <br>
        <br>

</div>





<div class="container">
    <div  class="main-attention-cont" style="margin-top:50px;">Не стоит торопиться и делать выбор в пользу первого попавшегося участка даже с очень привлекательной ценой.
Помните, что просто перенести мойку на другой объект не получится!</div>
</div>


<div class="sub-header-line-left"></div>

<div class="container">
    <div class="column-sub-header">2. Автомойка под ключ – подготовка проекта </div>
    <p style="padding:15px 0px;">
        Стоимость и сроки проектирования зависят от того, как планируется оформлять построенный комплекс – как капитальное или как временное сооружение.
В первом случае сроки и бюджет значительно выше. Это обусловлено необходимостью разработки большего количества документов и получения разрешений в государственных учреждениях.

    </p></br>
            <div id="img-5" style="">
            <img alt="проект мойки самообслуживания" title="проект мойки самообслуживания от компании Nerta-SW" src='https://nerta-sw.ru/wp-content/uploads/2020/06/-d0-bf-d1-80-d0-be-d0-b5-d0-ba-d1-82_-d0-bc-d0-be-d0-b9-d0-ba-d0-b8_-d1-81-d0-b0-d0-bc-d0-be-d0-be-d0-b1-d1-81-d0-bb-d1-83-d0-b6-d0-b8-d0-b2-d0-b0-d0-bd-d0-b8-d1-8f_nerta-sw.jpg'>
            <i class="under-image-i">Проектная документация </i>
        </div>
        <div class="options-text left" id="text-1" >

            <div>
                <p style="font-weight: bold;">Строительство капитального сооружения </p>

                <p>Реализация капитального сооружения возможно только при нахождении земельного участка  в собственности или в муниципальной аренде.</p>
                </br>
                <p>Подготовка проекта состоит из:</p>
                </br>
                <ul class="list-numered">
                    <li>Разработка обоснования предельного отклонения от допустимых параметров (при необходимости)</li>
                    <li>Разработка архитектурно-градостроительного облика (АГО)</li>
                    <li>Разработка Проектной документации автомойки самообслуживания. Согласно Постановлению РФ №87 от 16.02.2008г.</li>

                </ul>
            <table style="width:100%" class="second-page-table gray-table">
              <tr >
                <th colspan="2" style="text-transform: none;"><div class="table-panel"></div><span class="in-table-th">Проект строительства капитального здания</span></th>
              </tr>
              <tr>
                <td>Цена</td>
                <td>от 500 000 ₽</td>
              </tr>
              <tr>
                <td>Срок</td>
                <td>от 2 месяцев</td>
              </tr>
            </table>

            </div>

        </div>


        <br>
        <br>

</div>




<div class="container">

</br>

        <div class="options-text left" id="text-1" >

            <div>
                <p style="font-weight: bold;">Строительство временного сооружения </p>

                <p>Как правило, возводят на арендованных площадях. При аренде земельного участка рекомендуем оформлять договор долгосрочной аренды.
                Это обеспечит стабильность ваших взаимоотношений с собственником. 
Для возведения временного сооружения необходимы следующие документы:
</p>
                </br>
                <p>Для строительства временного сооружения необходимо:</p>
                </br>
                <ul class="list">
                    <li>Проектное решение временного сооружения АГР (Архитектурное-градостроительное решения)</li>
                    <li>Согласование АГР в администрации населенного пункта;</li>
                    <li>Заключение кадастрового инженера о некапитальности объекта.</li>

                </ul>
            <table style="width:100%" class="second-page-table gray-table">
              <tr >
                <th colspan="2" style="text-transform: none;"><div class="table-panel"></div><span class="in-table-th">Проект для возведения временного сооружения</span></th>
              </tr>
              <tr>
                <td>Цена</td>
                <td>от 500 000 ₽</td>
              </tr>
              <tr>
                <td>Срок</td>
                <td>от 2 месяцев</td>
              </tr>
            </table>

            </div>

        </div>
        <div id="img-5" class="right-pic" style="">
            <img alt="проект автомойки самообслуживания" title="проект автомойки самообслуживания от производителя Nerta-SW" src='//nerta-sw.ru/wp-content/uploads/2020/07/55551.jpg'>
            <i class="under-image-i under-image-i-right">Проектное решение</i>
        </div>


        <br>
        <br>

</div>

</br></br></br>



<div class="sub-header-line-left"></div>
<div class="container">

    <div class="column-sub-header ">3. Подготовка земельного участка мойки самообслуживания:</div>


        <div class="options-text left" id="text-1" style="">

            <div>
                <ul class="list" style="padding-top: 48px;">

                    <li>Выровнять участок;</li>
                    <li>Подвести и проложить необходимые инженерные коммуникации;</li>
                    <li>Установить ЖБИ кольца.</li>

                </ul>
            <table style="width:100%" class="second-page-table gray-table">
              <tr >
                <th colspan="2" style="text-transform: none;"><div class="table-panel"></div><span class="in-table-th">Стоимость подготовки земельного участка</span></th>
              </tr>
              <tr>
                <td>Цена</td>
                <td>от 100 000 ₽</td>
              </tr>
              <tr>
                <td>Срок</td>
                <td>от 2 месяцев</td>
              </tr>
            </table>

            </div>

        </div>

            <div id="img-5"  class="right-pic" style="margin-top:15px;">
            <img alt="строительство автомоек самообслуживания" title="строительство автомоек самообслуживания под ключ Nerta-SW" src='https://nerta-sw.ru/wp-content/uploads/2020/06/proecting.jpg'>
            <i class="under-image-i under-image-i-right" >Монтаж железобетонных колец</i>
        </div>


        <br>
        <br>

</div>



</br>
</br>
</br>
<div class="sub-header-line-left"></div>
<div class="container" style="position: relative;">
        <div class="column-sub-header">
        <div class="options-text left" id="text-1" style="padding-left:10px;">
            4. Подготовка земельного участка мойки самообслуживания:
        </div>

        </div>

            <div id="img-5" style="margin-top:48px;">
            <img alt="мойки самообслуживания строительство" title="мойки самообслуживания строительствопод ключ Nerta-SW" src='https://nerta-sw.ru/wp-content/uploads/2020/06/contrast_7.jpg'>
                        <i class="under-image-i">Пользуемся бетоном марки не ниже М350</i>
        </div>

        <div class="options-text left" id="text-1" style="">

            <div style="padding-top: 11px;">
            	<br>
                <p>1) Заливка монолитной бетонной плиты или монтаж сборно-разборного фундамента
                </p>
                <ul class="list">
                <p>Строительной бригадой осуществляется:</p>
                </br>
 
                    <li>заливка подбетонки;</li>
                    <li>укладка теплоизоляции, теплого пола, арматуры;</li>
                    <li>заливка основного бетонного фундамента;</li>
                    <li>финишной стадией выступает шлифовка готового бетона</li>

                </ul>
            <table style="width:100%" class="second-page-table gray-table">
              <tr >
                <th colspan="2" style="text-transform: none;"><div class="table-panel"></div><span class="in-table-th">Стоимость подготовки земельного участка</span></th>
              </tr>
              <tr>
                <td>Цена</td>
                <td>от 250 000 ₽</td>
              </tr>
              <tr>
                <td>Срок</td>
                <td>от 3 недель</td>
              </tr>
            </table>

            </div>

        </div>




        <br>
        <br>

</div>










<div class="container">
</br>
</br>
</br>

            <div id="img-5" style="padding-top:17px;">
            <img alt="мойки самообслуживания установка" title="мойки самообслуживания установка Nerta-SW" src='https://nerta-sw.ru/wp-content/uploads/2020/06/Contrast_2.jpg'>
            <i class="under-image-i">Монтаж каркаса</i>
       
        </div>


        <div class="options-text left" id="text-1" style="">

            <div>
                <p>2) Установка навеса, технического помещения мойки самообслуживания.
                </p>
                <ul class="list">
                    <p>В рамках этого этапа предусматривается:</p>
                </br>
                    <li>установка каркаса автомойки и технического помещения</li>
                    <li>прокладка кабелей освещения</li>
                    <li>монтаж ламп, сборка электрических щитов</li>
                    <li>крепление рекламного фриза.</li>

                </ul>
            <table style="width:100%" class="second-page-table gray-table">
              <tr >
                <th colspan="2" style="text-transform: none;"><div class="table-panel"></div><span class="in-table-th">Стоимость и сроки</span></th>
              </tr>
              <tr>
                <td>Цена</td>
                <td>от 250 000 ₽</td>
              </tr>
              <tr>
                <td>Срок</td>
                <td>от 2 месяцев</td>
              </tr>
            </table>

            </div>

        </div>



        <br>
        <br>

</div>

</br>
</br>
</br>





<div class="sub-header-line-left"></div>
<div class="container">
        <div class="column-sub-header" style="float: right;">5. Монтаж моечного оборудования</div>
        <div class="options-text left" id="text-1" style="">

            <div>

                <ul class="list" style="padding-top: 18px;">
                    <p>В этап входит:</p>
                </br>
                    <li>Установка оборудования;</li>
                    <li>Тестирование и наладка;</li>
                    <li>Ввод в эксплуатацию автомойки самообслуживания</li>
                </ul>
            <table style="width:100%" class="second-page-table gray-table">
              <tr >
                <th colspan="2" style="text-transform: none;"><div class="table-panel"></div><span class="in-table-th">Стоимость и сроки</span></th>
              </tr>
              <tr>
                <td>Цена</td>
                <td>от 170 000 ₽</td>
              </tr>
              <tr>
                <td>Срок</td>
                <td>от 3 дней</td>
              </tr>
            </table>

            </div>

        </div>

            <div id="img-5" class="right-pic" style="margin-top:15px;">
            <img alt="стоимость установки оборудования мойка самообслуживания " title="стоимость установки оборудования для мойки самообслуживания Nerta-SW" src='https://nerta-sw.ru/wp-content/uploads/2020/06/-d0-b0-d0-b2-d1-82-d0-be-d0-bc-d0-be-d0-b9-d0-ba-d0-b8_-d1-81-d1-82-d1-80-d0-be-d0-b8-d1-82-d0-b5-d0-bb-d1-8c-d1-81-d1-82-d0-b2-d0-be_-d0-bf-d0-be-d0-b4_-d0-ba-d0-bb-d1-8e-d1-87_nerta-sw.jpg'>
                        <i class="under-image-i under-image-i-right">Монтаж оборудования</i>
        </div>

        <br>
        <br>

</div>


</br>
</br>
</br>
<div class="sub-header-line-left"></div>

<div class="container">
<div class="column-sub-header" >6. Благоустройство прилегающей территории</div>


        <div id="img-5" style="margin-top:15px;">
            <img alt="купить автомойку самообслуживания под ключ цена" title="купить автомойку самообслуживания под ключ от производителя Nerta-SW" src='https://nerta-sw.ru/wp-content/uploads/2020/06/contrast_3.jpg'>
                                    <i class="under-image-i">МСО с благоустроенной территорией</i>
        </div>


        <div class="options-text left" id="text-1" style="">

            <div>

                <ul class="list" style="padding-top: 20px;">

 
                    <li>выполняется асфальтирование площадки;</li>
                    <li>наносится разметка;</li>
                    <li>укладываются газоны;</li>
                    <li>производятся другие работы, направленные на придание мойке привлекательности</li>
                </ul>
            <table style="width:100%" class="second-page-table gray-table">
              <tr >
                <th colspan="2" style="text-transform: none;"><div class="table-panel"></div><span class="in-table-th">Стоимость и сроки</span></th>
              </tr>
              <tr>
                <td>Цена</td>
                <td>от 1200 руб. / кв.м.</td>
              </tr>
              <tr>
                <td>Срок</td>
                <td>от 3 дней</td>
              </tr>
            </table>

            </div>

        </div>


        <br>
        <br>

</div>


</br>
</br>
</br>

<div class="sub-header-line-left"></div>

<div class="container">

<div class="column-sub-header" >7. Обучение и помощь</div>

        <div class="options-text left" id="text-1" style="">

            <div>

                <p style="padding-top: 58px;">Обеспечиваем комплексную поддержку клиентов, осуществляя:
                </p>
                <ul class="list">

                </br>
                    <li>Обучение персонала правилам работы, обслуживания, поддержания стабильного функционирования мойки;</li>
                    <li>Поставка моющей химии собственного производства</li>
                    <li>Помощь в раскрутке, организация рекламных кампаний</li>
                </ul>


            </div>

        </div>

        <div id="img-5" class="right-pic" style="margin-top:15px;">
            <img alt="автомойка пена от производителя Nerta-SW" title="Собственное производство моющей химии NERTA-SW" src='https://nerta-sw.ru/wp-content/uploads/2020/07/331wer.jpg'>
                        <i class="under-image-i under-image-i-right">Моющая химия собственного производства</i>
        </div>

</div>



<div class="container">
        <div class="title">
          <h2>Сколько стоит мойка самообслуживания под ключ?</h2>
        </div>

<p>На итоговую стоимость влияет комплекс факторов, часть из которых уже была рассмотрена выше. Но всегда есть выбор - экономить на каждой статье расходов, покупать все самое дорогое или выбирать сбалансированный вариант.  NERTA-SW –  это, как раз, баланс между ценой, надёжностью и удобством эксплуатации.</p>
<br>
        <div class="options-text left" id="text-1" style="">

            <div style="padding-top:24px;">
                <p>NERTA-SW –   разрабатываем проекты, сочетающие:
                </p>
                <ul class="list">


                    <li>адекватную стоимость</li>
                    <li>расширенную функциональность</li>
                    <li>достойное качество</li>
                    <li>эффективность работы</li>
                </ul>

            <table style="width:100%" class="second-page-table gray-table">
              <tr >
                <th colspan="2" style="text-transform: none;"><div class="table-panel"></div><span class="in-table-th">Автомойка самообслуживания - цена под ключ (4 поста)</span></th>
              </tr>
              <tr>
                <td>Цена</td>
                <td>от 8 000 000 ₽</td>
              </tr>
              <tr>
                <td>Срок</td>
                <td>3-4 месяца</td>
              </tr>
            </table>
            </div>

        </div>
        <div id="img-5" class="right-pic" style="">
            <img alt="бесконтактные автомойки" title="бесконтактные автомойки от производителя Nerta-SW" src='https://nerta-sw.ru/wp-content/uploads/2020/06/-d0-b0-d0-b2-d1-82-d0-be-d0-bc-d0-be-d0-b9-d0-ba-d0-b8_-d1-81-d0-b0-d0-bc-d0-be-d0-be-d0-b1-d1-81-d0-bb-d1-83-d0-b6-d0-b8-d0-b2-d0-b0-d0-bd-d0-b8-d1-8f_-d0-bf-d0-be-d0-b4_-d0-ba-d0-bb-d1-8e-d1-87_nert.jpg'>
                                                <i class="under-image-i under-image-i-right" >Четырёх постовая МСО</i>

        </div>

<br><br>
<p style="display:block;clear: both;">В среднем строительство МСО от разработки проекта до запуска мойки занимает в районе 3-4 месяцев. Затянуть проект может согласование разрешительной документации, необходимость серьезной подготовки участка, масштабное благоустройство, т.д.
</p><br>
<p>В среднем цена строительства бюджетной мойки самообслуживания на 4 поста составляет около 8 миллионов рублей. Эта цифра ориентировочная, но близкая к реальности. Кроме уже рассмотренных факторов бюджет в рублях может меняться под воздействием колебаний курса евро и доллара. Услуги поставщиков, стоимость оборудования чаще всего определяется в иностранной валюте.</p>
        <br>
        <br>

</div>



<div class="container">
        <div class="title">
          <h2>Мойка самообслуживания под ключ - цена в Москве</h2>
        </div>

<p>Стоимость строительства МСО в Москве выше по сравнению с регионами. Это обусловлено рядом причин:</p>
<br>
        <div class="options-text left" id="text-1" style="padding-top: 0px;">

            <div>
                <ul class="list">

                    <li><span class="b">Цена участка.</span> Земля в Москве дорогая. Нужно закладывать в бюджет более высокие расходы</li>
                    <li><span class="b">Стоимость услуг строителей и материалов.</span> В Москве по сравнению с регионами завышенные цены на работу мастеров и поставки стройматериалов.</li>
                    <li><span class="b">Требовательность клиентов.</span> Столичный автолюбитель привык к сервису. Поэтому придется вложить средства в благоустройство территории. </li>
                    
                </ul>
<br>
            <table style="width:100%" class="second-page-table gray-table">
              <tr >
                <th colspan="2" style="text-transform: none;"><div class="table-panel"></div><span class="in-table-th">Автомойка самообслуживания - цена под ключ (4 поста)</span></th>
              </tr>
              <tr>
                <td>Цена</td>
                <td>~ 12 000 000 ₽</td>
              </tr>
              <tr>
                <td>Срок</td>
                <td>3-4 месяца</td>
              </tr>
            </table>
            </div>

        </div>
        <div id="img-5" class="right-pic" style="">
            <img alt="мойка самообслуживания под ключ цена москва" title="мойка самообслуживания под ключ цена москва от производителя Nerta-SW" src='//nerta-sw.ru/wp-content/uploads/2020/07/123.jpg' style="width:97%"><i class="under-image-i under-image-i-right" >МСО закрытого типа</i>
        </div>



</div>

<br><br><br>


<div  style="background-color: #00013c;color:white;text-align: center;padding:45px 0px;">
    <div class="container">
    <p>Чтобы уточнить сколько будет стоить проект, задать вопросы, получить консультации, свяжитесь с менеджерами компании «NERTA-SW».
Специалисты соберут данные, проведут расчеты, подготовят коммерческое предложение.</p>
<div class="third-group">
    <div class="third-blocks">
    <a href="tel:8 (495) 6-777-000">8 (495) 6-777-000</a>
</div>
<div class="third-blocks callback-on-panel">
   <div class="">
            <span class=" js-callback-popup-show">Обратный звонок</span>
        </div> 
</div>
<div class="third-blocks">
        <a href="mailto:info@nerta-sw.ru" style="padding-top: 11px;
">info@nerta-sw.ru</a>
</div>

</div>

    </div>
</div>






<div class="container">
        <div class="title">
          <h2>Почему выгоднее купить мойки самообслуживания под ключ, чем строить самостоятельно?</h2>
        </div>

<p>Поиск земельного участка и подрядчиков для выполнения строительных работ, разработка и согласование проектной документации.
С этими трудностями сталкиваются предприниматели, желающие самостоятельно открыть мойку самообслуживания. Отсутствие необходимого опыта, достаточных знаний в рассматриваемой сфере часто приводит к нерациональному использованию имеющихся средств. Кроме этого выполнение проекта «под ключ» обеспечивает:</p>
<br>


            <div>
                <ul class="">

                    <li><div class="quadrblock">● Экономию времени и средств</div><span style="padding-left:25px;display: block;padding-top:10px;">Клиенту не придется заниматься общением с подрядчиками, выбором оборудования. Сотрудники компании NERTA-SW помогают оценить перспективность участка. Это поможет избежать неоправданных расходов. </span><br></li>
                    <li><div class="quadrblock" style="background-color:#1893d0;">● Комфорт и уверенность</div><span style="padding-left:25px;display: block;padding-top:10px;">Компания NERTA-SW ввела в эксплуатацию более 100 моек, работает с 2000 года. Мы гарантированно построим автомойку, которая будет востребована у клиентов.</span> <br></li>
                    <li><div class="quadrblock">● Помощь и поддержка</div>
                        <span style="padding-left:25px;display: block;padding-top:10px;">
Консультируем по продвижению бизнеса. Помогаем организовать эффективную рекламную кампанию, которая обеспечит стабильный поток клиентов на МСО.</span><br>
                    </li>
                    <li><div class="quadrblock" style="background-color:#1893d0;">● Гарантия и ответственность</div><span style="padding-left:25px;display: block;padding-top:10px;">Доверять выполнение всего комплекса работ одной компании удобно и выгодно. NERTA-SW несёт полную ответственность за выполненные работы. Вам не нужно разбираться кто из подрядчиков виноват в проблеме в случае её возникновения. На выполненные работы, а также установленное оборудование дается гарантия 1 год.</span><br>
</li>

<li><div class="quadrblock">● Эксплуатационное обслуживание</div><span style="padding-left:25px;display: block;padding-top:10px;">
Благодаря системе удалённого мониторинга оборудования, вы всегда в курсе, когда нужно произвести регламентные работы.</span></li>
                </ul>

            </div>


</div>



<div class="container">
        <div class="title">
          <h2>Рекомендации, которые помогут сделать будущий бизнес еще более успешным!</h2>
        </div>

<br>
        <div id="img-5" style="">
            <img alt="бесконтактная мойка сколько стоит" title="бесконтактная мойка сколько стоит от производителя Nerta-SW " src='https://nerta-sw.ru/wp-content/uploads/2020/06/contrast_6.jpg'>
            <i class="under-image-i">Мойка закрытого типа</i>
        </div>

        <div class="options-text left" id="text-1" style="padding-top:15px;">

            <div>
                <p>В регионах, где на протяжении трёх и более месяцев стабильно отрицательная температура (от -10°С и ниже), стоит сразу строить закрытую автомойку самообслуживания.</p>
                <p>Вы получаете неоспоримое преимущество перед конкурентами с открытыми мойками самообслуживания. Достоинства закрытых МСО:</p>
                <ul class="list">

                    <li>Значительно увеличивает поток клиентов при отрицательной температуре</li>
                    <li>Защищает клиентов от ветра и косого дождя</li>
                    <li>Может обустраиваться в уже возведенном здании</li>
  <p>Но стоимость строительства закрытых моек самообслуживания выше. Поэтому наиболее рентабельным использование такого решения становится в северных регионах страны.</p>
                </ul>


            </div>

        </div>




</div>



<div class="container">

<br><br><br>

        <div class="options-text left" id="text-1" style="">

            <div>
                <p>Не экономьте на оборудовании. От качества, надежности, удобства работы технического оснащения зависит впечатления клиентов МСО. </p><br>
                <p>Дешевое оборудование часто выходит из строя, что приводит к простоям, непредвиденным затратам, клиенты автомойки самообслуживания оставляют отрицательные отзывы. Все эти факторы значительно уменьшают посещаемость МСО.</p><br>
                <p>Не пренебрегайте внешним видом. Важно использовать долговечные стройматериалы при строительстве и отделке мойки. Такая МСО будет сохранять привлекательность на протяжении 5 и более лет, обеспечивая уверенность в качестве, надежности бизнеса.  </p><br>

                <p>В случае ограниченности бюджета можно арендовать старую автомойку либо площадку на паркинге. Плюсы такого решения:</p><br>

                <ul class="list">

                    <li>Наличие подведенных инженерных коммуникаций</li>
                    <li>Готовые транспортные узлы</li>
                    <li>Наличие «разогретой клиентской базы, которая уже готова пользоваться услугами вашей МСО</li>
                    <li>Уменьшение затрат на покупку участка </li>
                </ul>

 
            </div>

        </div>
        <div id="img-5" class="right-pic" style="">
            <img alt="москва строительство автомойки под ключ" title="москва строительство автомойки под ключ от производителя Nerta-SW" src='//nerta-sw.ru/wp-content/uploads/2020/06/selective_color_2-1.jpg'>
              <i class="under-image-i under-image-i-right">Используем нержавеющую сталь Aisi 304</i>
        </div>



</div>


<div class="container">
        <div class="title">
          <h2>5 причин доверить строительство мойки самообслуживания под ключ компании NERTA-SW</h2>
        </div>

<p>Осуществляем деятельность на территории РФ с 2000 года. За это время было успешно реализовано более 100 проектов. Установлено более 500 постов по всей территории Российской Федерации. Накопили опыт, который позволяет оптимизировать реализацию каждого проекта.
Преимущества компании NERTA-SW:</p>
<br>
        <div class="options-text left" id="text-1" >

            <div style="padding-top:24px;">
                <ul class="list without-dots">

                    <li><span class="b">1. Собственное производство моечного оборудования. </span>Высокотехнологичные заводские линии – залог изготовления моечного оборудования в оговоренные сроки.</li>
                    <li><span class="b">2. Комплексный подход и сервисное обслуживание. </span>У компаний имеется система дистанционного мониторинга и управления оборудованием МСО для контроля стабильности работы МСО, минимизирования простоев, вызванных техническими неисправностями.</li>
                    <li><span class="b">3. Безупречная репутация.</span> Располагаем собственной сетью моек самообслуживания, состоящей из 18 автомоек по всей России. Благодаря этому досконально знаем все «подводные камни», нюансы бизнеса. Делимся опытом с партнерами, помогаем развить бизнес, привлечь клиентуру, решать возникающие задачи.</li>
                   
                </ul>

            </div>

        </div>
        <div id="img-5" style="" class="right-pic">
            <img alt="сотрудники производства моек самообслуживания" title="сотрудники производства моек самообслуживания от Nerta-SW" src='https://nerta-sw.ru/wp-content/uploads/2020/06/-d0-bc-d0-be-d0-b9-d0-ba-d0-b8_-d1-81-d0-b0-d0-bc-d0-be-d0-be-d0-b1-d1-81-d0-bb-d1-83-d0-b6-d0-b8-d0-b2-d0-b0-d0-bd-d0-b8-d1-8f_-d1-81-d1-82-d1-80-d0-be-d0-b8-d1-82-d0-b5-d0-bb-d1-8c-d1-81-d1-82-d0-b2.jpg'>
            <i class="under-image-i under-image-i-right">Производство NERTA-SW</i>
        </div>



</div>



<div class="container">
    
<br>
        <div id="img-5" style="">
            <img alt="строительство автомойки самообслуживания" title="строительство автомойки самообслуживания от производителя Nerta-SW" src='https://nerta-sw.ru/wp-content/uploads/2020/06/-d1-81-d1-82-d1-80-d0-be-d0-b8-d1-82-d0-b5-d0-bb-d1-8c-d1-81-d1-82-d0-b2-d0-be_-d0-b0-d0-b2-d1-82-d0-be-d0-bc-d0-be-d0-b5-d0-ba_-d1-81-d0-b0-d0-bc-d0-be-d0-be-d0-b1-d1-81-d0-bb-d1-83-d0-b6-d0-b8-d0-b2.jpg'>
            <i class="under-image-i">Отработанные процессы строительства</i>
        </div>

        <div class="options-text left" id="text-1" style="">

            <div style="padding-top: 40px;">
                <ul class="list without-dots">

                    <li><span class="b">4. Сбалансированные цены.</span> Рационально подходим к использованию предоставленного бюджета. Экономим там, где можно, инвестируем туда, куда нужно. Благодаря налаженным партнерским отношениям с подрядчиками гарантируем доступные цены на проектировочные, строительные, монтажные работы. </li>
                    <li><span class="b">5. Скорость строительства.</span>Строим быстро даже масштабные объекты «с нуля». Оптимизируем транспортные издержи, используем современные технологии.</li>
                   
                </ul>


            </div>

        </div>

</div>



<div class="container">
        <div class="title">
          <h2>Как купить мойки самообслуживания под ключ?</h2>
        </div>


        <div id="img-5" style="">
            <img alt="сотрудник компании автомоек самообслуживания" title="сотрудник компании автомоек самообслуживания от производителя Nerta-SW" src='//nerta-sw.ru/wp-content/uploads/2020/06/-d1-81-d0-be-d1-82-d1-80-d1-83-d0-b4-d0-bd-d0-b8-d0-ba-d0-b8_nerta-sw.jpg'>
                      <i class="under-image-i"> NERTA-SW – работаем с 2000 года</i>
        </div>


        <div class="options-text left" id="text-1" style="">

            <div>
                <p>Первый шаг к созданию успешного и прибыльного бизнеса - заполните форму заявки на сайте либо созвонитесь с менеджерами компании по номеру 8 (495) 6-777-000. </p><br>
                <p>Консультанты компании свяжутся с вами. Подготовим детализированный бизнес-план, поможем спрогнозировать прибыль, рассчитать затраты. Обеспечиваем комфортные условия сотрудничества, отсутствие «сюрпризов» (увеличение цены, затягивание сроков, т.д.) при реализации проекта.</p><br>
                <p>Открыты к диалогу и выполнению масштабных задач. Работаем открыто и прозрачно. Обращаем внимание, что все расчеты, консультации предоставляются бесплатно. Поэтому оставьте свои контактные данные. Мы же сделаем всё возможное, чтобы подготовить для вас успешный бизнес с серьезными перспективами, гарантированной прибылью.</p><br>



            </div>
        </div>




</div>




<div style="background-color: #f3f3f3;width:100%; margin-top: 60px;">

        <div class="title" style="padding-top:69px;padding-bottom:57px; text-align:center; margin:0px !important;font-size: 32px;"><h2 style="max-width: 1177px;margin: 0 auto;">Автомобильные мойки самообслуживания — максимальный доход с минимальными вложениями!</h2></div>

</div>


    <script>
//--------------------------------
if(window.innerWidth<850)
{
  document.querySelector('.cost').appendChild(document.querySelector('.dop'));
}
////--------------------------------
</script>
<?php get_footer(); ?>