<?php
/**
 * Template Name: equipment optima
 */
get_header(); ?>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/optima.css">

    <section class="chief"
             style="background-image: url('<?= get_template_directory_uri(); ?>/img/equipment_optima/main.jpg')">
        <div class="chief__container">
            <div class="breadcrumbs">
                <?php the_breadcrumb() ?>
            </div>
            <h1 class="chief__title">
                Комплектация <br>
                optima
            </h1>
            <div class="chief__description">
                <p class="chief__description__information">
                    Комплект моечного оборудования с лучшем соотношением цены-качества. Самая популярная комплектация.
                </p>
                <script data-b24-form="click/4/zy99qe" data-skip-moving="true"> (function (w, d, u) {
                        var s = d.createElement('script');
                        s.async = true;
                        s.src = u + '?' + (Date.now() / 180000 | 0);
                        var h = d.getElementsByTagName('script')[0];
                        h.parentNode.insertBefore(s, h);
                    })(window, document, 'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_4.js'); </script>
                <button class="chief__description__button">Оформить заявку</button>
            </div>
        </div>
    </section>

    <section class="small__advantages">
        <div class="small__advantages__container">
            <div class="small__advantages__block">
                <img class="small__advantages__image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/icon1.png" alt="">
                <p class="small__advantages__block__bold_text">
                    Пена подается под низким давлением
                </p>
                <p class="small__advantages__block__normal_text">
                    Более экономичное и удобное в эксплуатации решение чем при подаче пены под высоким давлением
                </p>
            </div>
            <div class="small__advantages__block">
                <img class="small__advantages__image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/icon2.png" alt="">
                <p class="small__advantages__block__bold_text">
                    Используются дорогие комплектующие
                </p>
                <p class="small__advantages__block__normal_text">
                    Их качество выше, а срок эксплуатации больше, чем у дешевых аналогов
                </p>
            </div>
            <div class="small__advantages__block">
                <img class="small__advantages__image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/icon3.png" alt="">
                <p class="small__advantages__block__bold_text">
                    OPTIMA – bestseller
                </p>
                <p class="small__advantages__block__normal_text">
                    Гарантия на оборудование – 1 год
                </p>
            </div>
        </div>
    </section>

    <section class="equipment__description">
        <div class="equipment__description__container">
            <div class="equipment__description__title">
                <h2>
                    Панель управления из нержавеющей стали
                </h2>
            </div>

            <div class="equipment__description__blocks">
                <div class="equipment__description__block1">
                    <img class="block__image_2" src="<?= get_template_directory_uri(); ?>/img/equipment_optima/6.jpg"
                         title="Панель управления optima от производителя оборудования для моек самообслуживания  NERTA-SW"
                         alt="Панель управления optima оборудование  мойки самообслуживания">

                    <div class="equipment__description__block1__information">
                        <div class="equipment__description__block1__column">
                            <p class="equipment__description__subtitle">Моющие программы:</p>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/icon4.png" alt="">
                                <p>пена</p>
                            </div>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/icon5.png" alt="">
                                <p>вода+пена (шампунь)</p>
                            </div>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/icon6.png" alt="">
                                <p>теплая вода</p>
                            </div>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/icon7.png" alt="">
                                <p>холодная вода</p>
                            </div>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/icon8.png" alt="">
                                <p>воск</p>
                            </div>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/icon10.png" alt="">
                                <p>осмос</p>
                            </div>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/icon9.png" alt="">
                                <p>стоп/пауза</p>
                            </div>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/icon11.png" alt="">
                                <p>воздух</p>
                            </div>
                        </div>
                        <button class="equipment__description__button">Рассчитать стоимость</button>
                    </div>
                </div>
                <div class="equipment__description__block2">
                    <div class="equipment__description__block2__content">
                        <p class="equipment__description__block2__subtitle__big">
                            Панель управления <br>
                            «OPTIMA»
                        </p>
                        <p class="equipment__description__block2__subtitle">
                            Внешняя панель управления мойкой из
                            нержавеющей стали Aisi 304 с сенсорными клавишами с подсветкой
                        </p>
                        <ul>
                            <li>
                                Купюроприемник Comestero с кассетой на 600 купюр (Италия)
                            </li>
                            <li>
                                Монетоприемник Alberici (Италия)
                            </li>
                            <li>Пеноизоляция</li>
                            <li>Теплоизоляция</li>
                            <li>Климат-контроль</li>
                            <li>Подготовка под оплату картами лояльности</li>
                        </ul>
                    </div>
                    <div class="equipment__description__block2__content">
                        <p class="equipment__description__block2__dop_1">
                            возможно подключить дополнительные программы:
                        </p>
                        <p class="equipment__description__block2__dop_2">
                            воздух, осмос, мойка дисков, средство от насекомых, щетка
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="power__equipment">
        <div class="power__equipment__container">
            <div class="power__equipment__title">
                <h2>
                    Силовое оборудование
                </h2>
            </div>
            <div class="power__equipment__block__image">
                <img src="<?= get_template_directory_uri(); ?>/img/equipment_optima/1.jpg"
                     title="Рама с силовым оборудованием  от компании Nerta-SW"
                     alt="Рама с силовым оборудованием для моек самообслуживания|Nerta-SW">
                <div class="power__equipment__block__image__title">
                    <p>Рама из нержавеющей стали Aisi 304</p>
                </div>
            </div>
            <div class="power__equipment__block__title">
                Комплект моечного оборудования «OPTIMA» с лучшем соотношением цены-качества.
                Самая популярная комплектация. В нее входят:
            </div>
            <div class="power__equipment__block__list">
                <div class="power__equipment__block__1">
                    <ul>
                        <li>
                            Рама из нержавеющей стали Aisi 304
                        </li>
                        <li>
                            Промышленный насос высокого давления Annovi Reverberi с муфтой, рабочее давление от 15 до
                            200 бар (Италия)
                        </li>
                        <li>
                            Электродвигатель Ravel мощностью 5,5 кВт (Италия)
                        </li>
                        <li>
                            Система против замерзания ANTI FROST
                        </li>
                        <li>
                            Воздушный компрессор Remeza (Италия-Белоруссия)
                        </li>
                        <li>
                            Насосная станция Wilo на 380 с электрическим шкафом и системой безопасности (Германия)
                        </li>
                        <li>
                            Бойлер косвенного нагрева Baxi на 400 литров
                        </li>
                    </ul>
                </div>
                <div class="power__equipment__block__1">
                    <ul>
                        <li>
                            Система дозировки жидких химических реагентов SECO AKL (Италия)
                        </li>
                        <li>
                            Магистрали подачи воды с шаровым кранами
                        </li>
                        <li>
                            Электромагнитный клапан высокого давления NRT (Китай)
                        </li>
                        <li>
                            Электрический шкаф Schneider Electric (Франция) – индивидуальный на каждый пост
                        </li>
                        <li>
                            Комплект электрооборудования Schneider Electric (Франция)
                        </li>
                        <li>
                            Частотный преобразователь Schneider Electric (Франция)
                        </li>
                        <li>
                            Комплект для нанесения пены R+M (Германия)
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </section>

    <section class="dop__description dop__description__blue">
        <div class="dop__description__container dop__description__container__padding">
            <div class="dop__description__block__img">
                <img class="dop__description__image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/2.jpg"
                     title="Высокоточные дозатор моющего средства для моек самообслуживания Nerta-SW"
                     alt="Высокоточные дозатор моющего средства для мойки самообслуживания |Nerta-SW">
            </div>
            <div class="dop__description__block">
                <ul>
                    <li>
                        Система удаленного контроля и управления постом самообслуживания PRO
                    </li>
                    <li>
                        Пистолеты высокого давления ST2600 R+M (Германия)
                    </li>
                    <li>
                        Пистолеты низкого давления R+M (Германия)
                    </li>
                    <li>
                        Держатель для пистолета из нержавеющей стали
                    </li>
                    <li>
                        Поворотная консоль 1800 мм. из нержавеющей стали
                    </li>
                    <li>
                        Держатель для автомобильных ковриков из нержавеющей стали
                    </li>
                    <li>
                        Емкость для подготовленной воды на 2000 литров с системой автоматики
                    </li>
                </ul>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Цена 1 поста
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                от 10 500 €
                            </div>
                            <div class="block__cost__number3">
                                1 050 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Монтаж, в независимости от количества постов
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                3 000 €
                            </div>
                            <div class="block__cost__number3">
                                300 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Slider main container -->
    <section class="swiper__content swiper__content__grey">
        <div class="swiper">
            <div class="swiper-button-container">
                <div class="swiper-button-prev swiper-button-prev1"></div>
                <div class="swiper-button-next swiper-button-next1"></div>
            </div>
            <div class="swiper-container swiper-1">
                <div class="swiper-wrapper">
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_optima/s1.png"
                             title="Шкаф управления оборудованием для мойки самообслуживания из нержавейки|Nerta-SW"
                             alt="шкаф управления оборудованием мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Тепло-гидроизоляция
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_optima/s6.png"
                             title="Монетоприёмник (Италия) для моек самообслуживания |Nerta-SW"
                             alt="Монетоприёмник Alberici AL 66S для мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Монетоприёмник Alberici
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_optima/s4.png"
                             title="Бойлеры для нагрева воды для моек самообслуживания Nerta-SW"
                             alt="Бойлеры Maxi для мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Бойлер Baxi
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_optima/s5.png"
                             title="Клапана высокого давления  (Optima) для моек самообслуживания Nerta-SW"
                             alt="Насосная станция Wilo 380 для моек самообслуживания|Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Насосная станция Wilo на 380 с электрическим шкафом и системой безопасности (Германия)
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_optima/s2.png"
                             title="Монетоприёмник   (OPTIMA) для моек самообслуживания Nerta-SW"
                             alt="Монетоприёмник Commestero для мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Купюроприемник Comestero
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_optima/s7.png"
                             title="Рукав высокого давления для моек самообслуживания |Nerta-SW"
                             alt="Рукав высокого давления для мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Рукав высокого давления
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_optima/s8.png"
                             title="Клапана высокого давления  (Optima) для моек самообслуживания Nerta-SW"
                             alt="Клапана высокого давления NRT для моек самообслуживания|Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Клапан высокого давления NRT
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_optima/s9.png"
                             title="Воздушный компрессор для моек самообслуживания |Nerta-SW"
                             alt="Воздушный компрессор  Remez для мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Воздушный компрессор Remeza (Италия - Белоруссия)
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_optima/s10.png"
                             title="Система дозировки для моек самообслуживания |Nerta-SW"
                             alt="Дозатор моющего средства SECO для мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Дозатор моющего средства SECO
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dop__description">
        <div class="dop__description__container dop__description__container__margin dop__description__container__reverse">
            <div class="dop__description__block__img">
                <img class="dop__description__image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/3.jpg"
                     title="Сборочный цех компании Nerta-SW"
                     alt="Сборка силовой рамы оборудования для мойки самообслуживания |Nerta-SW">
            </div>
            <div class="dop__description__block ">
                <div class="dop__description__block__title">
                    <p>Дополнительные опции</p>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Бесконтактная оплата с помощью пластиковых карт лояльности
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                200 €
                            </div>
                            <div class="block__cost__number3">
                                20 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Бесконтактная оплата с помощью банковских карт и смартфонов (Apple/Samsung/ GooglePay)
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                400 €
                            </div>
                            <div class="block__cost__number3">
                                40 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Замена насоса высокого давления на CatPumps
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                300 €
                            </div>
                            <div class="block__cost__number3">
                                30 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Замена электромагнитного клапана на Burkert
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                250 €
                            </div>
                            <div class="block__cost__number3">
                                25 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Замена системы дозировки на SECO TPG
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                400 €
                            </div>
                            <div class="block__cost__number3">
                                40 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Замена панели управления программами на Maxi
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                250 €
                            </div>
                            <div class="block__cost__number3">
                                25 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Slider main container -->
    <section class="swiper__content swiper__content__display">
        <div class="swiper">
            <div class="swiper__content__title">
                <h2>
                    Пример цветового оформления панелей
                </h2>
            </div>
            <div class="swiper-button-container">
                <div class="swiper-button-prev swiper-button-prev2"></div>
                <div class="swiper-button-next swiper-button-next2"></div>
            </div>
            <div class="swiper-container swiper-2">
                <div class="swiper-wrapper">
                    <div class="swiper-slide swiper-slide__margin_bottom swiper-slide_center">
                        <img class="swiper-slide__color_image"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_optima/4.jpg"
                             title="Панель управления optima от производителя оборудования для моек самообслуживания  NERTA-SW"
                             alt="Панель управления optima для оборудования  мойки самообслуживания –цвет синий">
                    </div>
                    <div class="swiper-slide swiper-slide__margin_bottom swiper-slide_center">
                        <img class="swiper-slide__color_image"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_optima/5.jpg"
                             title="Панель управления optima от производителя оборудования для моек самообслуживания  NERTA-SW"
                             alt="Панель управления optima для оборудования  мойки самообслуживания –цвет зеленый">
                    </div>
                    <div class="swiper-slide swiper-slide__margin_bottom swiper-slide_center">
                        <img class="swiper-slide__color_image"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_optima/6.jpg"
                             title="Панель управления optima от производителя оборудования для моек самообслуживания  NERTA-SW"
                             alt="Панель управления optima для оборудования  мойки самообслуживания –цвет пурпурный">
                    </div>
                    <div class="swiper-slide swiper-slide__margin_bottom swiper-slide_center">
                        <img class="swiper-slide__color_image"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_optima/7.jpg"
                             title="Панель управления optima от производителя оборудования для моек самообслуживания  NERTA-SW"
                             alt="Панель управления optima для оборудования  мойки самообслуживания –цвет красный">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="color__variations">
        <div class="color__variations__container">
            <div class="color__variations__title">
                <h2>
                    Пример цветового оформления панелей
                </h2>
            </div>
            <div class="color__variations__content">
                <img class="swiper-slide__color_image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/4.jpg"
                     title="Панель управления optima от производителя оборудования для моек самообслуживания  NERTA-SW"
                     alt="Панель управления optima для оборудования  мойки самообслуживания –цвет синий">
                <img class="swiper-slide__color_image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/5.jpg"
                     title="Панель управления optima от производителя оборудования для моек самообслуживания  NERTA-SW"
                     alt="Панель управления optima для оборудования  мойки самообслуживания –цвет зеленый">
                <img class="swiper-slide__color_image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/6.jpg"
                     title="Панель управления optima от производителя оборудования для моек самообслуживания  NERTA-SW"
                     alt="Панель управления optima для оборудования  мойки самообслуживания –цвет пурпурный">
                <img class="swiper-slide__color_image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_optima/7.jpg"
                     title="Панель управления optima от производителя оборудования для моек самообслуживания  NERTA-SW"
                     alt="Панель управления optima для оборудования  мойки самообслуживания –цвет красный">
            </div>
        </div>
    </section>

    <section class="feedback"
             style="background-image: url('<?= get_template_directory_uri(); ?>/img/equipment_optima/feed.jpg')">
        <div class="feedback__container">
            <div class="feedback__title">
                Звоните, инженеры компании помогут с выбором оборудования
            </div>
            <p>
                Сравните характеристики комплектаций, рассчитайте стоимость оборудования
            </p>
            <div class="feedback__phone_number">
                8 (800) 555-25-93
            </div>
            <div class="feedback__button">
                <script data-b24-form="click/4/zy99qe" data-skip-moving="true"> (function (w, d, u) {
                        var s = d.createElement('script');
                        s.async = true;
                        s.src = u + '?' + (Date.now() / 180000 | 0);
                        var h = d.getElementsByTagName('script')[0];
                        h.parentNode.insertBefore(s, h);
                    })(window, document, 'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_4.js'); </script>
                <button class="feedback__button__1">Оформить заявку</button>
                <button class="feedback__button__2">Калькулятор</button>
            </div>
        </div>
    </section>
    <!-- Slider main container -->
    <section class="options__mini">
        <div class="options__mini__container">
            <div class="options__mini__subtitle">
                <h2>
                    Посмотреть другие комплектации
                </h2>
            </div>
            <div class="options__mini__row">

                <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_start/'"
                     class="options__mini__block color_1 shadow_box">
                    <div class="options__mini__block__content">
                        <p class="options__mini__title__big">
                            START
                        </p>
                        <p class="options__mini__title">
                            Для тех кто собирает мойку самостоятельно, на своих компонентах.
                        </p>
                        <ul class="options__mini__list">
                            <li>Панель управления;</li>
                            <li>Электротехнический шкаф;</li>
                            <li>Дозатор моющей химии.</li>
                        </ul>
                    </div>
                    <div class="options__mini__block__cost">
                        <div class="block__cost">
                            <div class="block__cost__container">
                                <div class="block__cost__text">
                                    Цена за 1 пост
                                </div>
                                <div class="block__cost__number">
                                    <div class="block__cost__number1">
                                        2 400 €
                                    </div>
                                    <div class="block__cost__number2">
                                        240 000 ₽
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_basic/'"
                     class="options__mini__block color_2">
                    <div class="options__mini__block__content">
                        <p class="options__mini__title__big">
                            BASE
                        </p>
                        <p class="options__mini__title">
                            Для тех кто любит дёшево.
                        </p>
                        <ul class="options__mini__list">
                            <li>Полный комплект для организации мойки;</li>
                            <li>Пена наносится под высоким давлением;</li>
                            <li>Комплектующие начального уровня (Евросоюз, Китай);</li>
                            <li>Гарантия 1 год.</li>
                        </ul>
                    </div>
                    <div class="options__mini__block__cost">
                        <div class="block__cost">
                            <div class="block__cost__container">
                                <div class="block__cost__text">
                                    Цена за 1 пост
                                </div>
                                <div class="block__cost__number">
                                    <div class="block__cost__number1">
                                        5 000 €
                                    </div>
                                    <div class="block__cost__number2">
                                        500 000 ₽
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_light/'"
                     class="options__mini__block color_3">
                    <div class="options__mini__block__content">
                        <p class="options__mini__title__big">
                            LIGHT
                        </p>
                        <p class="options__mini__title">
                            Начальный уровень.
                            Для тех кто экономит с умом.
                        </p>
                        <ul class="options__mini__list">
                            <li>Полный комплект для организации МСО;</li>
                            <li>Пена наносится под низким давлением;</li>
                            <li>Комплектующие начального уровня (Евросоюз, Китай, Россия);</li>
                            <li>Гарантия 1 год.</li>
                        </ul>
                    </div>
                    <div class="options__mini__block__cost">
                        <div class="block__cost">
                            <div class="block__cost__container">
                                <div class="block__cost__text">
                                    Цена за 1 пост
                                </div>
                                <div class="block__cost__number">
                                    <div class="block__cost__number1">
                                        8 000 €
                                    </div>
                                    <div class="block__cost__number2">
                                        800 000 ₽
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_maxi/'"
                     class="options__mini__block color_5">
                    <div class="options__mini__block__content">
                        <p class="options__mini__title__big">
                            MAXI
                        </p>
                        <p class="options__mini__title">
                            Самоё надежное и неприхотливое.
                            Для тех кому лучше идеально, чем дёшево.
                        </p>
                        <ul class="options__mini__list">
                            <li>Полный комплект для мойки самообслуживания;</li>
                            <li>Пена наносится поднизким давлением;</li>
                            <li>Типовые комплектующие (Япония, Канада, Евросоюз);</li>
                            <li>Монтаж;</li>
                            <li>Гарантия 2 года.</li>
                        </ul>
                    </div>
                    <div class="options__mini__block__cost">
                        <div class="block__cost">
                            <div class="block__cost__container">
                                <div class="block__cost__text">
                                    Цена за 1 пост
                                </div>
                                <div class="block__cost__number">
                                    <div class="block__cost__number1">
                                        13 000 €
                                    </div>
                                    <div class="block__cost__number2">
                                        1 300 000 ₽
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="<?php bloginfo("template_url"); ?>/js/gallery.js"></script>


<?php get_footer(); ?>