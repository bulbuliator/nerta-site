jQuery(document).ready(function(){
  jQuery(document).on("click", ".close-btn", function () {
    jQuery(".free-plugins-banner-container").css( 'display', 'none' );
    jQuery.ajax({
      type: "POST",
      url: ajaxurl,
      data: {
        action: 'wdSpeed_banner',
        task: "closeBanner",
      },
      success: function (response) {
        jQuery(".free-plugins-banner-container").remove();
      },
      error: function (jqXHR, textStatus, errorThrown) {
      }
    });
  });
});