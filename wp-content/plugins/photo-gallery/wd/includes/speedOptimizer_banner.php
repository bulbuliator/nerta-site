<?php
if ( !defined('ABSPATH') ) {
  exit;
}

class speedOptimizer_banner {
  public $config;
  public $wd_speedOption_banner;
  public $wd_prefix;

  /**
   * @param array $options
   */
  public function __construct( $options = array() ) {
    if ( isset($this->wd_speedOption_banner) && $this->wd_speedOption_banner == 1 ) {
      return;
    }
    $this->config = $options;
    $this->wd_prefix = $options->prefix;
    $this->speedOptimizer_banner_script_style();
    $task = isset($_POST['task']) ? sanitize_text_field($_POST['task']) : '';
    if( $task == 'closeBanner' ){
      $this->closeBanner();
    } else {
      add_action('admin_notices', array( $this, 'wd_speedOptimizer_banner' ));
    }
  }

  public function wd_speedOptimizer_banner(){
    $config = $this->config;
    ?>
    <div class="free-plugins-banner-container container wrap">
      <span class="close-btn"></span>
      <div class="banner-left-container">
          <p class="plugin-title"><?php echo $config->plugin_title; ?> created by 10Web</p>
          <h2 class="optimizer-banner-title">Managed <br class ="br-mobile"> WordPress Hosting</h2>
          <img class="google-cloud-mobile" src="<?php echo $config->wd_url_css . '/img/google_cloud.png'; ?>">
          <p class="banner-description">Copy your website with 1click to 10Web & automatically get 95+ PageSpeed Score</p>
          <div class="line"></div>
          <img class="google-cloud" src="<?php echo $config->wd_url_css . '/img/google_cloud.png'; ?>">
      </div>
      <div class="banner-right-container">
        <div class="button-div">
          <a href="https://10web.io/<?php echo $config->utm_source;?>" target="_blank">learn more</a>
        </div>
        <p>14-Day Free Trial</p>
      </div>
    </div>

    <?php 
  }

  /* Include js and css */
  public function speedOptimizer_banner_script_style() {
    $wd_options = $this->config;
    wp_register_script('speedOptimizer_banner_js', $wd_options->wd_url_js . '/speedOptimizer_banner.js', array( 'jquery' ));
    wp_register_style('speedOptimizer_banner_css', $wd_options->wd_url_css . '/speedOptimizer_banner.css');
    wp_enqueue_style('wd_speedOptimizer-open-sans', 'https://fonts.googleapis.com/css?family=Open+Sans:100,200,300,400,600,700,800&display=swap');
    wp_enqueue_script('speedOptimizer_banner_js');
    wp_enqueue_style('speedOptimizer_banner_css');
  }

  /* Close banner */
  public function closeBanner() {
    $this->wd_speedOption_banner = 1;
    $wd_speedOption_banner = $this->wd_speedOption_banner;
    update_option('wd_speedOption_banner', $wd_speedOption_banner);
  }
}